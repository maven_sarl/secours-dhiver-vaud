<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://secoursdhiver/secoursdhiver.yaml',
    'modified' => 1725271391,
    'size' => 205,
    'data' => [
        'enabled' => true,
        'default_lang' => 'fr',
        'dropdown' => [
            'enabled' => false
        ],
        'streams' => [
            'schemes' => [
                'theme' => [
                    'type' => 'ReadOnlyStream',
                    'prefixes' => [
                        '' => [
                            0 => 'user/themes/secoursdhiver',
                            1 => 'user/themes/maven'
                        ]
                    ]
                ]
            ]
        ]
    ]
];

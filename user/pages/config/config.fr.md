---
googletag: "<!-- Google Tag Manager -->\n<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':\nnew Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],\nj=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=\n'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);\n})(window,document,'script','dataLayer','GTM-P9ZBDSF');</script>\n<!-- End Google Tag Manager -->"
navigation_main:
    -
        name: 'Nos actions'
        internal_url: /actions
    -
        name: 'Demande d''aide'
        internal_url: /aide
    -
        name: Dons
        internal_url: /donations
    -
        name: Donateurs
        internal_url: /donateurs
    -
        name: Contact
        internal_url: /contact
    -
        name: 'Faire un don'
        icon: icon-shv-coeur
        class: donate
        internal_url: /donations/faire-un-don
navigation_top:
    -
        name: Accueil
        internal_url: /home
    -
        name: 'Actualités et événements'
        internal_url: /blog
    -
        name: Médias
        internal_url: /medias
    -
        name: 'A propos de nous'
        internal_url: /a-propos
published: false
sitemap:
    lastmod: '16-12-2024 15:43'
popins:
    -
        is_published: '1'
        image: null
        slug: null
        date_start: '16-12-2024 15:45'
        date_end: '05-01-2025 20:00'
        duration: null
        content: 'Nous sommes en vacances. Le bureau est fermé du vendredi 20 décembre 2024 au dimanche 5 janvier 2025.'
        cta_label: null
        cta_link: null
date: '16-12-2024 15:45'
publish_date: '16-12-2024 15:45'
---


---
title: 'Politique en matière de Cookies'
date: '05-10-2023 12:42'
publish_date: '05-10-2023 12:42'
visible: true
---

<div id="policy"></div>
#POLITIQUE EN MATIÈRE DE COOKIES
La présente politique en matière de cookies vous permet de comprendre ce que sont les cookies, les types de cookies que nous utilisons et comment nous les utilisons, c’est-à-dire quelles informations nous recueillons à l’aide de ces cookies. 
Cette politique vous indique également :
*	comment les informations recueillies sont utilisées, ainsi que toutes les informations pertinentes et nécessaires pour vous permettre de comprendre et consentir de manière éclairée à notre politique
*	si des données sont transférées à des tiers et lesquels 
*	comment gérer et supprimer les cookies
*	comment nous contacter en cas de questions

Important : vous pouvez à tout moment modifier ou retirer votre consentement à l’utilisation de cookies (cf. ci-dessous sous « comment puis-je contrôler les préférences des cookies ? »).

## Qu’est-ce que les cookies ?
Un cookie est un petit fichier texte contenant des informations, qui est enregistré sur votre appareil (ordinateur, tablette ou smartphone) lorsque vous visitez un site ou une application. Ils sont utilisés pour stocker des informations telles que vos préférences, vos sessions de connexion et d'autres données pertinentes pour améliorer votre expérience en ligne. Les cookies permettent aux sites de se souvenir de vos actions et de vos préférences au fil du temps, ce qui peut rendre votre navigation plus efficace et personnalisée.

La plupart des cookies sont supprimés de votre appareil lorsque vous fermez votre navigateur (cookies de session). D'autres restent sur votre ordinateur et sont capables de vous reconnaître si vous revenez sur le même site (cookies permanents). 
Vous pouvez bloquer l'utilisation de cookies dans votre navigateur en limitant ou en désactivant leur enregistrement et leur lecture (cf. ci-dessous sous « comment puis-je contrôler les préférences des cookies ? »).

## Quels types de cookies sont utilisés sur ce site ?
Les types de cookies suivants sont utilisés sur ce site :

### Cookies nécessaires
Les cookies nécessaires sont essentiels au bon fonctionnement de notre site. Ils garantissent des fonctionnalités de base telles que [config name="client.legal_options.has_ecommerce"]la gestion du contenu du panier, le processus de paiement, [/config]l'état des pages visitées et la navigation. Voici comment fonctionne l'utilisation de ces cookies :

[config name="client.legal_options.has_ecommerce"]
* **Fonctionnalités de base :** Les cookies nécessaires sont indispensables pour que notre site e-commerce fonctionne correctement. Ils vous permettent de naviguer sur notre catalogue, de consulter les produits, de les ajouter au panier et de finaliser vos achats en toute sécurité.
* **Connexion sécurisée :** Lorsque vous vous connectez à votre compte sur notre site, les cookies nécessaires garantissent que votre session reste active et sécurisée. Cela vous évite de devoir vous reconnecter à chaque nouvelle page.
* **Processus de paiement :** Les cookies nécessaires sont également déterminants pendant le processus de paiement car ils assurent que vos informations de paiement sont transmises de manière sécurisée et que la transaction s'effectue en toute confiance.
[/config]
* **Compatibilité :** Les cookies nécessaires garantissent que notre site fonctionne correctement sur différentes plateformes et appareils, offrant ainsi une expérience cohérente aux utilisateurs.

Les cookies nécessaires ne collectent pas d'informations personnelles identifiables. Ils ne suivent pas votre comportement de navigation en dehors de notre site, et ils sont essentiels pour vous offrir une expérience en ligne sans accroc.
Il est important de noter que ces cookies sont activés par défaut lorsque vous visitez notre site. Ils ne nécessitent pas de consentement explicite, car ils sont indispensables au bon fonctionnement du site et de ses services. 

### Cookies statistiques 
Le rôle principal des cookies statistiques est de collecter des informations anonymes sur votre comportement de navigation. Cela nous aide à mieux comprendre comment vous interagissez avec notre contenu, quelles pages vous visitez le plus fréquemment et comment vous utilisez notre site dans son ensemble.

Plus spécifiquement, nous utilisons Google Analytics, un outil d'analyse de site fourni par Google. Voici comment fonctionne l'utilisation des cookies statistiques :

* **Collecte de données anonymes :** Lorsque vous visitez notre site, Google Analytics active des cookies qui collectent des informations telles que votre adresse IP, votre type de navigateur, votre système d'exploitation et les pages que vous visitez. Ces données sont anonymes et ne permettent pas de vous identifier personnellement.

* **Analyse complète :** Les informations collectées par les cookies statistiques nous aident à obtenir une vue d'ensemble de l'utilisation de notre site. Nous pouvons ainsi savoir quelles pages sont les plus populaires, quelles sections sont moins visitées, et comment les visiteurs interagissent avec notre contenu.

* **Amélioration de l'expérience utilisateur :** En comprenant mieux vos préférences et votre comportement, nous sommes en mesure d'améliorer l'expérience utilisateur sur notre site. Nous pouvons adapter nos contenus en fonction de ce que vous appréciez le plus, ce qui nous permet de mieux répondre à vos besoins.

Il est important de souligner que les cookies statistiques ne stockent pas d'informations personnelles identifiables. Nous rappelons que les données collectées sont agrégées et anonymes.

Si vous ne souhaitez pas que Google Analytics enregistre votre comportement, nous vous recommandons de télécharger et d’installer le module suivant sur votre navigateur pour désactiver la collecte des données : [https://tools.google.com/dlpage/gaoptout?hl=fr](https://tools.google.com/dlpage/gaoptout?hl=fr) 

### Cookies marketing
Les cookies marketing servent à collecter des informations anonymes sur votre comportement de navigation et vos interactions avec notre contenu. Ces informations nous aident à mieux comprendre vos préférences et à personnaliser vos futures interactions avec notre site.

Plus spécifiquement, nous utilisons les cookies marketing pour deux objectifs principaux :

* **Suivi de campagnes publicitaires :** Lorsque vous interagissez avec nos publicités sur des plateformes telles que Google Ads et Facebook, des cookies sont activés pour enregistrer ces interactions. Cela nous permet d'évaluer l'efficacité de nos campagnes publicitaires, de mesurer le nombre de clics, de visualisations et d'actions réalisées en réponse à nos annonces. Cette information nous aide à optimiser nos stratégies de marketing et à vous proposer du contenu plus pertinent.

* **Reciblage (remarketing) :** Les cookies marketing nous permettent également de mettre en place des campagnes de reciblage. Lorsque vous visitez notre site et interagissez avec certaines pages ou produits, ces actions sont enregistrées via les cookies. Par la suite, lorsque vous naviguez sur d'autres sites ou plateformes en ligne, comme Google ou Facebook, vous pourriez voir nos annonces qui sont spécifiquement adaptées à vos intérêts, basées sur votre historique de navigation sur notre site.

Il est important de souligner que les cookies marketing ne stockent pas d'informations personnelles identifiables telles que votre nom, adresse ou informations de contact. Les données collectées sont anonymes et ne permettent pas de vous identifier directement.

## Pourquoi les cookies marketing sont-ils utiles pour nous ?
Nous utilisons ces cookies pour présenter des offres ciblées à la fois au sein de notre site et en dehors de celui-ci lorsque vous naviguez sur d'autres sites. Ces cookies peuvent également être utilisés pour déterminer si un utilisateur a visité notre site après avoir vu une de nos annonces, par exemple. L'objectif de ces cookies est d'évaluer notre audience et notre capacité à convertir votre visite en interaction, que ce soit sur notre site ou notre application ou après les avoir visités.
Les cookies de cette classification nous apportent un appui pour recenser le nombre de visiteurs sur notre site web et pour tracer l'itinéraire des utilisateurs. Cette démarche nous autorise à adapter et à perfectionner le fonctionnement de notre site en vue de faciliter l'accès aux informations que vous sollicitez.

## Comment puis-je contrôler les préférences des cookies ?
Lors de votre première visite sur notre site, vous avez la possibilité d'accepter ou de refuser certains types de cookies non essentiels en utilisant notre fenêtre de gestion des cookies.  Vous pouvez également modifier vos préférences en matière de Cookies à tout moment, grâce aux options suivantes :

[rgpd]Liste ici…[/rgpd]

Vous avez également la possibilité de refuser l’utilisation de tous les cookies via les paramètres de votre navigateur.  Veuillez noter que la désactivation des cookies peut affecter certaines fonctionnalités de notre site (voir point précédent « Cookies nécessaires »).

### Paramétrage des cookies sur votre navigateur
Pour Google Chrome : 
[https://support.google.com/chrome/answer/95647?hl=fr](https://support.google.com/chrome/answer/95647?hl=fr)

Pour Internet Explorer : 
[https://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-Cookies](https://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-Cookies)

Pour Firefox : 
[https://support.mozilla.org/fr/kb/cookies-informations-sites-enregistrent](https://support.mozilla.org/fr/kb/cookies-informations-sites-enregistrent)

Pour Opéra : 
[http://help.opera.com/Windows/10.20/fr/cookies.html](http://help.opera.com/Windows/10.20/fr/cookies.html)

Pour Safari : 
[https://support.apple.com/guide/safari/enable-cookies-ibrw850f6c51/mac](https://support.apple.com/guide/safari/enable-cookies-ibrw850f6c51/mac)

Pour Arc : 
[https://thebrowser.company/privacy/](https://thebrowser.company/privacy/)

## Modifications de la politique en matière de gestion des cookies
Nous nous réservons le droit de mettre à jour cette politique en matière de gestion de cookies pour refléter les évolutions technologiques, les changements dans nos pratiques de gestion des cookies et être conformes aux exigences légales en la matière. Nous vous encourageons à consulter régulièrement cette page pour rester informé des mises à jour.

Pour toute question concernant notre utilisation des cookies, veuillez nous contacter par e-mail à l'adresse suivante : [config name="client.legal_email"/]

Nous vous remercions de votre confiance et de votre compréhension quant à notre utilisation des cookies pour améliorer votre expérience en ligne.
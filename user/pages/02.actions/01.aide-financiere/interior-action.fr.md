---
title: 'Contribution financière'
displaytitle: Contribution/financière
image: banner.jpg
meta:
    description: 'Nous apportons un soutien sous forme de contribution financière aux personnes dans le besoin dans le canton de Vaud. Découvrez comment nous pouvons vous aider.'
shortcut: true
icon: icon-shv-soleil
color: '#008bbf'
menu_visible: true
topbar_visible: false
pagetitle: "<span><span>aide</span></span>\r\n<span><span>financière</span></span>"
---

# aide financière

Afin que les personnes en détresse économique retournent à une vie normale leur permettant de subvenir elles-mêmes à leurs besoins, le Secours d'hiver accorde son aide financière de manière ciblée. Les bénéficiaires sont des personnes seules et des familles qui vivent près du minimum vital. Il ne saurait toutefois être question de se substituer à l'aide sociale étatique. En revanche, il s'agit de compléter de manière spécifique les prestations des pouvoirs publics. Toutefois, le Secours d'hiver ne saurait accorder son aide pendant une période illimitée.

Voici comment le Secours d'hiver aide à surmonter des difficultés momentanées:

* Par des contributions financières
* En réglant des factures intempestives (en collaboration avec d'autres œuvres d'entraide)
* Sous forme de bons d'achat pour des biens de première nécessité ainsi que pour des habits et des chaussures

![](aide-financiere.png?classes=none)
---
title: 'Production du Birnel'
displaytitle: 'production/du Birnel'
image: banner.jpg
meta:
    description: 'La vente du produit Birnel, ce concentré de poires à cidre cultivées en Suisse, représente une importante source de revenus pour notre œuvre d’entraide.'
shortcut: false
menu_visible: true
topbar_visible: false
pagetitle: "<span><span>le</span></span>\r\n<span><span>Birnel</span></span>"
---

# Birnel, un bienfait de la nature

Birnel, ce concentré de poires à cidre cultivées en Suisse, connaît un véritable renouveau. Remplaçant avantageusement le sucre, le concentré Birnel correspond parfaitement bien aux ambitions d'une cuisine moderne, rompue à une alimentation équilibrée: il est universel à l'emploi, particulièrement sain et, de surcroît, d'un prix extrêmement avantageux.

Pour produire un kilo de Birnel, on a besoin de dix kilos de poires à cidre, fruits qui auront mûri sur des arbres hautes tiges et pour lesquels les cultivateurs auront renoncé à tout sulfatage. Les poires sont pressées, leur jus décanté, filtré, déacidifié et, finalement, concentré. Il en résulte un produit cent pour cent naturel à haut potentiel énergétique: environ 600 grammes de fructose de haute qualité ainsi que de précieux sels minéraux. Par conséquent, Birnel nourrit, donne des forces, est digeste et stimule le métabolisme. Les diabétiques aussi peuvent consommer Birnel dont 13 grammes correspondent à une unité de pain.

![](birnel-002.jpg)

### Un arôme délicieux, une utilisation variée

Grâce à son subtil arôme propre au Birnel – un mélange de parfums de poire, de caramel et de miel – le concentré se prête particulièrement bien pour tartiner le pain. Au muesli, il confère un attrait supplémentaire et rehausse l'arôme de nombreux desserts aux fruits, tartes et biscuits. D'autre part, Birnel remplacera le sucre pour bien d'autres occasions encore: en effet, 60 grammes de concentré correspondent environ à la capacité sucrante de 100 grammes de sucre. L'universalité de Birnel ressort du grand nombre de recettes originales pour lesquelles ce concentré peut être utilisé. Celles-ci ont d'ailleurs été réunies dans une brochure qui est mise gratuitement à la disposition des personnes intéressées.

![](birnel-001.jpg)

### Un service du Secours d'hiver

Depuis 1952, le Secours d'hiver vend le produit Birnel à un prix favorable. Pour notre œuvre d'entraide, ce commerce est une importante source de revenus qui aide à soutenir son travail. Bien que le produit soit en vente toute l'année, la campagne est toujours lancée en automne; de nombreuses communes, des magasins spécialisés et des personnes privées la soutiennent. 

Pour plus d'informations, n'hésitez pas à consulter notre page: 
[https://www.secours-d-hiver.ch/aider/le-birnel-du-secours-dhiver-aide](https://www.secours-d-hiver.ch/aider/le-birnel-du-secours-dhiver-aide?target=_blank)
---
title: 'Parrainage d''une famille'
displaytitle: 'Parrainage/d''une famille'
image: banner.jpg
meta:
    description: 'Nous apportons un soutien sous forme de parrainage aux familles dans le besoin dans le canton de Vaud. Découvrez comment nous pouvons vous aider.'
shortcut: true
icon: icon-shv-maison
color: '#006188'
menu_visible: true
topbar_visible: true
pagetitle: "<span><span>parrainage</span></span>\r\n<span><span> d'une famille</span></span>"
---

# parrainage d'une famille vaudoise

Il arrive que certaines familles aient besoin d'un soutien régulier, parfois durant quelques mois,  mais parfois sur une période un peu plus longue, notamment pour les familles avec plusieurs enfants  ou les familles monoparentales.

Des  donateurs sont intéressés à soutenir plus spécifiquement une famille, sur un plus long terme afin de concrètement les aider à passer un cap difficile.

De cette manière l'aide est un peu plus personnalisée tout en garantissant la dignité des bénéficiaires puisque le SHV fait le relais entre eux et le parrain ou la marraine.

Aux donateurs intéressés il est soumis un choix descriptif de familles ayant besoin d'être soutenues. Ces familles pourront, par notre entremise, donner des nouvelles, envoyer des photos, envoyer des dessins d’enfants si le parrain/la marraine le souhaite.

Plutôt que de verser de l'argent cash nous ferions parvenir aux bénéficiaires des cartes d'achats de nourriture ( type COOP ou Migros )  ou alors le paiement d'une ou de plusieurs factures suivant le budget à disposition et les besoins.

Vous trouverez ci-joint un coupon d'inscription qui  vous permette de vous déterminer sur la formule qui vous convient.

Vous avez tout loisir aussi d’indiquer le montant de votre choix, en sachant que CHF 50,00 montant minimum pour que cette action soit efficace.

L’engagement de ce soutien sera de deux ans renouvelables automatiquement sans avis contraire écrit. Le minimum de deux ans est nécessaire car nous devons mettre en place une infrastructure pour la gestion et le suivi de ce parrainage et aussi pour la perspective des familles.  Les frais administratifs découlant de cette action sont pris en charge par le fonctionnement normal de notre bureau.

Nous vous rappelons que les dons envoyés à SHVaud, y compris pour cette forme de parrainage, sont déductibles des impôts et qu’une attestation  annuelle est  envoyée.

Nous avons d’ailleurs dévolu à cette action un compte spécifique le 10-220458-3.

Alors, A l’ACTION, remplissez le formulaire : je veux devenir parrain ou marraine d’une famille vaudoise.

![](parrainage-famille.png?classes=none)
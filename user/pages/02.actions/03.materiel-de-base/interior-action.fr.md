---
title: 'Fourniture de materiel de base'
displaytitle: 'Fourniture/de materiel de base'
image: banner.jpg
meta:
    description: 'Nous apportons un soutien sous forme de matériel de base aux personnes dans le besoin dans le canton de Vaud. Découvrez comment nous pouvons vous aider.'
shortcut: true
icon: icon-shv-lit
color: '#004159'
menu_visible: true
topbar_visible: false
pagetitle: "<span><span>matériel</span></span>\r\n<span><span> de base</span></span>"
---

## Fourniture de matériel de base
### Lits

Grâce à l'«Action lits», le Secours d'hiver permet à des familles et à des personnes seules aux revenus très modestes de se procurer des lits. Il s'agit donc là d'une aide des plus concrètes pour de véritables nécessiteux dont le budget ne permet pas de se procurer ce mobilier essentiel.

Les lits que le Secours d'hiver met à disposition sont des modèles pour enfants, superposés ou pour adultes. Ils comprennent les bois de lits et matelas de bonne qualité, fabriqués en Suisse, ainsi que la literie et deux garnitures de linge de lit.

Parfois, le Secours d'hiver est seulement sollicité pour la fourniture d'une partie de ces articles.

### Vêtements

L'action vêtements permet au Secours d'hiver de fournir aux familles et personnes seules ne disposant que de très peu de revenus des vêtements et des chaussures. Il s'agit donc d'une aide concrète au bénéfice des nécessiteux dont le budget ne se voit pas grevé de dépenses pour d'urgents besoins vestimentaires alors que d'autres biens de première nécessité font défaut.

Le Secours d'hiver dispose ainsi d'un large assortiment de vêtements et de chaussures pour adultes, enfants et nourrissons. Les colis contenant les habits commandés sont préparés et expédiés par le Centre de tri de vêtements Caritas.

<!--
Dans d'autres cas, le Secours d'hiver remet des bons permettant aux bénéficiaires de s'approvisionner directement ou par correspondance dans les magasins de vêtements Charles Vögele Mode SA ainsi que dans les magasins de chaussures Karl Vögele SA.

Le Secours d'hiver reçoit volontiers des dons sous forme d'habits, de literie, de linges éponge et de jouets. 
Ils peuvent être adressés au: 

<p class="address">
<strong>Secours suisse d'hiver</strong><br>
c/o Centre de tri de vêtements Caritas<br>
Waldibrücke<br>
6032 Emmen
</p>
-->

![](founiture-materiel.png?classes=none)
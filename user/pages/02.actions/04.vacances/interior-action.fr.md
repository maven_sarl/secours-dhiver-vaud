---
title: 'Organisation de vacances Reka'
displaytitle: 'Organisation/de vacances reka'
image: banner.jpg
meta:
    description: 'Nous apportons un soutien sous forme d’organisation de vacances aux personnes dans le besoin dans le canton de Vaud. Découvrez comment nous pouvons vous aider.'
shortcut: true
icon: icon-shv-valise
color: '#009ddc'
menu_visible: true
topbar_visible: false
pagetitle: "<span><span>sorties</span></span>\r\n<span><span> vacances reka</span></span>"
---

## Organisation de vacances Reka

L'aide aux vacances Reka est destinée aux familles monoparentales ou autres, dont le budget ne le leur permettrait normalement pas de s'offrir des vacances en Suisse, et ceci à des conditions particulièrement avantageuses. 

En effet, ce sont précisément les personnes en situation de détresse qui risquent l'isolement social. Face au poids des difficultés auxquelles elles doivent faire face, un moment de répit et de dépaysement constitue une contribution particulièrement bénéfique à la cohésion de ces familles. 

![](organisation-vacances.png?classes=none)

### Qui peut faire cette demande ?

Les familles monoparentales ou biparentales qui n'ont encore jamais pu bénéficier de vacances Reka gratuites ou dont les dernières vacances gratuites Reka remontent à plus de deux ans.

Les personnes en question doivent habiter en Suisse depuis deux ans au minimum et avoir au moins un enfant. Le revenu annuel maximal du ménage (salaire net selon certificat de salaire) ne dépassera pas Fr. 52'000.– dans le cas de familles biparentales, ni Fr. 45'000.– pour les familles monoparentales. Pour chaque enfant à partir du deuxième, le revenu annuel maximal doit être majoré de Fr. 5'000.–.

Sur demande, la Reka peut accorder des séjours de vacances d'une ou de deux semaines dans un de ses appartements ou villages de vacances en Suisse. En plus du séjour, la Reka prend également à sa charge les frais de voyage à hauteur de Fr. 50.– par membre de la famille, sous la forme de chèques Reka. La part des frais que les bénéficiaires devront prendre en charge eux-mêmes est un montant forfaitaire de Fr. 100.– par semaine de vacances ainsi que les dépenses courantes pour l'alimentation et l'argent de poche.

### Délais de traitement des demandes

* Pour l'été au plus tard le 15 juin
* Pour Noël-Nouvel An le 30 novembre
* Pour les vacances de février au plus tard le 20 janvier
* Pour Pâques le 28 février.

---
title: 'Nos actions'
displaytitle: nos/actions
image: banner.jpg
meta:
    description: 'Nous apportons un soutien sous forme de contribution financière et matérielle aux habitants du canton de Vaud dans des situations de détresse. Découvrez nos actions.'
shortcut: true
menu_visible: true
topbar_visible: false
actions:
    -
        ref: parrainage
        icon: icon-shv-maison
        title: 'Parrainage/d''une famille vaudoise'
        color: '#006188'
    -
        ref: fourniture
        icon: icon-shv-lit
        title: 'Fourniture/de matériel de base'
        color: '#004159'
    -
        ref: vacances
        icon: icon-shv-valise
        title: 'Organisation/de vacances'
        color: '#009ddc'
    -
        ref: contribution
        icon: icon-shv-soleil
        title: Contribution/financière
        color: '#008bbf'
pagetitle: "<span><span>nos</span></span>\r\n<span><span>actions</span></span>"
pagesubtitle: actions
---

## nos actions <span>en cas de détresse dans les familles vaudoises</span>

Le Secours d'hiver intervient en Suisse lorsqu'il s'agit de faire à des situations extrêmes causées par la précarité. Il aide là où les pouvoirs publics ne peuvent pas intervenir ou lorsque leurs prestations ne suffisent pas. Lorsque des personnes seules ou des familles sont en prise avec un désespoir profond, le Secours d'hiver accorde son aide sous forme de:

* Contributions financières et règlement de factures inopinées
* Remise de bons d'achat pour biens de première nécessité
* Aide en nature sous forme de lits, d'habits, de machines à coudre, ...
* Arrangement de vacances Reka gratuites pour les familles
* Conseils et informations concernant d'autres possibilités d'obtenir de l'aide

Ce sont les directives concernant l'activité d'entraide du Secours d'hiver qui règlent les procédures pour l'obtention de prestations d'aide ainsi que les conditions préalables qui doivent être remplies. Dans son travail d'entraide, le Secours d'hiver part en effet de principes communs à toutes ses organisations cantonales et antennes locales.

À la base de son action, toutes les collaboratrices, tous les collaborateurs du Secours d'hiver doivent faire preuve de compréhension face aux personnes qui se trouvent dans des conditions financières précaires.
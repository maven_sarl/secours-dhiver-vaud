---
title: 'A propos de nous'
media_order: '20190131_211646.jpg,avatar.png,banner.jpg,daniel-ruch.png,henriette-lederrey.png,jean-daniel-faucherre.png,prisca-buechler.png,rapport-activites-2009.pdf,rapport-activites-2010.pdf,rapport-activites-2011.pdf,rapport-activites-2012.pdf,rapport-activites-2013.pdf,rapport-activites-2014.pdf,rapport-activites-2015.pdf,rapport-activites-2016.pdf,rapport-activites-2018.pdf,stephanie-bavozet.png,veronique-hurni.png,virgilinia-boshung.png,_veronique-hurni.png,rapport-activites-2020.pdf,statuts-2020.pdf,Photo-MG.jpg,rapport-activites-2021.pdf,Secours-hiver-Vaud_rapport-performance_2020-2021.pdf,Statuts-2021.pdf,Rapport de performance 2021-2022.pdf,Michel Bruwier.jpg,Photo Michèle Gaudiche.jpg,loic-bardet.jpg,rapport-activites-2022.pdf,rapport-activites-2023.pdf,nathalie-briod.jpg,marine-pichon.png,rapport-activites-2023-2024.pdf'
displaytitle: 'A propos/de nous'
image: banner.jpg
meta:
    description: 'Découvrez notre comité et nos rapports. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
comite:
    -
        image: daniel-ruch.png
        name: 'Daniel Ruch'
        role: Président
    -
        image: 'Photo Michèle Gaudiche.jpg'
        name: 'Michèle Gaudiche'
        role: Membre
    -
        image: marine-pichon.png
        name: 'Marine Pichon'
        role: 'Secrétaire SHVaud'
    -
        image: henriette-lederrey.png
        name: 'Henriette Lederrey'
        role: Membre
    -
        image: 'Michel Bruwier.jpg'
        name: 'Michel Bruwier'
        role: 'Membre & comptable'
    -
        image: loic-bardet.jpg
        name: 'Loïc Bardet'
        role: Membre
    -
        image: nathalie-briod.jpg
        name: 'Nathalie Briod'
        role: Membre
reports:
    -
        name: 'Rapport d''activités 2023-2024'
        file: rapport-activites-2023-2024.pdf
    -
        name: 'Rapport d''activités 2022-2023'
        file: rapport-activites-2023.pdf
    -
        name: 'Rapport d''activités 2021-2022'
        file: rapport-activites-2022.pdf
    -
        name: 'Rapport d''activités 2020-2021'
        file: rapport-activites-2021.pdf
    -
        name: 'Rapport d''activités 2019-2020'
        file: rapport-activites-2020.pdf
    -
        name: 'Rapport d''activités 2018'
        file: rapport-activites-2018.pdf
    -
        name: 'Rapport d''activités 2016'
        file: rapport-activites-2016.pdf
    -
        name: 'Rapport d''activités 2015'
        file: rapport-activites-2015.pdf
    -
        name: 'Rapport d''activités 2014'
        file: rapport-activites-2014.pdf
    -
        name: 'Rapport d''activités 2009'
        file: rapport-activites-2009.pdf
    -
        name: 'Rapport d''activités 2013'
        file: rapport-activites-2013.pdf
    -
        name: 'Rapport d''activités 2012'
        file: rapport-activites-2012.pdf
    -
        name: 'Rapport d''activités 2011'
        file: rapport-activites-2011.pdf
    -
        name: 'Rapport d''activités 2010'
        file: rapport-activites-2010.pdf
status:
    name: 'Statuts 2021'
    file: Statuts-2021.pdf
reviews:
    -
        name: 'Rapport 2020-2021'
        file: Secours-hiver-Vaud_rapport-performance_2020-2021.pdf
    -
        name: 'Rapport 2021-2022'
        file: 'Rapport de performance 2021-2022.pdf'
menu_visible: true
topbar_visible: true
date: '31-10-2023 08:47'
publish_date: '31-10-2023 08:47'
sitemap:
    lastmod: '30-07-2024 09:03'
---

## Qui sommes-nous ?

Le Secours d'hiver Vaud soutient des personnes habitant dans le canton de Vaud lorsqu'elles se trouvent dans le besoin pour des questions financières, sociales ou autres. Dans chaque cas de secours, le bon sens exige que celui-ci soit qualitativement et quantitativement adapté aux propres ressources des demandeurs et de leur entourage social. En règle générale, le Secours d'hiver n'intervient que ponctuellement et une seule fois par année comptable.

## Comité du secours d'hiver Vaud
---
title: 'Parrainer une famille'
displaytitle: 'parrainage/d''une famille'
image: banner.jpg
meta:
    description: 'Aidez-nous et parrainez une famille. Nous soutenons les personnes habitant dans le canton de Vaud lorsqu’elles se trouvent dans le besoin.'
menu_visible: true
topbar_visible: false
---

## Comment fonctionne le parrainage ?

Vous souhaitez soutenir une famille vaudoise ?

Il suffit pour cela de choisir sur le formulaire ci-dessous le type de famille et le montant mensuel que vous souhaitez.

Une fois ce formulaire rempli, il vous faut nous le retourner au bureau de Prilly.  En fonction de votre choix nous proposerons une famille qui a besoin d’être soutenue. Celle-ci écrira un courrier pour se présenter.

Tous les courriers passent par le SHV et sont anonymisés.

Tous les trois mois la famille reçoit des cartes d’achats d’une valeur égale aux versements.

La marraine ou le parrain peut verser son don mensuellement ou alors pour l’année.

L’engagement de ce soutien durera deux ans renouvelables automatiquement sans avis contraire écrit. Le minimum de deux ans est nécessaire car nous devons mettre en place une infrastructure pour la gestion et le suivi de ce parrainage et aussi pour la perspective des familles.  Les frais administratifs découlant de cette action sont pris en charge par le fonctionnement normal de notre bureau.

Nous vous rappelons que les dons envoyés à SHVaud, y compris pour cette forme de parrainage, sont déductibles des impôts et qu’une attestation  annuelle est  envoyée.

Nous avons d’ailleurs dévolu à cette action un compte spécifique le 10-220458-3.

Alors, A l’ACTION, remplissez le formulaire ci-dessous : [je veux devenir parrain ou marraine d’une famille vaudoise](secours_d_hiver_coupon.pdf?target=_blank).

---
title: 'Faire du bénévolat'
displaytitle: 'faire/du bénévolat'
image: banner.jpg
meta:
    description: 'Aidez-nous et faites du bénévolat chez le Secours d’hiver. Nous soutenons les personnes habitant dans le canton de Vaud lorsqu’elles se trouvent dans le besoin.'
menu_visible: true
topbar_visible: true
pagetitle: "<span><span>parrainage</span></span>\r\n<span><span> d'une famille</span></span>"
---

# bénévolat

Le comité est composé d’une équipe énergique, efficace, sympathique et soudée.

Nous sommes toujours intéressé par la venue de nouveaux bénévoles afin d’assurer quelques missions.

Parmi celle-ci des personnes très à l’aise dans les points suivants :
* Comptabilité
* Informatique ( dépannages ordinateurs et imprimantes, créations de flyers, mise en page de rapports d’activités etc )
* Juriste qui pourrait se mettre à disposition des bénéficiaires un demi-jour  par mois
* Coiffure, esthétique et styliste ongulaire  qui pourraient se mettre à disposition pour quelques bénéficiaires à petits prix
* Renfort lors des tenues de stands
* Accompagnant lors de sorties en car
* Accompagnant ponctuel lorsque des bénéficiaires doivent aller acheter un meuble ( nous ne donnons pas de cash au bénéficiaire )
* Préparation de notre journée : action de Noël

Toute personne intéressée peut prendre contact par courriel afin que nous puissions vous rencontrer.

Merci de votre intérêt.
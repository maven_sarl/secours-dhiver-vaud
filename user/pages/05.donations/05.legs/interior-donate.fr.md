---
title: 'Faire un legs'
displaytitle: 'faire/un legs'
image: banner.jpg
meta:
    description: 'Aidez-nous et faites un legs au bénéfice du Secours d’hiver. Nous soutenons les personnes habitant dans le canton de Vaud lorsqu’elles se trouvent dans le besoin.'
menu_visible: true
topbar_visible: false
---

## Legs <span>au bénéfice d’une organisaton d’entraide</span>

Vous souhaitez soutenir nos familles dans le besoin par-delà la vie et ainsi faire une ultime bonne action ? Pensez au legs ! Vous aiderez ainsi les familles vaudoises qui ont été moins favorisé par l’existence à travers les actions de notre fondation.

Vous trouverez toutes les informations pratiques dans cette [fiche technique](legs.pdf?target=_blank).

---
title: 'pourquoi je donne ?'
stretch: '0'
tag: section
columns:
    -
        usecontent: '0'
        image: donation.png
        width: w-side
        class: content
    -
        usecontent: '1'
        width: w-main
        class: content
        content: "## pourquoi je donne ?\r\n\r\n**La vie prend parfois des chemins difficiles qui peuvent mener soudainement à la précarité. l'on peut avoir une vie réglée comme du papier à musique et se réveiller un matin assommé par des difficultés financières ingérables sans un soutien ponctuel.**\r\n\r\nNous effectuons ce soutien auprès de nos familles vaudoises, qui ont un travail et qui n'ont droit à quasiment aucune aide car leurs revenus se situent juste au-dessus des barèmes sociaux. Ces familles se retrouvent finalement plus pauvres que celles soutenues par le social, une fois leurs paiements mensuels effectués.\r\n\r\nDepuis de longues années, le Secours d'hiver Vaud travaille sur le terrain, analysant chaque demande déposée. Nous établissons un premier contact avec ces personnes et leur demandons des compléments d'informations, nécessaires pour la prise de décisions concernant les prestations qui leur seront accordées. Nos aides sont variées et s'adaptent aux besoins les plus divers. Elles peuvent être ponctuelles ou s'étaler sur plusieurs mois si besoin.\r\n\r\n"
size: flex-stretch
---


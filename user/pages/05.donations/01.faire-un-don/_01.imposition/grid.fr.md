---
title: 'le saviez-vous ?'
class: know
stretch: '0'
tag: section
columns:
    -
        usecontent: '1'
        width: w-main
        class: content
        content: "## le saviez-vous ?\r\n\r\n### Déduction à faire valoir à partir d’un don de CHF 100.-\r\nSi vous avez donné 100 francs ou plus au cours d’une année, vous pouvez déduire les dons de charité dans votre déclaration d’impôts. Le montant de vos impôts fédéraux, cantonaux et municipaux en seront réduits. Les dons peuvent être déduits du revenu imposable au titre tant de l’impôt fédéral direct que de l’impôt cantonal et communal. Les entreprises peuvent déduire de leur revenu imposable leurs dons en espèces ou sous forme d’autres valeurs patrimoniales en faveur de personnes morales exonérées de l’impôt pour buts d’utilité publique ou de service public dont le siège est en Suisse.\r\n\r\n### Dons aux organisations ayant obtenu le label de qualité ZEWO.\r\nArt. 37 lit. i StG VD,\_Art. 95 al. 1 let. c\r\nPersonnes physiques: jusqu’à 20% du revenu net diminué des déductions prévues à condition que ces dons s’élèvent au moins à 100 CHF. Personnes morales: jusqu’à 20% du bénéfice net. \r\n"
    -
        usecontent: '1'
        image: dons-info.png
        width: w-side
        class: content
        content: "## offrez-vous une belle visibilité !\r\n\r\n** Que vous soyez un donateur privé ou une entreprise, vous aurez, dans le cadre d'un don supérieur ou égal à CHF 500.-, la possibilité de figurer dans notre page \"[donateurs](/donateurs)\". **"
published: true
routable: false
visible: true
size: flex-stretch
---


---
title: 'Faire un don'
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
displaytitle: 'Faire/un don'
image: banner.jpg
meta:
    description: 'Aidez-nous financièrement et faites un don. Nous soutenons les personnes habitant dans le canton de Vaud lorsqu’elles se trouvent dans le besoin.'
menu_visible: false
topbar_visible: false
---

## Virement online <span>paiement sécurisé via Paypal</span>

Chères donatrices, Chers donateurs,

Malgré le grand âge de notre association, notre énergique comité se met à la page et embrasse les nouvelles habitudes et notamment celle du don en ligne.
Un clic sur notre site et c’est une famille vaudoise qui verra concrètement un effet.

** Donner ne soulage pas notre conscience mais donner permet d’engager une action concrète de soutien à son prochain. **

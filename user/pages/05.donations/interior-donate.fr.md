---
title: Dons
displaytitle: Dons
image: banner.jpg
meta:
    description: 'Aidez-nous financièrement et faites un don. Nous soutenons les personnes habitant dans le canton de Vaud lorsqu’elles se trouvent dans le besoin.'
menu_visible: false
topbar_visible: false
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---

## Aidez-nous financièrement <span>et contribuez à nos actions</span>

Chères donatrices, Chers donateurs,

Savez-vous que la précarité touche 590'000 personnes en Suisse selon OFS ?

Certaines familles vaudoises n’arrivent tout simplement plus à assumer une facture imprévue.

L’Office fédéral de la statistique a mis en évidence des données inquiétantes : un quart des familles avec un ou plusieurs enfants n’ont pas de réserves financières suffisantes pour assumer une dépense imprévue de CHF 2'000,00.

Le Secours d’hiver Vaud, actif depuis 80 ans cette année, constate aussi que les familles qui font appel à nous se trouvent réellement dans une situation délicate sitôt qu’une facture imprévue tombe. Nous avons aussi constaté qu’un simple imprévu de CHF 300,00 à CHF 500,00 est juste impossible à assumer en une fois ( par exemple : solde décompte chauffage, camps pour les enfants, diverses factures liées à la santé etc ).

Le Secours d’hiver Vaud agit sur divers plans pour soutenir des familles vaudoises qui se trouvent dans une situation délicate, dans le but justement d’éviter que celles-ci ne basculent dans les dettes et de faire en sorte ainsi que ces familles gardent la tête " hors de l’eau ". D’éviter aussi, si nous avons les moyens financiers nécessaires, des expulsions d’appartements, car il arrive que des familles utilisent un mois l’argent du loyer pour des factures urgentes et malheureusement n’arrivent pas à rattraper.

Il nous tient à cœur aussi que les enfants issus de ces familles se trouvant dans la précarité ( estimation Caritas : 260'000 en Suisse ) souffrent le moins possible de ces situations notamment en leur permettant l’accès au sport, aux activités créatrices, aux camps, en été ou en hiver, ou encore tout simplement à avoir des habits adaptés aux saisons et une nourriture saine.

Ces actions sont possibles grâce à vos dons seulement car le Secours d’hiver n’a pas de subventionnement étatique et nous sommes reconnaissants de votre fidèle soutien, qui nous permet de mener à bien toutes ces actions importantes envers une population fragilisées.

---
title: Medias
media_order: 'affiche-2017.jpg,affiche-2019.jpg,affiche_2007.jpg,affiche_2008.jpg,affiche_2009.jpg,affiche_2010.jpg,affiche_2011.jpg,affiche_2012.jpg,affiche_2013.jpg,affiche_2014.jpg,affiche_2015.jpg,affiche_2016.jpg,banner.jpg,affiche-2020.jpg,affiche_2021.jpg,banner-2021.jpg,article_generations_septembre_2022.pdf,secours-hiver-vaud_generations_sept22_Page_1.jpg,logo_lfm.svg,article_broye_novembre_22.pdf,article_broye_novembre_22.jpg,article_moudon_magazine_octobre_2024.pdf'
image: banner.jpg
meta:
    description: 'Découvrez nos médias. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
campaigns:
    -
        image: logo_lfm.svg
        url: 'https://avenue.argusdatainsights.ch/Article/AvenuePDFClip?artikelHash=825b558c3fd1425abe52dd21a6b0ad98_B7C5BB8ABF1427D84CC869FBB5B4DAE1&artikelDateiId=426764502'
        title: 'Interview avec LFM du 28 octobre 2024'
        format: horizontal
    -
        image: article_moudon_magazine_octobre_2024.png
        document: article_moudon_magazine_octobre_2024.pdf
        title: 'Article Moudon Magazine du 25 octobre 2024'
        format: vertical
    -
        image: article_broye_novembre_22.jpg
        document: article_broye_novembre_22.pdf
        title: 'ARTICLE JOURNAL DE LA BROYE DU 24 novembre 2022'
        format: vertical
    -
        image: logo_lfm.svg
        url: 'https://www.lfm.ch/podcasts/lfm-info-le-grand-format-14102022-1830/'
        title: 'Interview du 14.10.22 avec LFM'
        format: horizontal
    -
        image: secours-hiver-vaud_generations_sept22_Page_1.jpg
        document: article_generations_septembre_2022.pdf
        title: 'Magazine Générations septembre 2022'
        format: horizontal
    -
        image: affiche_2021.jpg
        title: 'Campagne 2021'
        format: vertical
    -
        image: affiche-2020.jpg
        title: 'Campagne 2020'
        format: horizontal
    -
        image: affiche-2019.jpg
        title: 'Campagne 2019'
        format: vertical
    -
        image: affiche-2017.jpg
        title: 'Campagne 2017'
        format: vertical
    -
        image: affiche_2016.jpg
        title: 'Campagne 2016'
        format: vertical
    -
        image: affiche_2014.jpg
        title: 'Campagne 2014'
        format: horizontal
    -
        image: affiche_2015.jpg
        title: 'Campagne 2015'
        format: horizontal
    -
        image: affiche_2013.jpg
        title: 'Campagne 2013'
        format: vertical
    -
        image: affiche_2012.jpg
        title: 'Campagne 2012'
        format: vertical
    -
        image: affiche_2011.jpg
        title: 'Campagne 2011'
        format: vertical
    -
        image: affiche_2010.jpg
        title: 'Campagne 2010'
        format: vertical
    -
        image: affiche_2009.jpg
        title: 'Campagne 2009'
        format: vertical
    -
        image: affiche_2008.jpg
        title: 'Campagne 2008'
        format: vertical
    -
        image: affiche_2007.jpg
        title: 'Campagne 2007'
        format: vertical
pagetitle: ''
sitemap:
    lastmod: '18-11-2024 07:47'
date: '18-11-2024 07:48'
publish_date: '18-11-2024 07:48'
---


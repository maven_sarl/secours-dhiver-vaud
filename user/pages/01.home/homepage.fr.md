---
title: 'Aide en cas de détresse en Suisse'
media_order: 'banner-2017.jpg,banner-2019-test.png,banner-2019.png,campagne-2016.jpg,cio-a-donner.jpg,merci.png,banner-2020.jpg,Mudanzas Logo Oficial.pdf,banner-2021.jpg,banner-2021.jpg'
image: banner-2021.jpg
meta:
    description: 'Nous sommes une organisation cantonale du Secours suisse d’hiver qui apporte un soutien aux habitants du canton de Vaud dans des situations de détresse.'
menu: Accueil
menu_visible: false
topbar_visible: true
pagetitle: "<span><span>Aujourd'hui en Suisse,</span></span>\r\n        <span><span>1 famille sur 10</span></span>\r\n        <span><span>se trouve dans la précarité</span></span>"
pagesubtitle: 'super génial'
---

<!--<div style="background:#d71683; color:#fff; padding: 20px;">
<h2 style="color:#fff;">Don facile et rapide ?</h2>
<strong style="font-size: 1.2em;">Faites un don de CHF 2.- par SMS</strong><br>
Envoyez "BESOIN-VD" au 339
</div>
</br>-->

<!--
<div style="background: #f8e8ef; padding: 15px;" markdown="1">

<h2 style="color: #d7165a;">Corona: Fonds pour les familles en détresse</h2>

<p>Grâce à un généreux don de Roger et Mirka Federer, le Secours d’hiver Vaud peut immédiatement remettre des bons alimentaires aux familles avec des enfants de 0 à 18 ans ou en cas d'urgence et/ou prendre en charge les frais de garde d'enfants. Cette aide est destinée aux familles de travailleurs pauvres qui subissent une perte de revenus en raison de la situation actuelle liée au coronavirus.</p>

<p>Formulaire de demande d'aide: <a href="https://www.secoursdhivervaud.ch/user/pages/03.aide/corona-demande-aide.pdf" target="_blank">Cliquez-ici pour le télécharger et l'imprimer</a></p>
</div>
-->

# Le secours d'hiver<span>aide en cas de détresse en Suisse</span>

La vie prend parfois des chemins difficiles qui peuvent mener soudainement à la précarité. L'on peut avoir une vie réglée comme du papier à musique et se réveiller un matin assommé par des difficultés financières ingérables sans un soutien ponctuel.

Nous effectuons ce soutien auprès de nos familles vaudoises, qui ont un travail et qui n'ont droit à quasiment aucune aide car leurs revenus se situent juste au-dessus des barèmes sociaux. Ces familles se retrouvent finalement plus pauvres que celles soutenues par le social, une fois leurs paiements mensuels effectués.

Depuis de longues années, le Secours d'hiver Vaud travaille sur le terrain, analysant chaque demande déposée. Nous établissons un premier contact avec ces personnes et leur demandons des compléments d'informations.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/izlF85Na5dw" frameborder="0" allowfullscreen></iframe>

Grâce à votre générosité, nous avons évité à beaucoup de familles des difficultés de type résiliation de bail, rupture assurantielle, non accès aux cantines scolaires suite à des factures impayées ou encore des décomptes chauffages très lourds pour certains ménages.

Le Secours d'hiver intervient en Suisse lorsqu'il s'agit de faire face à des situations extrêmes causées par la pauvreté. Il aide là où les pouvoirs publics ne peuvent pas intervenir ou lorsque leurs prestations ne suffisent pas. Lorsque des personnes seules ou des familles sont en prise avec un désespoir profond, le Secours d'hiver accorde son aide sous forme de :

* Contributions financières et règlement de factures inopinées
* Remise de bons d'achat pour biens de première nécessité
* Aide en nature sous forme de lits, d'habits, de machines à coudre, ...
* Arrangement de vacances Reka gratuites pour familles monoparentales ou autres
* Conseils et informations concernant d'autres possibilités d'obtenir de l'aide

![](merci.png?classes=none)
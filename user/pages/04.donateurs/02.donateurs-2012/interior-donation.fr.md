---
title: 'Nos donateurs 2012'
displaytitle: 'nos/donateurs 2012'
meta:
    description: 'Découvrez nos donateurs de 2012. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
image: banner.jpg
companies:
    -
        logo: donateur-apco.png
        name: 'APCO Technologies SA'
        url: Aigle
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: Montreux
    -
        logo: donateur-ecol-temler-dufour.png
        name: 'E.C.O.L. Temler Dufour'
        url: Vevey
    -
        logo: donateur-eca.png
        name: ECA
        url: Lausanne
    -
        logo: donateur-fondation-leyvraz.png
        name: 'Fondation Leyvraz'
        url: Ecublens
    -
        logo: donateur-loterie-romande.png
        name: 'Loterie Romande'
        url: Lausanne
    -
        logo: donateur-sls.png
        name: 'SLS Swiss Location Services GMBH'
        url: none
persons:
    -
        name: 'Paul Balleneger'
        region: Pully
    -
        name: 'Rolf Bez'
        region: Daillens
    -
        name: 'Yves Christofel'
        region: Lausanne
    -
        name: 'Catherine et Sébastien Cruchet'
        region: 'La Sarraz'
    -
        name: 'Luis Ferreira'
        region: Prilly
    -
        name: 'Hélène Piccard'
        region: Lausanne
    -
        name: 'Mme Louise Lebel'
        region: Pully
    -
        name: 'M. Marc Gigen'
        region: Lausanne
    -
        name: 'M. Marcel Leutenegger'
        region: Chavannes-près-Renens
    -
        name: 'Christine Moichon'
        region: Renens
    -
        name: 'André Monnet'
        region: Bussigny
    -
        name: 'Géraldine et Serge Piguet'
        region: Nyon
    -
        name: 'M. Marko Steinbrenner'
        region: Echallens
    -
        name: 'Claire Zimmermann'
        region: Begnins
    -
        name: 'Marc Zuber'
        region: Morges
---

## Merci à nos donateurs 2012

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


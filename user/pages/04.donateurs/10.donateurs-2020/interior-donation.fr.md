---
title: 'Nos donateurs 2020'
displaytitle: nos/donateurs
image: banner.jpg
meta:
    description: 'Découvrez nos donateurs. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
published: true
menu_visible: true
topbar_visible: false
companies:
    -
        logo: donateur-yteqam.png
        name: 'Ytequam, Lausanne'
        url: 'http://www.yteqam.ch/'
    -
        logo: donateur-atelier-z.png
        name: 'Atelier Z, Grancy'
        url: 'https://www.atelierz.ch/entreprise/accueil/'
    -
        logo: donateur-amicale-oldtimer.png
        name: 'Amicale Oldtimer Berna, Ogens'
    -
        logo: donateur-eca.png
        name: 'ECA Vaud'
        url: 'https://www.eca.ch/'
    -
        logo: donateur-dyn.png
        name: 'Dyn Fiduciaire, Lausanne'
        url: 'https://www.eca.ch/'
    -
        logo: donateur-gaznat.png
        name: 'Gaznat SA, Vevey'
        url: 'https://www.gaznat.ch/'
    -
        logo: donateur-daniel-ruch.png
        name: 'Entreprise Daniel Ruch, Corcelles-le-Jorat'
        url: 'https://www.danielruch.ch/'
    -
        logo: donateur-fondation-ernest-matthey.png
        name: 'Fondation Ernest Matthey'
    -
        logo: donateur-commune-st-barthelemy.png
        name: 'Commune de St-Barthelemy'
        url: 'https://www.st-barthelemy.ch'
    -
        logo: donateur-commune-lutry.png
        name: 'Commune de Lutry'
        url: 'https://www.lutry.ch/'
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: 'https://www.prilly.ch/accueil.html'
    -
        logo: donateur-lions-club.png
        name: 'Lions Club Lausanne Bourg'
    -
        logo: donateur-prodemco-sa.png
        name: 'Prodemco SA, Cugy'
        url: 'http://prodemco.ch/'
    -
        logo: donateur-commune-aigle.png
        name: 'Commune d''Aigle'
        url: 'https://www.aigle.ch/'
    -
        logo: donateur-commune-montreux.png
        name: 'Commune de Montreux'
        url: 'http://www.montreux.ch/accueil/'
    -
        logo: secours-hiver-vaud-donateur-commune-lutry.png
        name: 'Commune de Lutry'
        url: 'https://www.lutry.ch/'
    -
        logo: donateur-dr-invest.png
        name: 'DR Invest SA'
        url: 'https://www.drinvest.ch/'
    -
        logo: donateur-cand-landi.png
        name: Cand-Landi
        url: 'http://www.candlandi.com/site/fr/'
    -
        logo: donateur-pharmacie-prilly.png
        name: 'Pharmacie Prilly - M. Locca'
        url: 'https://www.pharmaciedeprilly.ch/'
persons:
    -
        name: 'M. Gaillard'
        region: Begnins
    -
        name: 'Mme Guyon'
        region: Lausanne
    -
        name: 'Mme et M Cuendet'
        region: St-Sulpice
    -
        name: 'Mme Ausoni'
        region: St-Sulpice
    -
        name: 'M. Steinbrenner'
        region: Echallens
    -
        name: 'M. Monnet'
        region: Bussigny
    -
        name: 'Mme et M Freitag'
        region: Pully
    -
        name: 'Mme et M Pellet'
        region: 'La Sarraz'
    -
        name: 'Mme Ryssel'
        region: Montreux
    -
        name: 'M. Ballenegger'
        region: Pully
    -
        name: 'M. Donzel'
        region: Lausanne
    -
        name: 'Mme Zimmermann'
        region: Begnins
    -
        name: 'Mme Cuendet'
        region: St-Sulpice
    -
        name: 'Mme Temler Dufour'
        region: Gland
    -
        name: 'M. P. Deillon'
        region: ' St-Prex'
    -
        name: 'Mme et M. M et P Marillier'
        region: St-Prex
    -
        name: 'Mme et M Demaurex'
        region: St-Sulpice
    -
        name: 'M. R. Härbeck'
        region: Territet
    -
        name: 'Mme F. Lorenceau'
        region: Crans-près-Céligny
    -
        name: 'M. M Zuber'
        region: Morges
    -
        name: 'M. P. Destraz'
        region: 'La Tour-de-Peilz'
---

## Merci à nos donateurs 2020 <span>C'est grace à vous que l'on avance</span>

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


---
title: 'Nos donateurs 2017'
displaytitle: nos/donateurs
image: banner.jpg
meta:
    description: 'Découvrez nos donateurs. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
companies:
    -
        logo: donateur-ecol-temler-dufour.png
        name: 'E.C.O.L Temler Dufour'
        url: Vevey
    -
        logo: donateur-fondation-juchum.png
        name: 'Fondation Juchum'
    -
        logo: donateur-apco.png
        name: 'APCO Technologies'
    -
        logo: donateur-nagravision-sa.png
        name: 'Nagravision SA, Chesaux-sur-Lausanne'
    -
        logo: donateur-cio.png
        name: CIO
    -
        logo: donateur-eca.png
        name: ECA
    -
        logo: donateur-commune-montreux.png
        name: 'Commune de Montreux'
    -
        logo: donateur-commune-st-barthelemy.png
        name: 'Commune de St-Barthélemy'
    -
        logo: donateur-smile.png
        name: 'Future Smile, Montreux'
    -
        logo: donateur-commune-gland.png
        name: 'Commune de Gland'
    -
        logo: donateur-ingold.png
        name: 'Entreprise Ingold Fils SA, Moudon'
persons:
    -
        name: ' O. et Y.  Bataillard'
        region: Vaud
    -
        name: 'M. M. Steinbrenner'
        region: Vaud
    -
        name: 'Mme et M. R et V Helf-Bischoff'
        region: Vaud
    -
        name: 'M. V. Thalmann'
        region: Vaud
    -
        name: 'M. M. Gilgen'
        region: Vaud
    -
        name: 'Mme A. Gerlich'
        region: Vaud
    -
        name: 'Mme K. Zimmermann'
        region: Vaud
    -
        name: 'M. H. Linder'
        region: Vaud
    -
        name: 'M. P Ballenegger'
        region: Vaud
    -
        name: 'Mme et M. Bez'
        region: Vaud
---

## Merci à nos donateurs 2017 <span>C'est grace à vous que l'on avance</span>

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


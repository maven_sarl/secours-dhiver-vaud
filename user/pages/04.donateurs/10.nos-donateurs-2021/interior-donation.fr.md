---
title: 'Nos donateurs 2021'
media_order: 'Logo_Retraites_Populaires.png,logo-commune-aigle.png,logos-montreux-carre-blanc.svg'
displaytitle: nos/donateurs
image: banner.jpg
meta:
    description: 'Découvrez nos donateurs. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
published: false
menu_visible: true
topbar_visible: true
companies:
    -
        logo: logos-montreux-carre-blanc.svg
        name: 'Commune de Montreux'
        url: 'https://www.montreux.ch/accueil'
persons:
    -
        name: 'Mme V. Pasteur'
    -
        name: 'M. X. Burrus'
    -
        name: 'M. E. Cochand'
    -
        name: 'M. A Donzel'
    -
        name: 'Mme et M. P et P. Davel'
    -
        name: 'Mme et M. P et S. Freitag'
    -
        name: 'M. P Grandchamp'
    -
        name: 'M. V. Thalmann'
---

## Merci à nos donateurs 2021 <span>C'est grace à vous que l'on avance</span>

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.
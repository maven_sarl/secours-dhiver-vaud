---
title: 'Nos donateurs 2016'
displaytitle: nos/donateurs
image: banner.jpg
meta:
    description: 'Découvrez nos donateurs. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
companies:
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: none
    -
        logo: donateur-commune-tour-de-peilz.png
        name: 'Commune de la Tour de Peilz'
        url: none
    -
        logo: donateur-commune-aigle.png
        name: 'Commune d''Aigle'
        url: Aigle
    -
        logo: donateur-commune-montreux.png
        name: 'Commune de Montreux'
        url: Montreux
    -
        logo: donateur-commune-st-barthelemy.png
        name: 'Commune de Saint-Barthélemy'
        url: Montreux
    -
        logo: donateur-commune-vevey.png
        name: 'Commune de Vevey'
        url: Vevey
    -
        logo: donateur-ecol-temler-dufour.png
        name: 'E.C.O.L Temler Dufour'
        url: Vevey
    -
        logo: donateur-fondation-ernest-matthey.png
        name: 'Fondation Ernest Matthey'
        url: Vevey
    -
        logo: donateur-eca.png
        name: ECA
        url: Lausanne
    -
        logo: donateur-maven.png
        name: 'Maven Sàrl'
        url: Ecublens
    -
        logo: donateur-prodemco-sa.png
        name: 'Prodemco SA'
        url: Lausanne
    -
        logo: donateur-joaillerie-tradition-sa.png
        name: 'Joaillerie Tradition'
        url: Genève
    -
        logo: donateur-edwards-lifesciences.png
        name: 'EDWARDS Lifesciences AG'
        url: none
persons:
    -
        name: 'Jennifer De Francesco'
        region: Lully
    -
        name: 'Gabriel Mobilia'
        region: Boussens
    -
        name: 'Amélie Rothfuss'
        region: Boussens
    -
        name: 'Valérie Trachsel'
        region: Chigny
    -
        name: 'Mme Ausoni'
        region: Vaud
    -
        name: 'Mme et M. Helf'
        region: Vaud
    -
        name: 'Mme et M. Modi Storhjohann'
        region: Vaud
    -
        name: 'M. Venitus'
        region: Vaud
    -
        name: 'M. Krampl'
        region: Vaud
    -
        name: 'Dresse C. Aurouze'
        region: Vaud
    -
        name: 'Rotary-Club Lausanne-Ouest'
        region: Vaud
    -
        name: 'Mme G. Scholl-Davaine'
        region: Vaud
    -
        name: 'Mme et M. P et M. Morgenthaler'
        region: Vaud
    -
        name: 'Mme et M. D et Y Rollier'
        region: Vaud
    -
        name: 'Mme K. Zimmermann'
        region: Vaud
---

## Merci à nos donateurs 2016 <span>C'est grace à vous que l'on avance</span>

**Nous recevons en moyenne plus de 3'000 dons financiers, en nature ou encore en legs par année.
Cette année 2016, nous comptons déjà plus de 300 donateurs ! Merci pour votre soutien régulier et votre aide à nos familles vaudoises.**

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


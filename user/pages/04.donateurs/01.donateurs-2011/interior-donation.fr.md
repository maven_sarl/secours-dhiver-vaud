---
title: 'Nos donateurs 2011'
displaytitle: 'nos/donateurs 2011'
meta:
    description: 'Découvrez nos donateurs de 2011. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
image: banner.jpg
companies:
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: Aigle
    -
        logo: donateur-commune-montreux.png
        name: 'Commune de Montreux'
        url: Montreux
    -
        logo: donateur-commune-ecublens.png
        name: 'Commune d''Ecublens'
        url: Vevey
    -
        logo: donateur-eca.png
        name: 'ECA Pully'
        url: Lausanne
    -
        logo: donateur-commune-st-barthelemy.png
        name: St-Barthélemy
        url: Ecublens
    -
        logo: donateur-fondation-juchum.png
        name: 'Fondation Juchum'
        url: Lausanne
persons:
    -
        name: 'Paul Ballenegger'
        region: Vaud
    -
        name: 'Rolf Bez'
        region: Vaud
    -
        name: 'Mme et Mr C et S Cruchet'
        region: 'La Sarraz'
    -
        name: 'Philippe Destraz'
        region: 'la Tour-de-Peilz'
    -
        name: 'Marc Gilgen'
        region: Vaud
    -
        name: 'Monica Kriens'
        region: Coppet
    -
        name: 'Christian Picart'
        region: Vaud
    -
        name: 'Jean Renard'
        region: Vaud
    -
        name: 'Claire Zimmermann'
        region: Vaud
---

## Merci à nos donateurs 2011

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


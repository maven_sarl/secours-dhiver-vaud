---
title: 'Nos donateurs 2013'
displaytitle: 'nos/donateurs 2013'
meta:
    description: 'Découvrez nos donateurs de 2013. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
image: banner.jpg
companies:
    -
        logo: donateur-suisse.png
        name: 'EIDG. Finanzverwaltung'
        url: none
    -
        logo: donateur-cio.png
        name: 'Comité International Olympique du CIO'
        url: none
    -
        logo: donateur-apco.png
        name: 'APCO Technologies SA'
        url: Aigle
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: Montreux
    -
        logo: donateur-ecol-temler-dufour.png
        name: 'E.C.O.L. Temler Dufour'
        url: Vevey
    -
        logo: donateur-fondation-juchum.png
        name: 'Fondation Juchum'
        url: Vevey
    -
        logo: donateur-eca.png
        name: ECA
        url: Lausanne
    -
        logo: donateur-commune-tour-de-peilz.png
        name: 'Commune de la Tour de Peilz'
        url: Ecublens
    -
        logo: donateur-loterie-romande.png
        name: 'Loterie Romande'
        url: Lausanne
    -
        logo: donateur-migros.png
        name: Migros
        url: none
    -
        logo: donateur-auer-et-fils.png
        name: 'Auer et fils Sàrl'
        url: none
    -
        logo: donateur-bio-energy-system.png
        name: 'Bio-Energy System'
        url: none
    -
        logo: donateur-edwards-lifesciences.png
        name: 'EDWARDS Lifesciences AG'
        url: none
persons:
    -
        name: 'M. Marc Gilgen'
        region: Lausanne
    -
        name: 'M. Fridolin Hefti'
        region: Villeneuve
    -
        name: 'Jean-Paul Parisod'
        region: Avenches
    -
        name: 'M. Jean-Gabriel Petit'
        region: Cugy
    -
        name: 'Mme et M. Sandra et Carlos Modi Storjohann'
        region: Coppet
    -
        name: 'M. Marko Steinbrenner'
        region: Echallens
    -
        name: 'M. Rémi Walbaum'
        region: Bussigny-près-Lausanne
    -
        name: 'Klara Zimmermann'
        region: Begnins
    -
        name: 'Christine et Marc Zuber'
        region: Morges
---

## Merci à nos donateurs 2013

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


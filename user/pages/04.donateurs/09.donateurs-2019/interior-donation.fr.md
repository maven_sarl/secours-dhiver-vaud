---
title: 'Nos donateurs 2019'
displaytitle: nos/donateurs
image: banner.jpg
meta:
    description: 'Découvrez nos donateurs. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
companies:
    -
        logo: donateur-audemars-piguet.png
        name: 'Audemars Piguet, Le Brassus'
        url: 'https://www.audemarspiguet.com/fr/'
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: 'https://www.prilly.ch/accueil.html'
    -
        logo: donateur-yteqam.png
        name: 'Yteqam SA, Lausanne'
        url: 'http://www.yteqam.ch/'
    -
        logo: donateur-edwards-lifesciences.png
        name: 'Edwards Lifesciences, Nyon'
        url: 'https://www.edwards.com/'
    -
        logo: donateur-commune-st-barthelemy.png
        name: 'Commune de St-Barthelemy'
        url: 'https://www.st-barthelemy.ch'
    -
        logo: donateur-cio.png
        name: 'Comité International Olympique'
    -
        logo: donateur-fondation-ernest-matthey.png
        name: 'Fondation Ernest Matthey'
    -
        logo: donateur-swiss-pearls.png
        name: ' Swiss Pearls SA'
    -
        logo: donateur-mover.png
        name: 'Mover Sportswear SA'
persons:
    -
        name: 'Mme Guyon'
        region: Lausanne
    -
        name: 'Mme et M Helf-Bischoff'
        region: Aigle
    -
        name: 'Mme Engel'
        region: Chavannes
    -
        name: 'Mme Dente'
        region: Pully
    -
        name: 'Mme Zimmermann'
        region: Begnins
    -
        name: 'Mme Gaudin'
        region: Bière
    -
        name: 'Mme Schweizer'
        region: Pully
    -
        name: 'M. Steinbrenner'
        region: Echallens
    -
        name: 'M. Thalmann'
        region: Rolle
    -
        name: 'Mme Ferland Muirhead'
        region: Granvaux
    -
        name: 'M. Destraz'
        region: Pully
    -
        name: 'Mme Lebel'
        region: Pully
    -
        name: 'Mme Gerlich'
        region: Corseaux
    -
        name: 'Mme Friberg'
        region: Echallens
    -
        name: 'M. Roth'
        region: 'La Sarraz'
    -
        name: 'Mme Pasteur'
        region: Blonay
    -
        name: 'Mme Ausoni'
        region: St-Sulpice
    -
        name: 'Mme et M. Monthoux'
        region: Bercher
    -
        name: 'M. Gaillard'
        region: Begnins
    -
        name: 'Mme Cuendet'
        region: St-Sulpice
    -
        name: 'M. A. Monnet'
    -
        name: 'Mme et M. Arnoux'
---

## Merci à nos donateurs 2019 <span>C'est grace à vous que l'on avance</span>

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


---
title: 'Nos donateurs 2014'
displaytitle: 'nos/donateurs 2014'
meta:
    description: 'Découvrez nos donateurs de 2014. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
image: banner.jpg
companies:
    -
        logo: donateur-adefi-sa.png
        name: 'Adefi SA'
        url: none
    -
        logo: donateur-cio.png
        name: 'Comité International Olympique du CIO'
        url: none
    -
        logo: donateur-commune-st-barthelemy.png
        name: 'Commune de St-Barthélémy'
        url: Aigle
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
        url: Montreux
    -
        logo: donateur-commune-montreux.png
        name: 'Commune de Montreux'
        url: Vevey
    -
        logo: donateur-commune-avenches.png
        name: 'Commune d''Avenches'
        url: Vevey
    -
        logo: donateur-eca.png
        name: ECA
        url: Lausanne
    -
        logo: donateur-commune-tour-de-peilz.png
        name: 'Commune de la Tour de Peilz'
        url: Ecublens
    -
        logo: donateur-fondation-ernest-matthey.png
        name: 'Fondation Ernest Matthey'
        url: Lausanne
    -
        logo: donateur-migros.png
        name: 'Pour Cent Culturel Migros'
        url: none
    -
        logo: donateur-auer-et-fils.png
        name: 'Auer et fils Sàrl'
        url: none
    -
        logo: donateur-la-forestiere.png
        name: 'La Forestière'
        url: none
    -
        logo: donateur-regie-crot-et-cie.png
        name: 'Régie Crot et Cie'
        url: none
    -
        logo: donateur-commune-savigny.png
        name: 'Vestiaire de Savigny'
        url: none
persons:
    -
        name: 'Y. Christophel'
        region: Vaud
    -
        name: 'Mme Desarnauds'
        region: Vaud
    -
        name: 'M. Leutenegger'
        region: Vaud
    -
        name: 'F. Hefti'
        region: Vaud
    -
        name: 'A. Monnet'
        region: Vaud
    -
        name: 'J-G Petit'
        region: Vaud
    -
        name: 'V. Schweizer'
        region: Vaud
    -
        name: 'M. Vannay'
        region: Vaud
---

## Merci à nos donateurs 2014

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


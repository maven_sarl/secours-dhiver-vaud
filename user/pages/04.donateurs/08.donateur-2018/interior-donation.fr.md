---
title: 'Nos donateurs 2018'
slug: donateur-2018
menu_visible: true
topbar_visible: true
companies:
    -
        logo: donateur-moreira-peinture.png
        name: 'Moreira Oliveira Peintures à Epalinges'
    -
        logo: donateur-commune-prilly.png
        name: 'Commune de Prilly'
    -
        logo: donateur-aldi.png
        name: 'Aldi Renens et Prilly'
    -
        logo: donateur-lidl.png
        name: 'Lidl Renens'
    -
        logo: donateur-fondation-casino-barriere.png
        name: 'Fondation du Casino Barrière'
    -
        logo: secours-hiver-vaud-donateur-commune-lutry.png
        name: 'Commune de Lutry'
---

## Merci à nos donateurs 2018 <span>C'est grace à vous que l'on avance</span>

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


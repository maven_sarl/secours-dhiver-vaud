---
title: 'Nos donateurs'
media_order: banner.jpg
displaytitle: nos/donateurs
image: banner.jpg
meta:
    description: 'Découvrez nos donateurs. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
companies:
    -
        logo: donateur-inner-wheel.png
        name: 'Inner Wheel, Lausanne'
        url: 'https://lausanne.innerwheel.ch/'
    -
        logo: donateur-commune-aigle.png
        name: 'Commune d''Aigle'
        url: 'https://www.aigle.ch/'
    -
        logo: donateur-commune-montreux.png
        name: 'Commune de Montreux'
        url: 'https://www.montreux.ch/accueil'
    -
        logo: donateur-retraites-populaires.png
        name: 'Retraites Populaires'
        url: 'https://www.retraitespopulaires.ch/'
    -
        logo: donateur-eca.png
        name: ECA
        url: 'https://www.eca-vaud.ch/'
    -
        logo: donateur-cio.png
        name: CIO
        url: 'https://olympics.com/cio'
persons:
    -
        name: 'Mme V. Pasteur'
    -
        name: 'M. X. Burrus'
    -
        name: 'M. E. Cochand'
    -
        name: 'M. A Donzel'
    -
        name: 'Mme et M. P et P. Davel'
    -
        name: 'Mme et M. P et S. Freitag'
    -
        name: 'M. P Grandchamp'
    -
        name: 'M. V. Thalmann'
---

## Merci à nos donateurs 2021 <span>C'est grace à vous que l'on avance</span>

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


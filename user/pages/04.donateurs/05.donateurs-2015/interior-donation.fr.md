---
title: 'Nos donateurs 2015'
displaytitle: 'nos/donateurs 2015'
meta:
    description: 'Découvrez nos donateurs de 2015. Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud.'
menu_visible: true
topbar_visible: false
image: banner.jpg
companies:
    -
        logo: donateur-electrosuisse-energie.png
        name: 'Electrosuisse Energie'
        url: none
    -
        logo: donateur-fondation-herrod-stiftung.png
        name: Herrod-Stiftung
        url: none
    -
        logo: donateur-langbow-finance-sa.png
        name: 'Longbow Finance SA'
        url: none
    -
        logo: donateur-eca.png
        name: ECA
        url: none
    -
        logo: donateur-fondation-juchum.png
        name: 'Fondation Juchum'
        url: none
    -
        logo: donateur-edwards-lifesciences.png
        name: 'EDWARDS Lifesciences AG'
        url: none
persons:
    -
        name: 'Mme Aurouze'
        region: Vaud
    -
        name: 'M. Ballenegger'
        region: Vaud
    -
        name: 'M. Berger'
        region: Vaud
    -
        name: 'M. Christophel'
        region: Vaud
    -
        name: 'M. Destraz'
        region: Vaud
    -
        name: 'M. J-G Petit'
        region: Vaud
    -
        name: 'M. Steinbrenner'
        region: Vaud
    -
        name: 'Mme Sercomanens'
        region: Vaud
    -
        name: 'Mme Siegenthaler'
        region: Vaud
    -
        name: 'Mme Zimmermann'
        region: Vaud
---

## Merci à nos donateurs 2015

Parmis ceux-ci, voici la liste des donateurs privés et professionnels qui ont effectués un don supérieur à CHF 500.- cette année.


---
title: 'Demande d''aide'
media_order: 'banner.jpg,campagne-2016.jpg,conditions-aide-vacances-reka.pdf,corona-demande-aide.pdf,demande-aide-fond-federer.pdf,demande-aide-machine-a-coudre.pdf,demande-aide-vacances-reka.pdf,demande-d''aide.pdf,demande-d''aide_2019.pdf,demande-materiel-lits.pdf,demande-materiel-lits_2018.pdf,demande-materiel-vetements.pdf,federer-2019.pdf,formulaire-activite-enfant_2020.pdf,Commande aide lits.pdf,Soutien activites enfants 2021.pdf,Information activite et sport enfant 2021.pdf,Formulaire-soutien-activites-enfants-2021.pdf,Formulaire-vetements-2021.pdf,formulaire-de-demande-2021.pdf,formulaire-fournitures-scolaires-2021.pdf,formulaire-machine-a-coudre-2021.pdf,catalogue-et-formulaire-fournitures-scolaires-2021.pdf,formulaire-commande-aide-lits-v2-2021.pdf,Formulaire fournitures scolaires 2022.pdf,catalogue-sacs-ecole-2022.pdf,Formulaire vêtements 2022.pdf,conditions-aide-vacances-reka-2025.pdf,demande-aide-vacances-reka-2025.pdf'
displaytitle: 'demande/d''aide'
image: banner.jpg
meta:
    description: 'Nous soutenons les personnes habitant dans le canton de Vaud lorsqu’elles se trouvent dans le besoin. Découvrez nos actions et faites une demande d’aide.'
menu_visible: true
topbar_visible: false
formular:
    -
        title: 'Demande d''aide'
        description: null
        files:
            -
                title: 'Formulaire principal'
                file: formulaire-de-demande-2021.pdf
            -
                title: 'Formulaire prise en charge activité enfant'
                file: Formulaire-soutien-activites-enfants-2021.pdf
    -
        title: 'Matériels de base'
        description: null
        files:
            -
                title: 'Commande de lits'
                file: formulaire-commande-aide-lits-v2-2021.pdf
            -
                title: 'Commande de vêtements'
                file: 'Formulaire vêtements 2022.pdf'
            -
                title: 'catalogue sacs d''école'
                file: catalogue-sacs-ecole-2022.pdf
            -
                title: 'Commande de fournitures scolaires'
                file: 'Formulaire fournitures scolaires 2022.pdf'
            -
                title: 'Commande de machines à coudre'
                file: formulaire-machine-a-coudre-2021.pdf
    -
        title: 'Vacances Reka'
        description: "Délais de traitement pour toute demande de vacances REKA :\n\n* Pour l'été au plus tard le 15 juin\n* Pour Noël-Nouvel An le 30 novembre\n* Pour les vacances de février au plus tard le 20 janvier\n* Pour Pâques le 28 février"
        files:
            -
                title: 'Demande d''aide aux vacances Reka'
                file: demande-aide-vacances-reka-2025.pdf
            -
                title: 'Conditions d''aide aux vacances Reka'
                file: conditions-aide-vacances-reka-2025.pdf
    -
        title: 'Soutien des activités des enfants'
        description: 'Le Secours d''hiver peut prendre en charge des activités para-scolaires des enfants.'
        files:
            -
                title: 'Demande d''aide et infos'
                file: 'Information activite et sport enfant 2021.pdf'
pagetitle: "<span><span>demandes</span></span>\r\n<span><span>d'aide</span></span>"
categories:
    -
        name: 'Formulaire A'
        file: IBO-wireframe.pdf
    -
        name: 'Formualire B'
        file: school-schedule.pdf
date: '13-11-2023 14:42'
publish_date: '13-11-2023 14:42'
sitemap:
    lastmod: '09-12-2024 12:49'
---

## Demande d'aide <span>Avant propos</span>

Le Secours d'hiver soutient des personnes habitant en Suisse lorsqu'elles se trouvent dans le besoin pour des questions financières, sociales ou autres. Dans chaque cas de secours,
le bon sens exige que celui-ci soit qualitativement et quantitativement adapté aux propres
ressources des demandeurs et de leur entourage social. En règle générale, le Secours d'hiver
n'intervient que ponctuellement et une seule fois par année comptable.

Pour venir à l'aide en cas de budgets ménagers très étriqués, les moyens dont dispose le Secours d'hiver sont les suivants:
* **soutien financier** pour les familles avec enfants mineurs se trouvant en situation de détresse: prise en charge de factures (p.ex. opticien, dentiste, formation continue, loyer, électricité);
* **action "lits"** (fourniture de nouveaux lits pour personnes seules ou couples, lits d'enfants, lits superposés, matelas, duvets, coussins);
* **aide aux vacances gratuites Reka** pour familles biparentales ou monoparentales vivant au seuil du minimum vital.

Cette aide, pratique et concrète, est offerte tout au long de l'année et de manière non bureaucratique. Il va de soi que le Secours d'hiver choisit soigneusement les cas pour lesquels
des prestations sont octroyées. Avec le consentement préalable des demandeurs, le Secours d'hiver peut solliciter des renseignements complémentaires auprès d'une personne de confiance ou d'un office de conseils.

## Comment formuler une demande ?

La demande doit être adressée au Secours d'hiver Vaud.  Elle peut se faire directement ou par l'intermédiaire d'un autre organisme. Pour que votre demande soit complète, vous pouvez télécharger, imprimer et utiliser les formulaires ci-après.

** Pour être complète, votre demande doit contenir les renseignements suivants: **
* Remplir le formulaire de demande d'aide SHVaud _(formulaires disponibles ci-après)_
* Une lettre explicative de votre situation actuelle
* Joindre une copie de votre dernière taxation d'impôts
* Une copie de votre certificat de salaire
* Une copie de votre décompte prestations complémentaires / AI / rentes / APG
* Une copie bail à loyer et son bv
* Copie permis d’établissement
* Copies factures, avec BV, impayées
* Copie de la carte grise de la voiture ou (des cartes si plusieurs) et détail leasing
* Tout documents que vous jugerez utiles, afin que nous puissions évaluer votre situation rapidement

**Obligatoire :** Suisse ou permis C pour tous les membres de la famille.<br>
Pas d'entrée en matière pour les personnes seules ou à l'AVS.

**Votre dossier doit impérativement nous être transmis par courrier, à l'adresse ci-dessous.** <font style="color: #fc8b12;">Seuls les dossiers envoyés par poste seront traités.</font>

<p class="address" style="border: 1px solid #f3a00f; background: #fffbee; color: #000;">
<strong>{{ site.name }}</strong><br>
{{ site.address.street }} {{ site.address.street_number }}<br>
{{ site.address.zipcode }} {{ site.address.city }}
</p>

** Le Secours d'hiver n'est pas autorisé à libérer les pouvoirs publics de leurs obligations
légales. C'est pourquoi notre œuvre d'entraide doit d'abord vérifier si les demandeurs
n'ont pas légalement droit à un soutien de la part des pouvoirs publics.**

Dans des cas où des besoins importants d'aide sont justifiés, le Secours d'hiver peut participer au redressement de la situation. Le cas échéant, il est utile de présenter un plan de financement qui informe également sur votre propre contribution.

Les moyens financiers de notre œuvre d'entraide sont limités. En présence d'un grand nombre de demandes, une limitation de
nos interventions peut se révéler indispensable. Le principe des prestations du Secours d'hiver, même si elles ont été accordées plus d'une fois, ne correspond à aucune prétention de droit.

Merci de faire connaître les activités du Secours d'hiver auprès de vos proches et de vos amis. En effet, notre œuvre d'entraide a besoin d'une certaine notoriété afin de pouvoir bénéficier des dons de nos sympathisants. 
---
title: 'Politique de confidentialité'
date: '05-10-2023 12:41'
publish_date: '05-10-2023 12:41'
---

<div id="policy"></div>
# Déclaration de protection des données
## 1. Généralités
Le Secours d'hiver se réjouit de votre visite sur son site internet et de l'intérêt que vous portez à son organisation. Pour nous et les 27 organisations cantonales du Secours d'hiver, le respect des dispositions relatives à la protection des données et donc la protection de vos données personnelles est une préoccupation majeure à laquelle nous accordons le plus grand soin. Nous ne faisons pas le commerce de vos adresses et nous ne louons, ni ne vendons ou n'échangeons les données que vous nous avez confiées. Nous respectons en outre les prescriptions de la ZEWO.

Dans cette déclaration de protection des données, nous expliquons comment nous collectons et traitons les données personnelles. Nous tenons compte de la loi suisse révisée sur la protection des données (nLPD) et, dans certains cas, notamment en ce qui concerne les outils d'analyse basés sur internet et autres outils de fournisseurs tiers, nous tenons également compte des dispositions du règlement général sur la protection des données de l'UE (RGPD). Les lois applicables dépendent de chaque cas particulier.  

## 2. Personne responsable de la protection des données 
Pour toute question relative à la protection des données, vous pouvez contacter notre personne de contact responsable à l'adresse suivante :  

Secours suisse d'hiver
Protection des données
Clausiusstrasse 45
8006 Zurich
protectiondonnees(at)secours-d-hiver.ch

Téléphone : 044 269 40 50

Dans le cadre des activités des organisations cantonales du Secours d'hiver, celles-ci sont conjointement responsables avec nous du traitement des données personnelles correspondantes. En cas de questions relatives à la protection des données, vous nous faciliterez le traitement des demandes en rapport avec une organisation cantonale du Secours d'hiver en vous adressant aux responsables des secrétariats cantonaux. Les adresses sont indiquées sur notre site internet www.secours-d-hiver.ch.

Veuillez noter que des déclarations de protection des données spécifiques peuvent être en vigueur pour certaines organisations cantonales du Secours d'hiver. Celles-ci peuvent être consultées sur le site internet de l’organisation cantonale.

## 3. Vos droits
Vous avez à tout moment le droit d'obtenir des informations sur l'origine, le destinataire et la finalité de vos données personnelles enregistrées, ainsi que d'exiger la rectification immédiate des données personnelles inexactes ou de compléter les données enregistrées chez nous. Vous pouvez également demander à tout moment la suppression ou le blocage de vos données personnelles enregistrées ou conservées au Secours d’hiver.

Veuillez noter que nous nous réservons le droit de faire valoir les restrictions prévues par la loi, par exemple si nous sommes obligés de conserver ou de traiter certaines données, si nous avons un intérêt prépondérant à le faire (dans la mesure où nous pouvons l'invoquer) ou si nous en avons besoin pour faire valoir des droits.

L'exercice de vos droits au sein du Secours d'hiver induit généralement que vous prouviez votre identité (en joignant une copie de votre pièce d'identité si votre identité n'est pas claire ou ne peut pas être vérifiée). Pour faire valoir vos droits, vous pouvez nous contacter à l'adresse indiquée au point 2.

Toute personne concernée a en outre le droit de faire valoir ses droits en justice ou de déposer une plainte auprès de l'autorité de protection des données compétente. L'autorité de protection des données compétente en Suisse est le Préposé fédéral à la protection des données et à la transparence (https://www.edoeb.admin.ch/edoeb/fr/home.html).

## 4. Traitement des données dans le cadre de nos prestations de services et de nos relations d’affaires 
Nous traitons les données mises à notre disposition principalement dans le cadre de nos activités, services, relations d’affaires et dans le cadre de l'exploitation de notre site internet, de nos applications et de nos logiciels.

Les données personnelles sont toutes les données qui se rapportent à une personne identifiée ou identifiable. Une personne concernée est une personne au sujet de laquelle des données personnelles sont traitées, comme l'adresse, la date de naissance, le numéro de téléphone, le numéro AVS, etc.

Le traitement comprend toute manipulation de données personnelles, quels que soient les moyens et procédés utilisés, notamment la conservation, la communication, la collecte, la suppression, la sauvegarde, la modification, la destruction et l'utilisation de données personnelles.

Vous n'êtes pas obligé de nous fournir des données personnelles. Dans le cadre d'une relation d’affaires, vous devez toutefois nous fournir les données personnelles nécessaires à l'établissement et à l'exécution de notre relation client et à l'accomplissement des obligations correspondantes. Cette mise à disposition s'applique également dans le cas de votre don. Sans ces données, nous ne serons pas en mesure de recevoir votre don, de conclure un contrat avec vous (ou avec l'organisme que vous représentez) ou de l'exécuter. Le site internet ne peut pas non plus être utilisé si certaines données permettant de garantir la circulation des données (comme l'adresse IP) ne sont pas collectées.

Si vous mettez à notre disposition des données personnelles de tiers (p. ex. des membres de votre famille ou des personnes dont vous vous occupez), vous ne nous communiquez leurs données personnelles que si vous y êtes autorisé ou si vous avez leur consentement.

## 5. Buts du traitement des données personnelles
Nous utilisons les données personnelles que nous collectons en premier lieu pour fournir nos prestations dans le cadre du soutien aux personnes touchées par la précarité en Suisse ainsi que pour la collecte de fonds, la publicité et le marketing, dans la mesure où vous ne vous êtes pas opposé à l'utilisation de vos données.

Lorsque vous vous abonnez à notre newsletter, que vous nous soutenez financièrement, que vous commandez du BIRNEL ou que vous nous envoyez des questions d'ordre général, vous décidez de nous transmettre des données personnelles à ces fins. Nous avons besoin de votre adresse e-mail pour répondre à vos demandes ou pour confirmer vos commandes. Nous n'utilisons votre numéro de téléphone que pour les demandes de renseignements.

Sur notre site internet, vous pouvez faire un don en ligne de manière sûre et simple. Vous pouvez utiliser à cet effet des cartes de débit ou de crédit. La plate-forme de paiement électronique certifiée RaiseNow a été spécialement conçue pour les organisations d'utilité publique et les œuvres de bienfaisance. Les données que vous nous fournissez dans le cadre de votre don, à savoir le montant, l'affectation de votre don et votre adresse, sont enregistrées dans notre base de données de dons. Les données de vos cartes de crédit passent exclusivement par nos partenaires de paiement, de sorte qu'elles ne peuvent pas être enregistrées chez nous.

Si vous nous avez donné votre accord pour le traitement de vos données personnelles à des fins spécifiques (par exemple lors de votre inscription à la newsletter ou dans le cadre d'un bénévolat pour le Secours d'hiver), nous traitons vos données personnelles sur la base de cet accord. Le consentement peut être révoqué à tout moment, mais sera sans effet sur le traitement des données déjà réalisé.

## 6. Sécurité des données 
Des mesures organisationnelles et techniques permettent, d'une part, de garantir la sécurité des données et, d'autre part, de protéger toutes les données, en particulier les données personnelles, contre l'accès de personnes non autorisées, contre l'abus, la destruction, la perte, les erreurs techniques, la falsification, le vol, etc. Nos collaborateurs et bénévoles des organisations cantonales et au siège du Secours d’hiver sont régulièrement formés sur le thème de la protection des données et sur le traitement sûr et confidentiel des données des clients. Ils sont en outre tenus à la confidentialité et signent dans leur contrat de travail ou dans un accord de protection des données une déclaration d'engagement à respecter la protection des données et à préserver la confidentialité des données personnelles.

## 7. Durée de conservation des données personnelles 
Nous traitons et enregistrons vos données personnelles aussi longtemps que cela est nécessaire pour remplir nos obligations contractuelles et légales ou pour atteindre les objectifs nécessaires à la finalisation d’un contrat. C'est-à-dire pendant toute la durée de la relation d'affaires (de l'initiation, du déroulement et de la fin d'un contrat) et au-delà, conformément aux obligations légales de conservation de documents. Nous respectons ces obligations légales qui sont en général de 10 ans. Passé ce délai, vos données personnelles sont effacées ou rendues anonymes.

En tant que donateur ou bienfaiteur, vous avez le droit de faire effacer vos données liées à un don. Dans ce cas, nous ne les supprimons pas, mais nous les rendons inactives afin de garantir que vos données ne soient pas réintégrées dans notre base de données de dons par d'autres canaux (p. ex. achat d'adresses). Si vous demandez expressément leur suppression, nous ne pouvons pas garantir qu'elles ne seront pas réintégrées dans notre base de données de dons par des sociétés d'adresses externes.

## 8. Transmission de données à des tiers 
Le Secours suisse d'hiver ne transmet des données personnelles à des tiers que dans le cadre de nos activités commerciales et, en règle générale, avec votre accord, lorsque ces données sont nécessaires pour vous fournir des prestations de service. Il s'agit par exemple de données pour :

* Les sous-traitants tels que les imprimeurs et les fournisseurs de services informatiques
* Les fournisseurs de nos prestations en nature, boutique en ligne
* Les banques 
* Les organisations partenaires telles que les organisations cantonales et régionales du Secours d'hiver.
* Lorsque des données personnelles sont traitées en notre nom (p. ex., envoi d'appels aux dons), nous concluons un accord de traitement des données de commande avec le prestataire de services et échangeons soit les données de manière cryptée soit sur des réseaux sécurisés.

## 9. Transfert de données personnelles vers des pays tiers sans un niveau adéquat de protection des données 
À une exception près nous ne transmettons en principe pas de données personnelles à des tiers en dehors de la Suisse et de l'UE. S'il devait arriver exceptionnellement que nous transmettions des données à un pays tiers pour une prestation de service spécifiquement souhaitée, nous nous assurons que des mesures de protection des données appropriées ont été prises du côté de notre partenaire contractuel. 

## 10. Cookies
Nous utilisons des cookies sur nos pages internet afin de rendre notre offre plus conviviale, plus efficace et plus sûre.

Vous pouvez configurer votre navigateur de manière à être informé de l'installation de cookies et à pouvoir les exclure. Nous attirons toutefois votre attention sur le fait que la désactivation des cookies peut limiter les fonctionnalités du site internet du Secours suisse d'hiver.

## 11. Analyse web 
Le présent site web utilise le Google Tag Manager, qui permet de gérer les balises du site. Ce service ne collecte pas de données à caractère personnel. Mais il déclenche d’autres services qui, de leur côté, peuvent collecter des données. Si une désactivation a été effectuée au niveau du domaine ou du cookie, elle reste valable pour toutes les balises de suivi, dans la mesure où celles-ci sont mises en œuvre au moyen de Google Tag Manager.

L’outil d’analyse publicitaire « Google Analytics » sert à l’évaluation anonymisée des données d’utilisation non personnelles (données de connexion), évaluation permettant d’identifier des tendances et d’améliorer notre offre en ligne. Cela signifie que ces données ne sont pas seulement traitées par le Secours suisse d'hiver, mais aussi par Google. Google utilisera cette information dans le but d’évaluer votre utilisation du site, de compiler des rapports concernant les activités sur le site et de fournir d'autres services relatifs à l'activité du site et à l'utilisation d'internet. Google peut également transmettre ces informations à des tiers pour autant que ceci soit prescrit par la loi ou dans la mesure où ces tiers traitent ces données sur mandat de Google. L’adresse IP transmise par votre navigateur dans le cadre de Google Analytics n’est pas fusionnée avec d’autres données de Google (voir les conditions d'utilisation de Google Analytics ci-dessous).

<table cellpadding="0" cellspacing="0" style="width:466.5" class="contenttable"> 	<tbody> 		<tr> 			<td style="vertical-align:top"><p><strong>Service</strong></p></td> 			<td style="vertical-align:top"><p><strong>But</strong></p></td> 			<td style="vertical-align:top"><p><strong>Prestataire de service</strong></p></td> 			<td style="vertical-align:top"><p><strong>Adresse</strong></p></td> 			<td style="vertical-align:top"><p><strong>Liens</strong></p></td> 		</tr> 		<tr> 			<td style="vertical-align:top"><p>Google Tag Manager</p></td> 			<td style="vertical-align:top"><p>Analyse web</p></td> 			<td style="vertical-align:top"><p>Google Inc.</p></td> 			<td style="vertical-align:top"><p>1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</p></td> 			<td style="vertical-align:top"><p><a href="https://www.google.com/intl/de/policies/privacy/%22%20%5Ct%20%22_blank" target="_blank" rel="noreferrer">Protection des données</a></p></td> 		</tr> 		<tr> 			<td style="vertical-align:top"><p>Google Analytics</p></td> 			<td style="vertical-align:top"><p>Analyse web</p></td> 			<td style="vertical-align:top"><p>Google Inc.</p></td> 			<td style="vertical-align:top"><p>1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</p></td> 			<td style="vertical-align:top"><p><a href="https://www.google.com/intl/de/policies/privacy/%22%20%5Ct%20%22_blank" target="_blank" rel="noreferrer">Protection des données</a></p></td> 		</tr> 		<tr> 			<td style="vertical-align:top"><p>Google Ads</p></td> 			<td style="vertical-align:top"><p>Analyse web</p></td> 			<td style="vertical-align:top"><p>Google Inc.</p></td> 			<td style="vertical-align:top"><p>1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</p></td> 			<td style="vertical-align:top"><p><a href="https://www.google.com/intl/de/policies/privacy/%22%20%5Ct%20%22_blank" target="_blank" rel="noreferrer">Protection des données</a></p></td> 		</tr> 		<tr> 			<td style="vertical-align:top"><p>Metricool Tag</p></td> 			<td style="vertical-align:top"><p>Marketing Automation Tool</p></td> 			<td style="vertical-align:top"><p>Metricool Software</p></td> 			<td style="vertical-align:top"><p>c / Téllez, nº 12, Entreplanta H, 28007, Madrid- Spain</p></td> 			<td style="vertical-align:top"><p><a href="https://metricool.com/privacy-policy/%22%20%5Ct%20%22_blank" target="_blank" rel="noreferrer">Protection des données</a></p></td> 		</tr> 	</tbody> </table>

## 12. Newsletter 
Si vous souhaitez recevoir la newsletter proposée sur notre site, nous avons besoin de votre prénom, de votre nom et d'une adresse e-mail ainsi que d'informations qui nous permettent de vérifier que vous êtes bien le propriétaire de l'adresse e-mail indiquée et que vous acceptez de recevoir la newsletter. Cela s'applique également si vous recevez notre newsletter par un autre moyen que notre site internet. Nous utilisons ces données exclusivement pour l'envoi des informations demandées par l'intermédiaire des sociétés Mailchimp et rapidmail, ci-après dénommées «services d'envoi». Vous trouverez de plus amples informations à ce sujet dans la section ci-dessous.

D’autre part, selon leurs propres informations, les services d’envoi peuvent utiliser ces données sous forme pseudonymisée, c’est-à-dire sans les associer à une personne, pour optimiser ou améliorer ses propres services, p. ex. pour perfectionner l’envoi et la présentation des e-mails ou à des fins statistiques pour déterminer de quels pays proviennent les destinataires. Toutefois, les services d’envoi n’utilisent pas les données de nos destinataires pour leur écrire directement et ne les transmettent pas à des tiers.

Les e-mails contiennent un dénommé « web-beacon », c’est-à-dire un fichier de la taille d’un pixel qui est appelé par le serveur du service d’envoi lors de l’ouverture d’e-mails. Des informations techniques sont d’abord collectées, comme celles portant sur le navigateur et votre système, ainsi que sur votre adresse IP et la date et l’heure d’ouverture de la newsletter. En se basant sur vos lieux d’ouverture (identifiables à l’aide de l’adresse IP) ou de vos horaires de connexion, ces informations sont exploitées pour améliorer les services en fonction des données techniques ou des groupes cibles, et des comportements de lecture. Les relevés statistiques ont également pour but de déterminer si les newsletters sont ouvertes, quand elles le sont et quels liens y ont été activés. Du point de vue technique, ces informations permettent effectivement d’identifier le ou la destinataire de l’e-mail. Mais il n’est ni dans notre intention ni dans celle du service d’envoi de surveiller des utilisateurs. Par l’exploitation de ces informations, nous cherchons bien plus à identifier les habitudes de lecture et à adapter nos contenus en conséquence, ou à envoyer des contenus correspondant aux intérêts de nos utilisatrices et utilisateurs.

Vous pouvez révoquer à tout moment votre consentement à l'enregistrement des données, de l'adresse e-mail et de leur utilisation pour l'envoi de la newsletter en cliquant sur le lien «se désinscrire» de la newsletter. La légalité des opérations de traitement des données déjà effectuées n'est pas affectée par la révocation. Les données que vous avez déposées chez nous dans le but de recevoir la newsletter sont enregistrées par nos soins jusqu'à ce que vous vous désabonniez de la newsletter. Elles seront ensuite supprimées.

<table cellpadding="0" cellspacing="0" style="width:481.7" class="contenttable"> 	<tbody> 		<tr> 			<td style="vertical-align:top"><p><strong>Services d‘envoi</strong></p></td> 			<td style="vertical-align:top"><p><strong>But</strong></p></td> 			<td style="vertical-align:top"><p><strong>Prestataire de service</strong></p></td> 			<td style="vertical-align:top"><p><strong>Adresse</strong></p></td> 			<td style="vertical-align:top"><p><strong>Liens</strong></p></td> 		</tr> 		<tr> 			<td style="vertical-align:top"><p>Mailchimp</p></td> 			<td style="vertical-align:top"><p>Plate-forme de marketing par courriel</p></td> 			<td style="vertical-align:top"><p>Rocket Science Group</p></td> 			<td style="vertical-align:top"><p>675 Ponce de Leon Ave NE, Suite 5000, Atlanta, GA 30308, USA</p></td> 			<td style="vertical-align:top"><p><a href="https://www.intuit.com/privacy/statement/" target="_blank" rel="noreferrer">Protection des données</a></p></td> 		</tr> 		<tr> 			<td style="vertical-align:top"><p>Rapidmail</p></td> 			<td style="vertical-align:top"><p>Plate-forme de marketing par courriel</p></td> 			<td style="vertical-align:top"><p>rapidmail GmbH</p></td> 			<td style="vertical-align:top"><p>Wentzingerstraße 21, 79106 Freiburg, DE</p></td> 			<td style="vertical-align:top"><p><a href="https://www.rapidmail.de/datensicherheit" target="_blank" rel="noreferrer">Protection des données</a></p></td> 		</tr> 	</tbody> </table>

Dans certains cas, nous dirigeons les destinataires de la newsletter vers les sites web des services d'envoi. Nos newsletters contiennent un lien qui permet aux destinataires de la newsletter de la consulter en ligne (en cas de problèmes d'affichage dans le programme de messagerie, par exemple). Dans ce cas, nous attirons votre attention sur le fait que des cookies sont également utilisés sur les sites web des services d'envoi et que des données personnelles sont traitées par ces derniers, leurs partenaires et les prestataires de services utilisés (par ex. Google Analytics).

## 13. Contributions sur les réseaux sociaux
Sur le site internet du Secours suisse d'hiver, vous trouverez des liens vers des contributions provenant de réseaux sociaux tels qu'Instragram, Facebook, Youtube et LinkedIn. Nous utilisons ces sites de réseaux sociaux comme moyen de communication et pour augmenter la notoriété du Secours suisse d'hiver et de ses organisations cantonales. Le Secours suisse d'hiver n’exerce aucune influence, ni n’a accès au traitement des données personnelles saisies par les utilisateurs eux-mêmes sur ces plates-formes de réseaux sociaux ou laissées par leur utilisation. Les contenus ou sites internet de tiers - y compris les sites internet de tiers qui renvoient à notre site internet ou qui l'affichent dans des cadres - sont hors de notre sphère d'influence. Le Secours suisse d'hiver décline toute responsabilité quant à l'exactitude, l'exhaustivité et la légalité des contenus et des liens avec d'autres sites internet, ainsi que pour les éventuelles offres, prestations de service et autres contenus qui y figurent. L'utilisation de tels contenus ou sites internet se fait sous votre propre responsabilité. À ce sujet, nous vous renvoyons aux déclarations de protection des données des fournisseurs respectifs.

### Options de partage sur les réseaux sociaux

Ce site internet propose des options de partage sur les médias sociaux afin de fournir des informations sur le Secours suisse d'hiver et d'augmenter son degré de notoriété. Nous considérons l'objectif publicitaire sous-jacent comme un intérêt légitime au sens de la loi sur la protection des données. La responsabilité d'une exploitation conforme à la protection des données incombe toutefois au prestataire concerné. En utilisant des options de partage ou autres, vous consentez au traitement des données de ces fournisseurs. Nous travaillons avec les fournisseurs suivants :

#### Instagram

Des fonctions d'Instagram sont intégrées à notre site internet. Ces fonctions sont proposées par Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA. Si vous êtes connecté à votre compte Instagram, vous pouvez, en cliquant sur le bouton Instagram, relier le contenu de nos pages à votre profil Instagram. Instagram peut ainsi associer la visite de nos pages internet à votre compte d'utilisateur. Nous attirons votre attention sur le fait qu'en tant que fournisseur des pages, nous n'avons pas connaissance du contenu des données transmises ni de leur utilisation par Instagram. Vous trouverez de plus amples informations à ce sujet dans la déclaration de confidentialité d'Instagram : https://about.instagram.com/fr-fr/about-us

#### Facebook 

Notre site internet utilise des fonctions de Facebook Inc, 1601 S. California Ave, Palo Alto, CA 94304, USA. Lorsque vous consultez nos pages avec des plug-ins Facebook, une connexion est établie entre votre navigateur et les serveurs de Facebook. Des données sont alors transmises à Facebook. Si vous possédez un compte Facebook, ces données peuvent y être associées. Si vous ne souhaitez pas que ces données soient associées à votre compte Facebook, veuillez vous déconnecter de Facebook avant de visiter notre site. Les interactions, en particulier l'utilisation d'une fonction de commentaire ou le clic sur un bouton «J’aime» ou «Partager», sont également transmises à Facebook. Pour en savoir plus, rendez-vous sur https://de-de.facebook.com/about/privacy

#### YouTube

Notre site internet intègre des fonctions de YouTube. YouTube appartient à Google Ireland Limited, une société de droit irlandais dont le siège est situé à Gordon House, Barrow Street, Dublin 4, Irlande, qui exploite ses services dans l'Espace économique européen et en Suisse.

Les conditions d’utilisation de otre contrat juridique avec YouTube sont disponibles sur le lien suivant : https://www.youtube.com/static?gl=de&template=terms&hl=fr. Ces conditions constituent un accord juridiquement contraignant entre vous et YouTube concernant l'utilisation des services. Les règles de confidentialité de Google expliquent comment YouTube traite et protège vos données personnelles lorsque vous utilisez c service.

#### LinkedIn

Dans le cadre de notre offre en ligne, nous utilisons les services de marketing du réseau social LinkedIn de LinkedIn Ireland Unlimited Company, Wilton Plaza, Wilton Place, Dublin 2, Ireland («LinkedIn»).

Ceux-ci utilisent des cookies, c'est-à-dire des fichiers texte qui sont enregistrés sur votre ordinateur. Cela nous permet d'analyser l'utilisation que vous faites de notre site internet. Cela nous permet par exemple de mesurer le rendement de nos annonces et d'afficher aux utilisateurs des produits qui les ont précédemment intéressés.

Sont ainsi saisies, par exemple, des informations sur le système d'exploitation, le navigateur, la page internet que vous avez consultée précédemment (URL de référence), les sites internet que vous avez visités, les offres sur lesquelles vous avez cliqué, ainsi que la date et l'heure de votre visite sur notre site internet.

Les informations générées par le cookie concernant votre utilisation de ce site internet sont transmises sous forme pseudonymisée à un serveur de LinkedIn aux États-Unis et y sont enregistrées. LinkedIn n'enregistre donc pas le nom ou l'adresse e-mail de l'utilisateur concerné. Au contraire, les données susmentionnées ne sont attribuées qu'à la personne pour laquelle le cookie a été créé. Cela ne s'applique pas si l'utilisateur a autorisé LinkedIn à traiter les données sans pseudonymisation ou s'il possède un compte LinkedIn.

Vous pouvez empêcher l'enregistrement des cookies en paramétrant votre navigateur en conséquence ; nous attirons toutefois votre attention sur le fait que, dans ce cas, il est possible que vous ne puissiez pas utiliser les fonctions de ce site internet dans leur intégralité. Vous pouvez également vous opposer à l'utilisation de vos données directement auprès de LinkedIn : https://www.linkedin.com/psettings/guest-controls.

Nous utilisons LinkedIn Analytics afin d'analyser l'utilisation de notre site internet et de pouvoir l'améliorer en permanence. Les statistiques obtenues nous permettent d'améliorer notre offre et de la rendre plus intéressante pour vous en tant qu'utilisateur. Toutes les entreprises LinkedIn ont accepté les clauses contractuelles standard afin de garantir que le transfert de données vers les États-Unis et Singapour, nécessaire au développement, à l'exécution et au maintien des services, s'effectue de manière légale. Si nous demandons le consentement de l'utilisateur, la base juridique du traitement est l'article 6, paragraphe 1, point a) du RGPD.

Informations du fournisseur tiers : LinkedIn Ireland Unlimited Company Wilton Place, Dublin 2 Ireland : accord d'utilisation et déclaration de confidentialité. 

## 14. Limite de la responsabilité
Le Secours suisse d'hiver est responsable de cette déclaration de protection des données. Les informations contenues sur notre site ont été rassemblées avec le plus grand soin. Toutefois, aucune garantie ne peut être donnée quant à l'actualité, l'exactitude et l'exhaustivité de ces informations. Pour cette raison, toute responsabilité pour d'éventuels dommages en rapport avec l'utilisation des informations contenues dans cette déclaration de protection des données est exclue. La simple utilisation de notre site internet ne donne lieu à aucune relation contractuelle avec le Secours suisse d'hiver.

## 15. Actualité et modification de la présente déclaration de protection des données Newsletter 
Nous pouvons à tout moment modifier ou adapter cette déclaration de protection des données. La déclaration de protection des données actuelle peut être consultée sur https://www.secours-d-hiver.ch/protection-des-donnees.
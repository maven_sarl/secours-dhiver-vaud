---
title: 'Journée Paella - Eté 2021'
media_order: '10.JPG,7.JPG,6.JPG,5.JPG,3.JPG,2.JPG,1.JPG,La danseuse.JPG,Danseuse et guitariste.JPG,12.JPG,11.JPG,Le président SHV.JPG,V.Hurni et H.Lederrey, comité SHV.jpg,VID-20210809-WA0010.mp4,VID-20210809-WA0007.mp4,VID-20210809-WA0005.mp4'
image: 11.JPG
taxonomy:
    category:
        - sortie
    tag:
        - famille
    author:
        - Véronique
---

En août une journée de détente pour des familles en pouvant pas partir en vacances. Le Secours d'hiver Vaud a organisé une magnifique journée dans un refuge. Le thème était espagnol avec paella, flamenco et karaoké.
Le soleil était de la partie et les parents et les enfants enchantés.
---
published: true
date: '20-06-2018 09:48'
publish_date: '20-06-2018 09:48'
visible: true
title: 'Journée de formation à Fribourg les 16 et 17 mai 2018'
image: secours_dhiver_vaud_suisse_7.jpg
imageAsBanner: '1'
headline: 'Les **16 et 17 mai 2018**, les collègues des Secours d''hiver de tous les cantons ont été conviés à participer à deux journées de formations continue à Fribourg.'
meta:
    description: 'Voici les photos et la vidéo des deux journées de formations du Secours d''Hiver Vaud à Fribourg les 16 et 17 mai dernier. Cliquez pour consulter l''article.'
---

# Information sur la pratique de la journée de Noël (organisation) # 

Les **16 et 17 mai 2018**, les collègues des Secours d'hiver de tous les cantons ont été conviés à participer à deux journées de formations continue à Fribourg.

**Les buts étaient multiples :** campagne 2018, positionnement de la marque, site web, obligations Zewo et beaucoup d'autres thèmes extrêmement instructifs.

**Dans le thème : ** Présentation des activités des autres organisations cantonales, Mme V. Hurni, secrétaire générale Vaud, a eu l'honneur de présenter l'action " Noël " qui est faite depuis de nombreuses années dans le canton de Vaud.

En Suisse, seuls 2 cantons mettent sur pieds ce type de journée, Bâle et Vaud et le but était de stimuler d'autres cantons.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ip5KnKCFkAA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

*** Légende Photo :** Samuel Schmid, Président du Secours d'hiver Suisse, ancien Conseiller Fédéral
---
title: 'Une journée récréative au bowling'
media_order: '20230610_100902.jpg,20230610_101415.jpg,20230610_101425.jpg,20230610_103611.jpg,20230610_131019.jpg,IMG-20230610-WA0008.jpg,IMG-20230610-WA0009.jpg,IMG-20230610-WA0010.jpg,IMG-20230621-WA0002.jpg,IMG-20230621-WA0003.jpg,IMG-20230621-WA0005.jpg,IMG-20230621-WA0006.jpg,IMG-20230621-WA0007.jpg'
published: true
date: '10-06-2023 19:10'
publish_date: '10-06-2023 19:10'
taxonomy:
    category:
        - événement
        - actualités
image: 20230610_100902.jpg
---

Le 10 juin 2023, le comité a accompagné des familles vaudoises pour une journée récréative au bowling.

Ce sont une trentaine de personnes ( adultes et enfants ) qui se sont retrouvées, réunies en équipe, avec un défi de taille : battre le comité !

Beaucoup de rires, de joie et de délassement. Plusieurs parents nous ont dit ne pas pouvoir offrir ce genre d’activité récréative à leurs enfants.

A l’issue des résultats, une constatation, le comité a été battu à plates coutures.

Les gagnants se sont vu offrir une vraie quille en souvenir et des cartes d’achats valable dans les supermarchés.

Enfin, tout le monde, gagnants et perdants, se sont retrouvés autour d’une bonne fondue chinoise qui a régalé tout le monde.

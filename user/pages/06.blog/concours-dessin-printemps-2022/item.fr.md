---
taxonomy:
    category:
        - actualités
    author:
        - Véronique
title: 'Concours dessin - Printemps 2022'
imageAsBanner: '1'
date: '15-03-2022 13:30'
headline: 'Découvrez les gagnants et tous les dessins collectés.'
media_order: '1. Concours dessins février 2022.jpg,2. Gagnant_Concours dessins février 2022_display.jpg,2. Gagnant_Concours dessins février 2022.jpg,3. Concours dessins février 2022.jpg,4. Concours dessins février 2022.jpg,5. Gagnant_Concours dessins février 2022.jpg,6. Concours dessins février 2022.jpg,7. Concours dessins février 2022.jpg,1. Concours dessins février 2022.jpg,2. Gagnant_Concours dessins février 2022_display.jpg,2. Gagnant_Concours dessins février 2022.jpg,3. Concours dessins février 2022.jpg,4. Concours dessins février 2022.jpg,5. Gagnant_Concours dessins février 2022.jpg,6. Concours dessins février 2022.jpg'
image: '2. Gagnant_Concours dessins février 2022_display.jpg'
---

Ce concours de dessins a été mis sur pieds au printemps 2022 auprès de nos bénéficiaires afin d'encourager leurs créativité et aussi d'offrir au SHV des supports pour alimenter nos futures collectes. 7 dessins nous sont revenus en retour.

Les gagnants sont :
- **"Coq" de Evan,** 12 ans, région lausannoise
- **"Mains-éléphants" de Mihaela,** 4 ans, région Broye-Vully.
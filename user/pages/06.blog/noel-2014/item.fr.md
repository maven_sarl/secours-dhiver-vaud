---
date: '25-12-2014 14:13'
taxonomy:
    category:
        - événement
    tag:
        - noël
        - fête
        - bonheur
        - amour
        - famille
    author:
        - Véronique
title: 'Noël 2014'
image: 024.jpg
meta:
    description: 'La magie de Noël est arrivée dans les coeurs des familles vaudoises qui ont fêté avec le secours d''hiver la veille de Noël 2014 à Prilly.'
highlight: false
headline: 'L''équipe du secours d''hiver Vaud accueillait les familles dans un cadre chaleureux à Prilly pour fêter la veille de Noël ensemble.'
header_image: '1'
header_image_file: provamobile.jpg
---

## La fête de Noël

La magie de Noël est arrivée dans les coeurs des familles vaudoises qui ont fêté avec le secours d'hiver la veille de Noël 2014 à Prilly. Nous avions la chance de profiter d'une belle soirée ensemble avec une visite du cher Père Noël ainsi qu'une bonne fondue. 
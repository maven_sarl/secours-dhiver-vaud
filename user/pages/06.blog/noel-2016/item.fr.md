---
published: true
visible: true
title: 'Noël 2016 avec le Secours d''Hiver'
image: DSC_9608.jpg
imageAsBanner: '1'
headline: 'Une belle veillée de Noël organisée par le Secours d''hiver en présence de plusieurs familles du canton de Vaud.'
meta:
    description: 'Une belle veillée de Noël organisée par le Secours d''hiver en présence de plusieurs familles du canton de Vaud.  Une édition 2016 bien réussie!'
date: '20.12.2016 16:13:42'
---

## Photos de la soirée de Noël 2016
---
date: '25-12-2013 18:25'
taxonomy:
    category:
        - événement
    tag:
        - noël
        - fête
        - bonheur
        - amour
        - famille
    author:
        - Véronique
title: 'Noël 2013'
image: 023.jpg
meta:
    description: 'Les familles vaudoises et l''équipe du secours d''hiver ont passé un bon moment ensemble : de l''apéro, en passant par la visite du Père Noël, jusqu''au souper.'
highlight: false
headline: 'Dans la salle du restaurant "La Treille" à Prilly avait lieu la fête de Noël de cette année. Comme chaque année, la visite du Père Noël ainsi qu''un souper convivial étaient à l''ordre du jour.'
header_image: '1'
header_image_file: provamobile.jpg
---

## La soirée de Noël

Les familles vaudoises et l'équipe du secours d'hiver ont passé un bon moment ensemble : de l'apéro, en passant par la visite du Père Noël, jusqu'au souper. Chaque enfant avait le droit de venir s'assoir sur les genoux du Père Noël afin de recevoir un cadeau. Les parents, eux, se réjouissaient de voir leurs enfants heureux et enchantés par la magie de Noël.
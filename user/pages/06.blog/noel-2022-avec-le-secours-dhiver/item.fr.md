---
title: 'Action de Noël 2022'
headline: 'Cette année le Secours d’hiver Vaud a pu organiser son traditionnel Noël, le 10 décembre 2022, pour les familles.'
media_order: 'DSCN9552 (Copier).JPG,IMG-20221211-WA0070 (Copier).jpg,IMG-20221211-WA0040 (Copier).jpg,IMG-20221211-WA0045 (Copier).jpg,IMG-20221211-WA0047 (Copier).jpg,Le comité SHV (Copier).JPG,IMG-20221211-WA0063 (Copier).jpg,IMG-20221211-WA0068 (Copier).jpg,IMG-20221211-WA0041 (Copier).jpg,IMG-20221211-WA0071 (Copier).jpg,IMG-20221211-WA0050 (Copier).jpg,IMG-20221211-WA0043 (Copier).jpg,IMG-20221211-WA0065 (Copier).jpg,IMG-20221211-WA0042 (Copier).jpg,IMG-20221211-WA0055 (Copier).jpg,DSCN9629 (Copier).JPG,DSCN9625 (Copier).JPG,DSCN9630 (Copier).JPG,IMG-20221211-WA0005 (Copier).jpg,IMG-20221211-WA0033 (Copier).jpg,DSCN9680 (Copier).JPG,DSCN9646 (Copier).JPG,DSCN9657 (Copier).JPG,DSCN9670 (Copier).JPG,DSCN9633 (Copier).JPG,DSCN9638 (Copier).JPG,IMG-20221211-WA0011 (Copier).jpg,IMG-20221211-WA0012 (Copier).jpg,DSCN9669 (Copier).JPG,DSCN9615 (Copier).JPG,IMG-20221211-WA0024 (Copier).jpg,DSCN9665 (Copier).JPG,IMG-20221211-WA0007 (Copier).jpg,DSCN9626 (Copier).JPG,DSCN9628 (Copier).JPG,DSCN9619 (Copier).JPG,IMG-20221211-WA0008 (Copier).jpg,IMG-20221210-WA0009 (Copier).jpg,IMG-20221211-WA0027 (Copier).jpg,DSCN9675 (Copier).JPG,IMG-20221211-WA0038 (Copier).jpg,IMG-20221211-WA0009 (Copier).jpg,DSCN9601 (Copier).JPG,DSCN9585 (Copier).JPG,DSCN9612 (Copier).JPG,DSCN9564 (Copier).JPG,DSCN9577 (Copier).JPG,DSCN9582 (Copier).JPG,DSCN9589 (Copier).JPG,DSCN9575 (Copier).JPG,DSCN9588 (Copier).JPG,DSCN9576 (Copier).JPG,DSCN9561 (Copier).JPG,DSCN9580 (Copier).JPG,DSCN9568 (Copier).JPG,DSCN9579 (Copier).JPG,DSCN9592 (Copier).JPG,DSCN9558 (Copier).JPG,DSCN9570 (Copier).JPG,DSCN9614 (Copier).JPG,DSCN9562 (Copier).JPG,DSCN9587 (Copier).JPG,DSCN9604 (Copier).JPG,DSCN9555 (Copier).JPG,DSCN9557 (Copier).JPG,DSCN9538 (Copier).JPG,DSCN9550 (Copier).JPG,DSCN9553 (Copier).JPG,DSCN9533 (Copier).JPG,DSCN9551 (Copier).JPG,DSCN9540 (Copier).JPG,DSCN9535 (Copier).JPG,DSCN9548 (Copier).JPG,DSCN9549 (Copier).JPG,DSCN9556 (Copier).JPG,DSCN9541 (Copier).JPG'
image: 'IMG-20221211-WA0033 (Copier).jpg'
published: true
date: '26-02-2023 13:39'
publish_date: '26-02-2023 13:39'
taxonomy:
    category:
        - événement
---

Cette année le Secours d’hiver Vaud a pu organiser son traditionnel Noël, le 10 décembre 2022, pour les familles.

En effet, ce sont une septantaine de personnes, adultes et enfants, qui ont pu se réunir pour une journée magique.
Des animations ont égayé la journée dont un bon repas, la venue du Père-Noël avec sa distribution de cadeaux, un mentaliste qui est passé de table en table, un concert de violon donné par un enfant soutenu par notre programme de soutien d’activités créatrices, sportives ou musicales ainsi qu’un karaoké a émerveillé petits et grands.

Merci à nos donateurs et donatrices pour votre soutien. Vous avez offert un moment de joie et d’insouciance en y apportant des étoiles dans les yeux. 

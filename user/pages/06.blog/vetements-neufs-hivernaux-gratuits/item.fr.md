---
title: 'Vêtements neufs hivernaux gratuits'
image: cio-a-donner.jpg
imageAsBanner: '1'
headline: 'Le Secours d’hiver Vaud en partenariat avec le CIO propose gratuitement à la population vaudoise des vêtements neufs d’hiver pour femmes, hommes et adolescents.'
meta:
    description: 'Le Secours d’hiver Vaud en partenariat avec le CIO propose gratuitement à la population vaudoise des vêtements neufs d’hiver pour femmes, hommes et adolescents.'
published: true
taxonomy:
    category:
        - actualités
    author:
        - Véronique
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: '1'
date: '20.10.2020 07:33:57'
---

Le Secours d’hiver Vaud en partenariat avec le CIO propose gratuitement à la population vaudoise des vêtements neufs d’hiver pour femmes, hommes et adolescents (Lillehammer et Rio).

Manteaux doudounes, pantalons de ski, boots Nike, sous-vêtements chaud, shorts, pantalons de golf en toile, T-shirts, pulls, jaquettes, bonnets etc
Ces vêtements sont gratuits. Une finance d’entrée au local sera demandée à titre de soutien à l’association. CHF 10,00 par adulte et CHF 5,00 par adolescent dès 12 ans. Plusieurs pièces possibles par personne.

Ils sont à retirer au magasin à la Rte de Cossonay 37 à Prilly, en face du Château.

## Dès aujourd'hui :

Mercredi 11.10.2017 de 09h15 à 17h45 ( Boots et vêtements )

Mercredi 01.11.2017 de 09h15 à 17h45 ( vestes et manteaux doudounes - pantalons de ski )

Entrée au local = soutien à l'association CHF 10,00

Prendre vos sacs pour le transport.


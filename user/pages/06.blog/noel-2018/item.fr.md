---
title: 'Noël 2018 avec le Secours d''Hiver'
media_order: 'IMG_7210.JPG,IMG_7206.JPG,IMG_7204.JPG,IMG_7202.JPG,IMG_7201.JPG,IMG_7199.JPG,IMG_7198.JPG,Des parents.JPG,IMG_7222.JPG,IMG_7221.JPG,IMG_7220.JPG,IMG_7219.JPG,IMG_7215.JPG,IMG_7213.JPG,IMG_7212.JPG,IMG_7211.JPG,IMG_7236.JPG,IMG_7231.JPG,IMG_7230.JPG,IMG_7228.JPG,IMG_7227.JPG,IMG_7226.JPG,IMG_7223.JPG,Les  Présidents Vaud et Valais ainsi qu''une partie du comité.JPG,Le Président SHV.JPG,Le dessert.JPG,IMG_7251.JPG,IMG_7247.JPG,IMG_7240.JPG,IMG_7239.JPG,IMG_7237.JPG,Une table d''enfants.JPG,Les cadeaux.JPG'
image: IMG_7211.JPG
taxonomy:
    category:
        - événement
    tag:
        - noël
        - famille
        - fête
    author:
        - Véronique
---

Le Secours d'hiver Vaud a organisé son traditionnel Noël. Des familles ont participé à cette belle journée où les enfants comme les parents ont été heureux et très touchés des attentions qui leur ont été offertes.
Un repas, le père-noël, une conteuse ont entouré les cadeaux.
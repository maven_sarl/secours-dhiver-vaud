---
taxonomy:
    category:
        - actualités
        - sortie
        - événement
    tag:
        - campagne
        - famille
    author:
        - Véronique
title: 'Collecte de printemps 2018'
image: collecte-2018.jpg
imageAsBanner: '1'
headline: 'Pour 2018, le comité du Secours d’hiver Vaud a à coeur de vous faire part de son très beau projet, ainsi l’inauguration de son local de distribution de nourriture pour les vaudois précaires.'
meta:
    description: 'Pour 2018, le comité du Secours d’hiver Vaud a à coeur de vous faire part de son très beau projet. Pendant les vacances scolaires d’été une semaine sera organisée en montagne et plus précisement aux Marécottes.'
date: '10.02.2018 16:07:37'
---

## La montagne en 2018 pour les familles vaudoises précaires

Très chers donateurs,

Depuis 82 ans, le Secours d’hiver Vaud vient en aide aux personnes rencontrant des problèmes passagers ou récurrents, de manière rapide et sans jugement. Cela concerne de plus en plus de familles avec enfants. Grâce à vos dons réguliers, nous avons la chance de pouvoir ainsi les aider efficacement durant toute l’année et nous vous en remercions chaleureusement.

Pour 2018, le comité du Secours d’hiver Vaud a à coeur de vous faire part de son très beau projet. Pendant les vacances scolaires d’été une semaine sera organisée en montagne et plus précisement aux Marécottes.

Ce projet vise plusieurs buts. D’une part, permettre à des familles avec enfants de s’échapper du quotidien, notamment
pour celles qui n’ont pas les moyens de prendre des vacances, mais aussi d’offrir l’opportunité aux enfants d’apprendre à lire une carte d’itinéraire, à se servir d’une boussole ou encore à reconnaître les plantes sauvages comestibles.

Plusieurs animations sont prévues aux Marécottes, profitant ainsi d’un cadre agréable, d’une nature magnifique et de repas chauds.
Les sourires et les rires que les séjours précédents nous ont permis de connaître nous confortent dans l’idée qu’un
moment de joie et de détente est magique pour ces familles démunies.

Nous avons déjà trouvé une partie du financement avec des dons spécifiques et affectés à ce projet mais nous avons encore besoin d’étoffer celui-ci.

Afin de pouvoir effectuer des réservations et d’organiser au mieux ce grand projet nous devons d’ores et déjà savoir sur quel montant nous pourrons nous appuyer et c’est la raison de notre sollicitation de ce printemps.

Par cette lettre nous espérons vous sensibiliser à ce projet, qui apportera, soyez certain, un souvenir inoubliable et énormément de joie à de nombreuses familles. Pour beaucoup d’enfants, c’est un moment unique passé avec leurs parents tous réunis et loin des soucis.

Chaque don compte, du plus petit au plus conséquent et c’est tous ensemble que nous y arriverons.

Le comité vous remercie chaleureusement de votre soutien.

Pour le comité: Véronique Hurni, Secrétaire Générale SHVaud

## Inauguration <font style="color:red;">Reportée !!!</font>
<span style="color:red;">**La date du jeudi 20 sept. 2018 est reportée !**</span>
Le comité du Secours d’hiver Vaud vous invite à l’inauguration de notre local de distribution de nourriture pour les vaudois précaires.
C’est pour nous l’occasion de vous rencontrer autour du verre de l’amitié, 
à la Route de Cossonay 37, à Prilly (parking COOP à 100 mètres).

Nous nous réjouissons de votre visite.

Un mail ou un sms serait apprécié.
* vaud@secours-d-hiver.ch
* 078 667 87 87
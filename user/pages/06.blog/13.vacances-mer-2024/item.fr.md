---
title: 'Vacances mer 2024'
date: '10-09-2024 14:02'
publish_date: '10-09-2024 14:02'
taxonomy:
    category:
        - événement
        - actualités
published: true
sitemap:
    lastmod: '10-09-2024 14:02'
media_order: 'IMG-20240723-WA0003.jpg,IMG-20240721-WA0018.jpg,IMG-20240721-WA0003.jpg,IMG-20240721-WA0019.jpg,IMG-20240721-WA0002.jpg,IMG-20240726-WA0001.jpg,IMG-20240721-WA0017.jpg,IMG-20240721-WA0009.jpg,IMG-20240723-WA0008.jpg,IMG-20240726-WA0000.jpg,IMG-20240720-WA0025.jpg,IMG-20240721-WA0015.jpg,IMG-20240723-WA0004.jpg,IMG-20240721-WA0016.jpg,IMG-20240723-WA0013.jpg,IMG-20240720-WA0040.jpg,IMG-20240721-WA0004.jpg,IMG-20240720-WA0026.jpg,IMG-20240721-WA0008.jpg,IMG-20240723-WA0007.jpg,IMG-20240721-WA0005.jpg,IMG-20240721-WA0013.jpg'
image: IMG-20240721-WA0013.jpg
---

Le grand jour est arrivé. 

Avec une vingtaine de familles le Secours d’hiver Vaud a offert et est parti mi-juillet, pour une semaine, en Ligurie / Italie.
Au programme : farniente sur la plage, un hébergement en village de vacances avec tout ce qu’il faut comme distractions pour les petits et les grands et un programme d’enfer !

Visite d’une huilerie, le grand marché de Finale Ligure, des repas au bord de mer et dégustation des mets typiques de la Ligurie et une journée en haute mer dans le formidable archipel Pelagos où nous avons eu la chance de voir des poissons Lune, une tortue, une baleine et deux espèces de dauphins. Ceux-ci nous ont accompagnés pratiquement jusqu’au port.

Les enfants bien équipés avec des masques-tubas et palme ont pu apprendre à nager sous l’eau. Une petite a même appris à nager, tout court. La mer a été super avec nous avec une température très agréable, sans algues et sans méduses !

Les familles ont été heureuses de l’immense opportunité qui leur a été offerte grâce à nos donateurs et ceci suite à deux campagnes spécifiques menées dans ce but.

Merci à vous chères donatrices et donateurs
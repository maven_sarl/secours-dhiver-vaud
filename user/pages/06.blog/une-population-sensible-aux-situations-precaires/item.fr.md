---
date: '16-10-2014 12:51'
taxonomy:
    category:
        - actualités
    tag:
        - video
        - 'la télé'
        - reportage
    author:
        - Véronique
title: 'Une population sensible aux situations précaires'
---

Une personne sur 8 souffre de pauvreté en Suisse. Le plus souvent ces personnes sont des "working pour", c'est a dire des gens qui travaillent. Pour lutter contre cette précarité, une chaine de solidarité a été crée pour la première fois, ce jeudi à Prilly. Organisée par le secours d'hiver Vaud, cette action a pour but de récolter des fonds pour les familles dans le besoin. Nous avons été à la rencontre des passants.

<iframe width="560" height="315" src="https://www.youtube.com/embed/h6BjlB_esQM" frameborder="0" allowfullscreen></iframe>

Lien original:
http://www.latele.ch/play?i=49312


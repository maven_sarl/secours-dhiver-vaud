---
date: '23-10-2016 12:38'
taxonomy:
    category:
        - actualités
    tag:
        - video
        - ensemble
        - tsr
    author:
        - Véronique
title: 'Emission "Ensemble" RTS'
---

Yannick vit seule avec ses 3 enfants. Après un divorce douloureux elle s’est retrouvée tout d’un coup face à une descente sociale. Elle s’est adressée à l’association "Secours d’hiver" pour obtenir un soutien complémentaire, ce qui l’a aidé à s’en sortir. Réalisation: Jochen Bechler. www.secours-d-hiver.ch

<iframe width="560" height="315" src="https://www.youtube.com/embed/JqJYyuZFLx8" frameborder="0" allowfullscreen></iframe>

Lien original:
http://www.rts.ch/play/tv/ensemble/video/secours-dhiver-pour-sortir-du-tunnel?id=8111210
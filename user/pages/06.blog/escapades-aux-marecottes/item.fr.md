---
date: '20-07-2011 16:03'
taxonomy:
    category:
        - sortie
    tag:
        - zoo
        - animaux
        - valais
        - piscine
    author:
        - Véronique
title: 'Escapades aux Marécottes 2011 et 2012'
image: 064.jpg
imageAsBanner: '1'
headline: 'Entre 2011 et 2012, nous avons fait deux escapades au zoo des Marécottes en Valais.'
meta:
    description: 'Le zoo le plus haut d''Europe était la destination de nos escapades en 2011 et 2012. Les familles qui ont participé ont pu profiter des animaux au parc alpin.'
highlight: false
header_image: '1'
header_image_file: provamobile.jpg
---

## Zoo et piscine

Le zoo le plus haut d'Europe était la destination de nos escapades en 2011 et 2012. Les familles qui ont participé ont pu profiter des animaux au parc zoologique alpin, du restaurant et de la piscine lovée dans les rochers. Un endroit sublime qui nous permettait de vivre une expérience inoubliable !
---
title: 'Noël 2023'
date: '19-02-2024 19:10'
publish_date: '19-02-2024 19:10'
taxonomy:
    category:
        - événement
        - actualités
media_order: '1.jpg,2.jpg,3.jpg,4.jpg,5.jpg,6.jpg,7-1.jpg,8-1.jpg,9-1.jpg,10-1.jpg,11-1.jpg,12-1.jpg,13.jpg,14-1.jpg,15.jpg,16.jpg,17-1.jpg,18.jpg,19.jpg,20-1.jpg,21-1.jpg,22-1.jpg,23.jpg,24.jpg,25.jpg,26-1.jpg,27-1.jpg,28.jpg,Comite.jpg'
published: true
image: 23.jpg
---

Le 09 décembre 2023 la grande famille du Secours d'hiver Vaud s'est retrouvée avec une quarantaine d'enfants et leurs parents pour fêter Noël autour d'un bon repas.

Quelle ne fût pas la surprise pour les enfants de voir arrivé le Père-Noël avec son petit âne avant de distribuer une multitude de cadeaux aussi bien pour eux que pour leur parents. Des étoiles dans les yeux et des immenses sourires. Après le repas la fête a continué avec un atelier coloriage et un karaoké.

Encore une belle édition grâce à nos donateurs et nos donatrices. 

Merci à eux.
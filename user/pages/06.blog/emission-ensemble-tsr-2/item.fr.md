---
date: '27-10-2013 12:38'
taxonomy:
    category:
        - actualités
    tag:
        - ensemble
        - tsr
        - video
    author:
        - Véronique
title: 'Emission "Ensemble" TSR'
---

Depuis sa création en pleine crise économique en 1936, le Secours d'hiver aide les personnes en difficulté et les familles démunies en Suisse. Par leurs actions pratiques et les conseils qu'elles dispensent, les 27 organisations cantonales de l'association apportent leur soutien tout au long de l'année à des milliers de personnes confrontées à des situations de détresse.

<iframe width="560" height="315" src="https://www.youtube.com/embed/GEkxBpbilWk" frameborder="0" allowfullscreen></iframe>

Lien original:
http://www.rts.ch/play/tv/ensemble/video/secours-suisse-dhiver?id=5326260
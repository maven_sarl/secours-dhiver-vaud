---
content:
    items: '@self.children'
    limit: 3
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
title: Actualités
meta:
    description: 'Depuis 80 ans, notre organisation est présente pour les personnes dans le besoin domiciliées dans le canton de Vaud. Découvrez nos actualités et événements.'
pagetitle: ''
image: banner.jpg
---


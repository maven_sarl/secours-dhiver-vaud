---
published: true
slug: secours-dhiver-sortie-en-mer-sur-la-vaudoise
visible: true
title: 'Sortie en mer sur la Vaudoise'
image: DSC_0068.JPG
imageAsBanner: '1'
headline: 'Voici les photos de la sortie du Secours d''Hiver Vaud sur le lac Léman, à bord du magnifique bateau La Vaudoise.'
meta:
    description: 'Voici les photos de la sortie du Secours d''Hiver Vaud sur le lac Léman, à bord du magnifique bateau La Vaudoise.'
date: '30.12.2019 16:16:15'
---

# À l'abordage

Voici les photos de la sortie du Secours d'Hiver Vaud sur le lac Léman, à bord du magnifique bateau La Vaudoise.
---
date: '16-10-2014 12:56'
taxonomy:
    category:
        - actualités
    tag:
        - video
        - 'la télé'
        - reportage
    author:
        - Véronique
title: 'Ils luttent contre la "pauvreté des travailleurs"'
---

Le secours d'hiver Vaud organisent une récolte de fonds à Prilly ce jeudi 16 octobre 2014.

<iframe width="560" height="315" src="https://www.youtube.com/embed/n084He08DAg" frameborder="0" allowfullscreen></iframe>
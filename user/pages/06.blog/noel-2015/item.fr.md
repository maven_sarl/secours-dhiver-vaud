---
date: '25-12-2015 15:56'
taxonomy:
    category:
        - événement
    tag:
        - noël
        - fête
        - famille
    author:
        - Véronique
title: 'Noël 2015'
image: 0.JPG
date: '20.12.2015 16:13:42'
headline: 'La magie de Noël est arrivée dans les coeurs des familles vaudoises qui ont fêté avec le secours d''hiver la veille de Noël 2015 à Prilly.'
meta:
    description: 'La magie de Noël est arrivée dans les coeurs des familles vaudoises qui ont fêté avec le secours d''hiver la veille de Noël 2015 à Prilly.'
---

## La fête de Noël

La magie de Noël est arrivée dans les coeurs des familles vaudoises qui ont fêté avec le secours d'hiver la veille de Noël 2015 à Prilly. Nous avions la chance de profiter d'une belle soirée ensemble avec une visite du cher Père Noël ainsi qu'une bonne fondue. 
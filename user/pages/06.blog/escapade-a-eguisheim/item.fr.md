---
date: '04-06-2013 16:08'
taxonomy:
    category:
        - sortie
    tag:
        - france
        - zoo
        - parc
        - attraction
        - animaux
        - aigle
        - cigogne
        - singe
        - fromage
        - alsace
    author:
        - Véronique
title: 'Escapade à Eguisheim'
image: 399.jpg
meta:
    description: 'Après un agréable voyage en bus, nous arrivions dans le petit village alsacien d''Eguisheim. Nous nous sommes installés à "La Ferme du Pape".'
highlight: false
headline: 'En 2013, le secours d''hiver Vaud a organisé pour ses familles une sortie en France, plus précisément en Alsace.'
header_image: '1'
header_image_file: provamobile.jpg
---

## La découverte de l'Alsace

Après un agréable voyage en bus, nous arrivions dans le petit village alsacien d'Eguisheim. Nous nous sommes installés à "La Ferme du Pape" pour loger et se nourrir. Au programme de notre séjour : pleines d'activités pour les familles vaudoises ! Une proménade et une dégustation de vin entre autres...

### La Maison du Fromage dans la Vallée de Munster

Notre visite comprenait un circuit complèt avec un film sur les 4 saisons dans la Vallée de Munster, une visite des étables avec les Vosgiennes, une démonstration de fabrication de fromage frais où nos enfants pouvaient découvrir le goût du lait cru et une dégustation de munster frais.

### La Montagne des Singes

La visite de la montagne était très sympa pour les enfants qui ont eu la possibilité de nourrir les singes avec du pop-corn offert à l’entrée. Munis de sacs à dos, tout le monde partait excité à la ballade pour aller observer les animaux et d'entrer en contact direct avec eux.

### Cigoland à Kintzheim

Le parc de loisirs nous accueillait avec ses attractions pour les enfants accompagnés de leur parents et l'équipe du secours d'hiver Vaud. Les manèges comme le train de la mine ou les bateaux tamponneurs ont mis le sourire sur les visages de notre groupe. Nous avons également pu observer des cigognes en liberté ainsi que d'autres animaux dans la nature.

### La Volerie des Aigles

Lors de notre visite de la volerie, nous avons eu l'occasion d'observer les plus grands rapaces du monde. Nous avons vécu un moment exceptionnel au Château de Kintzheim : après avoir assisté au spectacle des dresseurs, les plus courageux d'entre nous ont fait voler eux-mêmes les aigles !
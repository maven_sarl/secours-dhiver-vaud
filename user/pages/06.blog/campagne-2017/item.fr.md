---
taxonomy:
    category:
        - actualités
    author:
        - Véronique
title: 'Campagne 2017'
image: Stern2017.JPG
imageAsBanner: '1'
date: '01.09.2017 06:04:20'
headline: "Chaque année nous vous sollicitons pour notre campagne de dons en faveur des familles vaudoises précaires.\r\nEn 2016, le Secours d’hiver Vaud a pu faire un travail remarquable, grâce à votre soutien."
---

Chères donatrices, Chers donateurs,

Chaque année nous vous sollicitons pour notre campagne de dons en faveur des familles vaudoises précaires.
En 2016, le Secours d’hiver Vaud a pu faire un travail remarquable, grâce à votre soutien.

En effet ce sont 200 situations de précarité secourues dont 102 familles monoparentales, 48 familles avec enfants, 48 personnes seules et 3 couples sans enfants ont pu être aidées. 167 femmes, 95 hommes et 336 mineurs ont ainsi pu retrouver le sourire au travers d’une étape de vie difficile.

Vingt familles ont pu profiter de notre fête de Noël et 13'267 Fr ont pu apporter un grand soulagement pour les fêtes de fin d'année grâce à un bon repas chaud, des cadeaux pour tous et des animations inoubliables.

Les yeux brillants et les sourires des enfants et de leurs parents ont remercié le comité pour le travail qu’il a accompli toute l’année.

Depuis le début de notre projet parrainage, 25 familles ont pu ou peuvent bénéficier d'un soutien financier mensuel, d'un parrain ou d'une marraine. Pour ces familles c'est un gigantesque soutien extrêmement apprécié. De savoir qu’un parrain ou une marraine leur fourni régulièrement des cartes d’achats d’aliments est une véritable bulle d’air dans le cap difficile qu’ils traversent.

Grâce à des dons recherchés et affectés spécifiquement, le Secours d’hiver Vaud a pu organiser [un projet de loisirs d’une semaine au bord de mer pour les familles](/blog/vacances-italie-2016) grâce au montant de 87'251 Fr, qui a laissé d’impérissables souvenirs aux participants.

Des petits ont appris à nager y compris sous l’eau. Des familles qui n’étaient plus parties en vacances depuis 10 ans ont pleuré de joie devant ce magnifique séjour. Grâce à votre générosité. Soyez-en remerciés.

Nous avons plein de projets en tête pour cette année 2017-2018 et nous espérons vous avoir sensibilisé à soutenir notre action.
Nous sommes reconnaissants de votre fidèle soutien, qui nous permet de mener à bien toutes ces actions grâce à vos dons depuis 81 ans.

Un grand merci pour [votre don](/donations) à venir et votre soutien.

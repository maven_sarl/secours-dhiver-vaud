---
date: '27-08-2012 08:13'
taxonomy:
    category:
        - sortie
    tag:
        - léman
        - vaudoise
    author:
        - Véronique
title: 'Soirée sur "la Vaudoise"'
image: 005.jpg
meta:
    description: 'Le lundi soir, 27 août 2012, le secours d''hiver a loué le bateau "la Vaudoise" chez la Confrérie des Pirates d''Ouchy pour faire une sortie sur le Lac Léman.'
highlight: false
headline: 'En cette fin d''été, nous avons organisé une sortie très spéciale au bord de "la Vaudoise" sur le magnifique Lac Léman.'
header_image: '1'
header_image_file: provamobile.jpg
---

## Sortie maritime

Le lundi soir, 27 août 2012, le secours d'hiver a loué le bateau "la Vaudoise" chez la Confrérie des Pirates d'Ouchy pour faire une sortie sur le Lac Léman. En navigant, Véronique Hurni, secrétaire générale du SHV, faisait un discours suivi par un délicieux apéro.
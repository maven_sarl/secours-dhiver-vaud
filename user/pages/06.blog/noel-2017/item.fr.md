---
visible: true
title: 'Noël 2017 avec le Secours d''Hiver'
image: DSC_7082.jpg
imageAsBanner: '1'
headline: 'Comme chaque année, le Père Noël est venu rendre visite aux familles vaudoises qui se sont reunies avec le secours d''hiver pour célébrer les fêtes de fin d’année.'
meta:
    description: 'Comme chaque année, le Père Noël est venu rendre visite aux familles vaudoises qui se sont reunies avec le secours d''hiver pour célébrer les fêtes de fin d''année.'
date: '20.12.2017 16:13:42'
---

## Noël 2017 avec le Secours D'Hiver

Comme chaque année, le Père Noël est venu rendre visite aux familles vaudoises qui se sont reunies avec le Secours d'Hiver pour célébrer les fêtes de fin d'année. Au programme: bon repas, démonstration de capoeira et remise des cadeaux aux enfants présents! Encore une chaleureuse rencontre pleine de rires et de bonne humeur. Merci à tous pour votre présence et à l'année prochaine!

Un grand merci également aux généreux donateurs, sans qui, rien de tout cela n'aurait pu avoir lieu.

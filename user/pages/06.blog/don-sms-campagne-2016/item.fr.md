---
taxonomy:
    category:
        - actualités
    tag:
        - video
        - campagne
    author:
        - Véronique
title: 'Don par SMS - Campagne 2016'
date: '10.08.2016 00:00:00'
---

Le secours d'hiver Suisse lance une campagne de don par SMS, pour chaque canton.
Envoyez "BESOIN-VD" au 339 !

<iframe width="560" height="315" src="https://www.youtube.com/embed/XS7rC5oJxTY" frameborder="0" allowfullscreen></iframe>
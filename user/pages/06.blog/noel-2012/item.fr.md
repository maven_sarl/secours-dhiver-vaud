---
date: '25-12-2012 17:08'
taxonomy:
    category:
        - événement
    tag:
        - noël
        - fête
        - bonheur
        - amour
        - famille
    author:
        - Véronique
title: 'Noël 2012'
image: 047.jpg
meta:
    description: 'L''arrivée du Père Noël était le moment que tous les enfants attendaient impatiemment. Muni d''un grand sac à dos rempli de cadeaux, il s''installait la.'
highlight: false
headline: 'La soirée de Noël a été remplie de beaux moments : la visite du Père Noël avec un gros sac de cadeaux faisait briller les yeux des enfants et le souper de Noël offrait plein de bonheur et de convivialité pour les familles.'
---

## La visite du Père Noël

L'arrivée du Père Noël était le moment que tous les enfants attendaient impatiemment. Muni d'un grand sac à dos rempli de cadeaux, il s'installait au milieu de la foule pour distribuer ses petits quelques choses.


## Le souper de Noël

Plusieurs familles étaient réunies pour célébrer ensemble cette fête de fin d'année. Autour d'un bon souper de Noël, nous avons profité d'un moment convivial et chaleureux.
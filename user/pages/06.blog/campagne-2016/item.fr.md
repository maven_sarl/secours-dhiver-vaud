---
date: '01-09-2016 18:33'
taxonomy:
    category:
        - actualités
    author:
        - Véronique
title: 'Campagne 2016'
image: campagne-2016.jpg
imageAsBanner: '0'
headline: 'Voici la nouvelle campagne du Secours d''hiver Suisse pour 2016.'
---

![](campagne-2016.jpg)
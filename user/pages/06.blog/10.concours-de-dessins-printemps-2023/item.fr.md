---
title: 'Concours de dessins printemps 2023'
media_order: '11 à 18 ans-1.jpg,11 à 18 ans-2.jpg,Gagnant petits- Noahm.jpg,3 à 10 ans-9.jpg,3 à 10 ans-8.jpg,3 à 10 ans-7.jpg,3 à 10 ans-6.jpg,3 à 10 ans-5.jpg,3 à 10 ans-4.jpg,3 à 10 ans-3.jpg,3 à 10 ans-2.jpg,3 à 10 ans-1.jpg'
published: true
date: '07-03-2023 19:10'
publish_date: '07-03-2023 19:10'
taxonomy:
    category:
        - événement
        - actualités
image: 'Gagnant petits- Noahm.jpg'
---

Ce concours de dessins a été mis sur pieds au printemps 2023 auprès de nos bénéficiaires afin d'encourager leurs créativité et aussi d'offrir au SHV des supports pour alimenter nos futures collectes.

Les gagnants recevront un bon FNAC !
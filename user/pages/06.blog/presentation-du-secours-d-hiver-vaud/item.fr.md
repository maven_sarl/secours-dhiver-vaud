---
date: '17-11-2016 09:43'
taxonomy:
    category:
        - actualités
    tag:
        - campagne
        - famille
    author:
        - Véronique
title: 'Présentation du Secours d''hiver Vaud'
image: preview-video-intro.jpg
headline: 'Vidéo de présentation du Secours d''hiver Vaud à Prilly'
---

Vidéo de présentation du Secours d'hiver Vaud à Prilly.

<iframe width="560" height="315" src="https://www.youtube.com/embed/izlF85Na5dw" frameborder="0" allowfullscreen></iframe>
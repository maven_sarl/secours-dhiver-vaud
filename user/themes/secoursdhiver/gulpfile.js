require('events').EventEmitter.prototype._maxListeners = 1000;

var elixir = require('laravel-elixir');
var gulp = require('gulp');

elixir.config.assetsPath = 'resources';
elixir.config.publicPath = '';

var tingpng = require('gulp-tinypng');

var images = [
    //'images/**/*.{png,jpg}',
    //'../../pages/**/*.{png,jpg}'
    '../../pages/06.blog/noel-2014/**/*.{png,jpg}',
    '../../pages/06.blog/noel-2015/**/*.{png,jpg}',
    '../../pages/06.blog/vacances-italie-2016/**/*.{png,jpg}',
    '../../pages/06.blog/soiree-sur-la-vaudoise/**/*.{png,jpg}'
];

gulp.task('image', function () {
    gulp.src(images, {base: '.'})
        //.pipe(tingpng('REqM4CkY77zubkNaG-wIwz6DXMAHgibY')) // MAVEN
        .pipe(tingpng('NCuCCFIofwmtjZA-NpuOH1-SqY_FQSxV')) // E-nepsis
        .pipe(gulp.dest('.'));
});


elixir(function(mix) {
    mix
        //.task('icon')
        .less(['frontend.less'], 'css/core.css');
});





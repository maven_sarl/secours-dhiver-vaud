// Detect request animation frame
var lastScrollPosition = -1;
var scrollDirection = "down";
var windowHeight = 0;
var useScroll = true;

var scroll = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    window.oRequestAnimationFrame || null;
// IE Fallback, you can even fallback to onscroll
//function(callback){ window.setTimeout(callback, 1000/60) };

function loop(bool) {

    var currentScrollPosition = window.pageYOffset;

    if( lastScrollPosition <= currentScrollPosition){
        scrollDirection = "down";
    }else{
        scrollDirection = "up";
    }

    // Avoid calculations if not needed
    if (lastScrollPosition == currentScrollPosition) {
        //scroll(loop);
        //return false;
    } else {
        lastScrollPosition = currentScrollPosition;

        $visible.scroll();
        $parallax.scroll();
        //$navigation.scroll();
    }

    if( bool ){
        scroll( loop );
    }
    //
}

var $visible = {
    elements: [],
    quantity: 0,
    init: function(){
        this.elements = $('[data-visible]');
        this.quantity = this.elements.length;
        this.scroll();
    },
    scroll: function(){

        var visibleScreen = lastScrollPosition+windowHeight;

        for (var i = 0; i < this.quantity; i++) {
            var currentElement =  this.elements.eq(i);
            var offset = parseInt(currentElement.attr("data-visible-offset")?currentElement.attr("data-visible-offset"):0);
            var currentTop = currentElement.offset().top + 100 + (offset);
            var cssClass = currentElement.attr("data-visible");

            console.log("currentTop", currentTop, offset);

            if( currentTop >= lastScrollPosition && currentTop <= visibleScreen){
                currentElement.addClass(cssClass);
            }else{
                currentElement.removeClass(cssClass);
                /*
                 if( currentTop > visibleScreen){
                 currentElement.removeClass(cssClass);
                 }
                 if( currentTop > (currentTop+visibleScreen)){
                 currentElement.removeClass(cssClass);
                 }
                 */
            }
        }
    }
};

var $parallax = {
    parallaxElements: [],
    parallaxQuantity: 0,
    movePercentage:0.4,
    init: function(){
        var self = this;
        this.parallaxElements = [];
        $('[data-parallax="true"]').each(function(){

            $(this).height($(this).parent().height()+(this.movePercentage*$(this).parent().height()));

            self.parallaxElements.push({
                obj: $(this),
                properties: $.parseJSON($(this).attr("data-properties")),
                height: $(this).height()/2,
                parentHeight: $(this).parent().height()/2,
                parentTop: $(this).parent().offset().top
            });
        });
        this.parallaxQuantity = this.parallaxElements.length;
        this.scroll();
    },
    scroll: function(){

        for (var i = 0; i < this.parallaxQuantity; i++) {
            var object = this.parallaxElements[i];
            var currentElement =  object.obj;
            var currentElementHeight = object.height;
            var currentTop =  object.parentTop;



            var top = (object.parentHeight - currentElementHeight)/2;
            var prop = object.properties;
            var moveProp = {};
            //var move = top + ((lastScrollPosition+windowHeight/2 - currentTop)*this.movePercentage);
            var move = top + ((lastScrollPosition-currentTop)*this.movePercentage);

            TweenMax.to(currentElement, 0.05, {y:move});
            /*
            console.log(top, move);

            if( prop.y ){ moveProp.y = Math.round(move); }
            if( prop.x ){ moveProp.x = Math.round(move); }
            if( prop.scale ){ moveProp.scale = 1+Math.round(move)/1000; }

            TweenMax.to(currentElement, 0.05, moveProp);
            //TweenMax.to(currentElement, 0, {backgroundPosition:'50% '+Math.round(move)+'px'});
            /*
             currentElement.css({
             'transform': 'translateY(' + move + 'px)'
             });
             */

        }
    }
};


var $navigation = {
    obj: null,
    scrollUp: null,
    offsetTop: -1,
    init:function(){
        this.obj = $("#page-navigation");
        if( this.obj ){
            this.scrollUp = this.obj.attr("data-scroll") == "stick-on-scroll-up"?true:false;
            this.offsetTop = this.obj.offset().top;
        }
    },
    scroll: function(){
        if( this.obj ) {
            var limit = this.offsetTop;
            if (this.scrollUp && scrollDirection == "down") {
                limit += this.obj.height();
            }

            if (this.scrollUp) {
                limit *= 3;
            }

            if (lastScrollPosition >= limit && limit >= 0) {
                $("body").addClass("navigation-fixed");
                if (scrollDirection == "down") {
                    $("body").addClass("scroll-down");
                    $("body").removeClass("scroll-up");
                } else {
                    $("body").addClass("scroll-up");
                    $("body").removeClass("scroll-down");
                }
            } else {
                if (this.scrollUp) {
                    if (lastScrollPosition >= (limit / 2)) {
                        $("body").removeClass("scroll-up");
                        $("body").addClass("scroll-down");
                    } else {
                        $("body").removeClass("navigation-fixed");
                    }
                } else {
                    $("body").removeClass("navigation-fixed");
                }
            }
        }
    }
};

var $mobile = {
    init: function(){
        $(".navigation-mobile").on("click", function(){
            $("body").toggleClass("mobile-navigation-opened");
            return false;
        })
    }
};



// Call the loop for the first time

$(window).ready(function(){

    $mobile.init();

    setTimeout(function(){
        $parallax.init();
        //$navigation.init();
        $visible.init();
        windowHeight = $(window).height();

        if( !useScroll ){
            loop(true);
        }else{
            $(window).scroll(function() {
                loop(false);
            });
        }

    }, 300);


    $('[data-link="true"]').click(function () {

        var link = $(this).find('.title a').length ? $(this).find('.title a') : $(this).find('a');

        if( link.attr('href') === undefined ){
            link = $(this);
        }
        if ( link.attr('href') ) {
            if (link.attr('target') == "_blank") {
                window.open(link.attr('href'));
            } else {
                location.href = link.attr('href');
            }

        }
        return false;
    });

    /*
     ACTIONS TABS
     */
    $('[rel*="action:"]').hover(function () {
        var action = $(this).attr("rel").split(":")[1];
        $('body').addClass("action_" + action);
    }, function () {
        var action = $(this).attr("rel").split(":")[1];
        $('body').removeClass("action_" + action);
    });

});

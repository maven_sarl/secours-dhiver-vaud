(function() {

    'use strict';

    angular
        .module('web')
        .controller('contactController', contactController);


    function contactController($scope, $http, $location, config){

        $scope.sended = false;
        $scope.error = false;

        $scope.contact = {};

        $scope.submit = function(e){

            $scope.error = false;
            $scope.sended = false;

            var formElement = $("#contact-online");
            $scope.contact.uri = formElement.attr("action");
            $scope.contact['g-recaptcha-response'] = formElement.find('#g-recaptcha-response').val();

            $http.post($scope.contact.uri, $scope.contact).success(function(response){
                $scope.sended = true;
                window.weeboxAnalytics.pageView("WbxContactFormSuccess", "ContactForm Success");
            }).error(function(response){
                $scope.error = true;
                grecaptcha.reset();
                alert(response.error)
            });


            return false;
        };
    };

})();
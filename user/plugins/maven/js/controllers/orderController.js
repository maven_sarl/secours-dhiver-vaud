(function() {

    'use strict';

    angular
        .module('web')
        .controller('orderController', orderController);


    function orderController($scope, $http, $location, config){

        $scope.sended = false;
        $scope.error = false;

        $scope.countries = [
            {
                name: "Suisse",
                id: "CH"
            },
            {
                name: "France",
                id: "FR"
            },
            {
                name: "Italie",
                id: "IT"
            },
            {
                name: "Allemagne",
                id: "DE"
            }
        ];

        $scope.order = {
            articles: []
        };

        $scope.getTotalPrice = function () {
            var price = 0;
            angular.forEach($scope.order.articles, function (item) {
                price += item.price * item.qty;
            });
            return price.toFixed(2);
        };

        $scope.submit = function(e){

            $scope.error = false;
            $scope.sended = false;

            var formElement = $("#order-online");
            $scope.order.uri = formElement.attr("action");
            $scope.order['g-recaptcha-response'] = formElement.find('#g-recaptcha-response').val();

            $http.post($scope.order.uri, $scope.order).success(function(response){
                $scope.sended = true;
                window.weeboxAnalytics.pageView("WbxOrderFormSuccess", "OrderForm Success");
            }).error(function(response){
                $scope.error = true;
                grecaptcha.reset();
                alert(response.error)
            });


            return false;
        };
    };

})();
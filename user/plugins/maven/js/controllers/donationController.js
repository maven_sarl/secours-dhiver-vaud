(function() {

    'use strict';

    angular
        .module('web')
        .controller('donationController', donationController)


    function donationController($scope, $http, $location, config){

        $scope.sended = false;
        $scope.error = false;


        $scope.donation = {
            first_name: '',
            last_name: '',
            address1: '',
            zip: '',
            city: '',
            email: '',
            amountRadio: 100,
            amount: 100
        };


        $scope.submit = function(e){

            $scope.error = false;

            var returnUrl       = $scope.returnUrl;
            var returnCancelUrl = $scope.returnCancelUrl;

            if( $scope.donation.amountRadio ){
                $scope.donation.amount = angular.copy($scope.donation.amountRadio);
            }else{
                $scope.donation.amount = "";
            }

            $http.post($scope.apiUrl, $scope.donation).success(function(response){
                returnUrl = $scope.returnUrl;
                returnUrl += response.data.token;

                returnCancelUrl = $scope.returnCancelUrl;
                returnCancelUrl += response.data.token;

                $("input[name='amount']").val($scope.donation.amount);
                $("input[name='return']").val(returnUrl);
                $("input[name='cancel_return']").val(returnCancelUrl);

                //alert($scope.donation.amount)
                window.weeboxAnalytics.pageView("WbxDonationFormSuccess", "DonationForm Success");
                $("#donate-online").submit();
                
            }).error(function(response){
                $scope.error = true;
            });

            return false;
        };

    };


})();
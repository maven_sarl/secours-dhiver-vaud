(function() {

    'use strict';


    window.weeboxAnalytics = {
        dataLayer: window.dataLayer || [],
        getPathname: function(){
            var p = window.location.pathname;
            return p;
        },
        pageView: function(pageUrl, title){
            //alert(this.getPathname()+'/'+pageUrl)
            this.dataLayer.push({
                'event':'virtualPageView',
                'virtualPageURL': this.getPathname()+'/'+pageUrl,
                'virtualPageTitle' : title
            });
        },
        formSubmit: function(id){
            //alert(this.getPathname()+'/'+pageUrl)
            this.dataLayer.push({
                'event' : 'formSubmissionSuccess',
                'formId' : id
            });
        }
    };




    angular
        .module('web', [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<$');
            $interpolateProvider.endSymbol('$>');
        })
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.headers.common = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            };
        }])
        .config(function($locationProvider) {
            $locationProvider.html5Mode({
                enabled: false,
                requireBase: false
            });
        })
        .constant('config', {
            api: $('meta[name="api"]').attr('content') + "/api/"
        })
        .directive('input', [function () {
            'use strict';

            var directiveDefinitionObject = {
                restrict: 'E',
                require: '?ngModel',
                link: function postLink(scope, iElement, iAttrs, ngModelController) {
                    if (iAttrs.value && ngModelController) {
                        ngModelController.$setViewValue(iAttrs.value);
                    }
                }
            };

            return directiveDefinitionObject;
        }])
        .run(function(){

        });



    $(window).ready(function(){

        /*
         ACTIONS TABS
         */
        $('[rel*="action:"]').hover(function () {
            var action = $(this).attr("rel").split(":")[1];
            $('body').addClass("action_" + action);
        }, function () {
            var action = $(this).attr("rel").split(":")[1];
            $('body').removeClass("action_" + action);
        });


        $('[data-link="true"]').click(function(){

            var link = $(this).find('.title a').length ? $(this).find('.title a') : $(this).find('a');

            if( link.attr('href') ){
                if( link.attr('target') == "_blank" ){
                    window.open(link.attr('href'));
                }else{
                    location.href = link.attr('href');
                }

            }
            return false;
        });

        $('body').scroll(function(){
            console.log("roll", $('body').scrollTop())
            $('#navigation').css("top", $(document).scrollTop+"px");
        });



        $('.header-banner').each(function(){
            var img = $(this).find('img').attr('src');
            $('.header-banner').css('background-image', 'url("'+img+'")');
        });

        $('.mobile-navigation').find("> span").click(function(){
            $("html").toggleClass("mobile-navigation-opened");
        });
    });


})();


/*============================================================================
 Social Icon Buttons v1.0
 Author:
 Carson Shold | @cshold
 http://www.carsonshold.com
 MIT License
 ==============================================================================*/
window.CSbuttons = window.CSbuttons || {};

$(function() {
    CSbuttons.cache = {
        $shareButtons: $('.social-sharing')
    }
});

CSbuttons.init = function () {
    CSbuttons.socialSharing();
}

CSbuttons.socialSharing = function () {
    var $buttons = CSbuttons.cache.$shareButtons,
        $shareLinks = $buttons.find('a'),
        permalink = $buttons.attr('data-permalink');

    // Get share stats from respective APIs
    var $fbLink = $('.share-facebook'),
        $twitLink = $('.share-twitter'),
        $googleLink = $('.share-google'),
        $linkedinLink = $('.share-linkedin');

    if ( $fbLink.length ) {
        $.getJSON('https://graph.facebook.com/?id=' + permalink + '&callback=?')
            .done(function(data) {
                if (data.shares) {
                    $fbLink.find('.share-count').text(data.shares).addClass('is-loaded');
                } else {
                    $fbLink.find('.share-count').remove();
                }
            })
            .fail(function(data) {
                $fbLink.find('.share-count').remove();
            });
    };

    if ( $twitLink.length ) {
        $.getJSON('https://cdn.api.twitter.com/1/urls/count.json?url=' + permalink + '&callback=?')
            .done(function(data) {
                if (data.count > 0) {
                    $twitLink.find('.share-count').text(data.count).addClass('is-loaded');
                } else {
                    $twitLink.find('.share-count').remove();
                }
            })
            .fail(function(data) {
                $twitLink.find('.share-count').remove();
            });
    };

    if ( $googleLink.length ) {
        // Can't currently get Google+ count with JS, so just pretend it loaded
        $googleLink.find('.share-count').addClass('is-loaded');
    }

    if ( $linkedinLink.length ) {
        $.getJSON('http://www.linkedin.com/countserv/count/share?url=' + permalink + '&callback=?')
            .done(function(data) {
                if (data.count > 0) {
                    $linkedinLink.find('.share-count').text(data.count).addClass('is-loaded');
                } else {
                    $linkedinLink.find('.share-count').remove();
                }
            })
            .fail(function(data) {
                $linkedinLink.find('.share-count').remove();
            });
    };

    // Share popups
    $shareLinks.on('click', function(e) {
        var el = $(this),
            popup = el.attr('class').replace('-','_'),
            link = el.attr('href'),
            w = 700,
            h = 400;

        // Set popup sizes
        switch (popup) {
            case 'share_twitter':
                h = 300;
                break;
            case 'share_fancy':
                w = 480;
                h = 720;
                break;
            case 'share_google':
                w = 500;
                break;
            case 'share_reddit':
                popup = false;
                break;
            case 'share_skype':
                w = 300;
                h = 720;
                break;
        }

        if (popup) {
            e.preventDefault();
            window.open(link, popup, 'width=' + w + ', height=' + h);
        }
    });
}

$(function() {
    window.CSbuttons.init();
});
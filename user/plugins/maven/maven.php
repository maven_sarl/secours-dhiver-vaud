<?php
namespace Grav\Plugin;

use Grav\Common\Grav;
use Grav\Common\Plugin;
use Grav\Common\User\User;
use Grav\Common\Page\Page;

use Grav\Plugin\Maven\DB;
use Grav\Plugin\Maven\Contact;
use Grav\Plugin\Maven\Donation;
use Grav\Plugin\Maven\Order;

class MavenPlugin extends Plugin
{
    protected $api_route = null;
    protected $mode = "api";

    protected $adminRoute = [
        ["name" => "Donations", "route" => "maven-donation", "icon"=>"fa-heart-o"],
        ["name" => "Contacts", "route" => "maven-contact", "icon"=>"fa-envelope-o"],
        ["name" => "Orders", "route" => "maven-order", "icon"=>"fa-envelope-o"]
    ];

    private $Donation;
    private $Contact;
    private $Order;

    private $language;
    private $theme;
    private $uri;

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize configuration
     */
    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            $this->active = false;

            require_once __DIR__.'/vendor/autoload.php';

            $this->enable([
                'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
                'onPageInitialized' => ['onPageAdminInitialized', 0],
                'onAdminMenu' => ['onAdminMenu', 0]
            ]);

            return;
        }

        require_once __DIR__.'/vendor/autoload.php';

        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
            'onPageInitialized' => ['onPageInitialized', 0]
        ]);
    }

    /**
     * Add current directory to twig lookup paths.
     */
    public function onTwigTemplatePaths()
    {
        if (!$this->isAdmin()) {
            $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
        }else{
            $this->grav['twig']->twig_paths[] = __DIR__ . '/admin/templates';
        }
    }

    /**
     * Set needed variables to display pagination.
     */
    public function onTwigSiteVariables()
    {
        $this->grav['assets']
            ->add('jquery', 130)
            ->addJs('plugin://maven/js/angular.min.js')
            ->addJs('plugin://maven/js/app.js')
            ->addJs('plugin://maven/js/controllers/contactController.js')
            ->addJs('plugin://maven/js/controllers/orderController.js')
            ->addJs('plugin://maven/js/controllers/donationController.js')
            ->addCss('plugin://maven/plugins/lightgallery/css/video-js.css')
            ->addJs('plugin://maven/plugins/lightgallery/js/video.js', 120)
            ->addCss('plugin://maven/plugins/lightgallery/css/lightgallery.css')
            ->addJs('plugin://maven/plugins/lightgallery/js/lightgallery.js',110 )
            ->addJs('plugin://maven/plugins/lightgallery/js/lg-video.js', 100);
//            ->addJs('plugin://maven/plugins/lightgallery/js/lg-autoplay.js', 90);


    }


    public function onPageAdminInitialized()
    {
        /** @var Uri $uri */
        $uri = $this->grav['uri'];
        $route = $this->config->get('plugins.admin.route');
        $pageName = null;
        $file = null;
        $this->grav['twig']->template = $pageName;

        $routeChecked = null;
        foreach($this->adminRoute as $r){
            if (isset($uri->paths()[1]) && $uri->paths()[1] == $r["route"]) {
                $routeChecked = $r;
                break;
            }
        }

        if ($routeChecked == null) {
            return;
        }

        if ( $routeChecked ) {

            if ($this->grav['user']->authenticated) {
                // LOGGED

                if( $routeChecked["route"] == "maven-donation" ){
                    $this->Donation = new Donation();
                    $listing = $this->Donation->getAllResults();
                    $this->grav['twig']->items = $listing;
                }

                if( $routeChecked["route"] == "maven-contact" ){
                    $this->Contact = new Contact();
                    $listing = $this->Contact->getAllResults();
                    $this->grav['twig']->items = $listing;
                }

            }else{
                // NOT LOGGED
                $this->base = '/' . trim($route, '/');
                $this->grav->redirect($this->base);
                exit();
            }

        }

        return;
    }

    public function onPageInitialized()
    {

        $this->Donation = new Donation();
        $this->Contact  = new Contact();
        $this->Order  = new Order();


        $self = $this;


        $language           = $this->grav['language']->getActive()?$this->grav['language']->getActive():$this->grav['theme']->config()["default_lang"];
        $uri                = $this->grav['uri'];
        $config             = $this->grav['config'];
        $api_route          = "/" . $language . $this->grav['config']->get('plugins.maven.api_route') ."/";

        $uriCleanedPort = $uri->scheme().$uri->host();//str_replace(":80", "", $uri->base()."".$uri->rootUrl());
     

        $this->grav['twig']->twig_vars['site_url'] = $uriCleanedPort;
        $this->grav['twig']->twig_vars['api_url'] = $uriCleanedPort . $api_route;
        $this->grav['twig']->twig_vars['theme_url'] = $uriCleanedPort . "/user/themes/".$this->grav['themes']->current()."/";


        /*
         * USE PAGE
         */
        $this->Donation->routingPage();
        $this->Contact->routingPage();
        $this->Order->routingPage();


        /*
         * USE API
         */
        if ( $config->get('plugins.maven.api_route') && preg_match("#".$config->get('plugins.maven.api_route')."\/(.*)#i", $uri->path()) ) {

            unset($this->grav['page']);

            $app = new \Slim\App([
                'settings' => [
                    'displayErrorDetails' => true,
                    'addContentLengthHeader' => false
                ]
            ]);

            $this->Donation->routingApi($app);
            $this->Contact->routingApi($app);
            $this->Order->routingApi($app);

            $app->run();

        }


        if( isset($this->grav['page']) && !empty($this->grav['page']->name()) ){
            // display page
            //echo $this->grav['page']->name();
        }else{
            exit();
        }

        return;
    }


    /**
     * Add navigation item to the admin plugin
     */
    public function onAdminMenu()
    {
        foreach($this->adminRoute as $route){
            $this->grav['twig']->plugins_hooked_nav[$route["name"]] = ['route' => $route["route"], 'icon' => $route["icon"]];
        }
        //$this->grav['twig']->plugins_hooked_nav['PLUGIN_DATA_MANAGER.DATA_MANAGER'] = ['route' => $this->route, 'icon' => 'fa-heart-o'];
    }

}
<?php
namespace Grav\Plugin\Maven;

use Grav\Common\Grav;
use Grav\Common\GravTrait;
use RocketTheme\Toolbox\File\File;
use Grav\Common\Page\Page;

use Grav\Plugin\Maven\Controller;
use PDO;

class Donation extends Controller
{

    private $debug = false;
    private $form;


    function __construct(){
        parent::__construct();
        $this->_initDonationTable();
    }


    private function _initDonationTable(){

        $this->DB()->exec("CREATE TABLE IF NOT EXISTS donations (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    token TEXT,
                    first_name TEXT,
                    last_name TEXT,
                    email TEXT,
                    address1 TEXT,
                    zip TEXT,
                    city TEXT,
                    amount FLOAT,
                    status TEXT,
                    payment_fees FLOAT,
                    payment_amount FLOAT,
                    payer_id TEXT,
                    created_at DATETIME,
                    updated_at DATETIME)");
    }

    public function routingPage(){

        if( $this->isPage( $this->config("plugins.maven.donation.page") ) ){

            // Current page is donation
            self::getGrav()['twig']->twig_vars['donate_cancel'] = isset($_GET["cancel"])?true:false;
            self::getGrav()['twig']->twig_vars['donate_success'] = isset($_GET["success"])?true:false;
            self::getGrav()['twig']->twig_vars['donate_merchant_id'] = $this->config("plugins.maven.donation.merchant_id");
            if( isset($_GET["token"]) ){
                self::getGrav()['twig']->twig_vars['donate_form'] = $this->getUserByToken($_GET["token"]);
            }
        }
        // END

    }


    public function routingApi( $app ){


        if( $this->debug ){
            echo $this->language;
            echo "<br>";
            echo $this->api_route;
            echo "<br>";
            echo $this->uri->path();
            echo "<br>";
            echo $this->config("plugins.maven.donation.page");
            echo "<br>";
        }


        $self = $this;


        $app->get( $this->api_route . 'donate/cancel/{token}', function ($request, $response, $args) use ($self) {


            $self->form = $request->getParams();

            $data = $self->cancelDonation( $args['token'] );

            header('Location: ' . $self->getPageFolderUrl($self->config("plugins.maven.donation.page"))->url() . "?cancel=true&token=". $args['token']);
            exit();

        });


        $app->post($this->api_route . 'donate/success/{token}', function ($request, $response, $args) use ($self) {

            $self->form = $request->getParams();

            $data = $self->convertLeadToSuccessDonation( $args['token']);

            header('Location: ' . $self->getPageFolderUrl($self->config("plugins.maven.donation.page"))->url() . "?success=true&token=". $args['token']);
            exit();
        });


        $app->post($this->api_route . 'donate/create', function ($request, $response, $args) use ($self) {

            $self->form = $request->getParams();

            if( $self->_validateDonationFields() ){
                // FIELDS OK
                $data = $self->addLeadToDatabase();
                return $response->withJson(["success"=>"true", "data"=>$data], 200);
            }else{
                // FIELDS ERROR
                return $response->withJson(["success"=>"false", "error"=>"fields error"], 500);
            }

        });

    }



    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    private function _validateDonationFields(){

        if (
            isset($this->form['email']) &&
            isset($this->form['first_name']) &&
            isset($this->form['last_name']) &&
            isset($this->form['address1']) &&
            isset($this->form['zip']) &&
            isset($this->form['city']) &&
            filter_var($this->form['email'], FILTER_VALIDATE_EMAIL)
        ){
            $this->form['first_name']  = $this->test_input($this->form['first_name']);
            $this->form['last_name']   = $this->test_input($this->form['last_name']);
            $this->form['email']      = $this->test_input($this->form['email']);
            $this->form['address1']      = $this->test_input($this->form['address1']);
            $this->form['zip']      = $this->test_input($this->form['zip']);
            $this->form['city']      = $this->test_input($this->form['city']);

            return true;
        }
        return false;
    }


    public function getUserByToken( $token ){
        $results = $this->DB()->prepare('SELECT * from donations WHERE token = :token');
        $results->execute( array("token" => $token) );

        $user = $results->fetchAll(PDO::FETCH_ASSOC);

        return $user?$user[0]:null;
    }

    public function cancelDonation( $token = "xxx"){
        if( $this->DB() ){

            $results = $this->DB()->prepare('UPDATE donations SET updated_at=:updated_at, status=:status WHERE token = :token');
            $results->execute( array(
                "token" => $token,
                "status" => "canceled",
                "updated_at" => date("d-m-Y H:i:s")
            ));

            return $this->getUserByToken($token);
        }
    }

    public function convertLeadToSuccessDonation( $token = "xxx" ){

        $form = $this->form;


        if( $this->DB() ){

            $results = $this->DB()->prepare('UPDATE donations SET updated_at=:updated_at, payment_amount=:amount, payment_fees=:fees, payer_id=:payer_id, status=:status WHERE token = :token');
            $results->execute( array(
                "token" => $token,
                "amount" => isset($form["mc_gross"])?$form["mc_gross"]:"",
                "fees" => isset($form["mc_fee"])?$form["mc_fee"]:"",
                "payer_id" => isset($form["payer_id"])?$form["payer_id"]:"",
                "status" => "completed",
                "updated_at" => date("d-m-Y H:i:s")
            ));

            $user = $this->getUserByToken($token);

            if( $user ){
                $this->_sendToClient($user);
            }

            return $user;
        }
    }

    public function addLeadToDatabase()
    {

        $form = $this->form;

        if( $this->DB() ){

            //$this->db->beginTransaction();
            $query = $this->DB()->prepare('INSERT INTO donations (token, first_name, last_name, email, address1, zip, city, amount, created_at) VALUES (:token, :first_name, :last_name, :email, :address1, :zip, :city, :amount, :created_at)');
            $query->execute(array(
                "token" => uniqid(),
                "first_name" => isset($form["first_name"])?$form["first_name"]:"",
                "last_name" => isset($form["last_name"])?$form["last_name"]:"",
                "email" => isset($form["email"])?$form["email"]:"",
                "address1" => isset($form["address1"])?$form["address1"]:"",
                "zip" => isset($form["zip"])?$form["zip"]:"",
                "city" => isset($form["city"])?$form["city"]:"",
                "amount" => isset($form["amount"])?$form["amount"]:"",
                "created_at" => date("d-m-Y H:i:s")
            ));

            //$this->db->commit();

            $results = $this->DB()->prepare('SELECT * from donations WHERE id = :id');
            $results->execute(array(
                "id" => $this->DB()->lastInsertId()
            ));

            return $results->fetchAll(PDO::FETCH_ASSOC)[0];
        }

        return null;
    }


    function send(){

        $this->form['firstname']  = $this->test_input($this->form['firstname']);
        $this->form['lastname']   = $this->test_input($this->form['lastname']);
        $this->form['email']      = $this->test_input($this->form['email']);
        $this->form['comment']    = $this->test_input($this->form['comment']);

        if (
            filter_var($this->form['email'], FILTER_VALIDATE_EMAIL) &&
            !empty($this->form['firstname']) &&
            !empty($this->form['lastname']) &&
            !empty($this->form['comment'])
        ){

            $this->form['address1']    = $this->test_input(isset($this->form['address1'])?$this->form['address1']:"");
            $this->form['zip']         = $this->test_input(isset($this->form['zip'])?$this->form['zip']:"");
            $this->form['city']        = $this->test_input(isset($this->form['city'])?$this->form['city']:"");


            // ENVOI DES MAILS
            if( !$this->_sendToClient() || !$this->_sendToAdmin() ){
                return ["success"=>false, "error"=>"sendmail"];
            }

            return ["success"=>true];

        }else{
            return ["success"=>false, "error"=>"fields"];
        }
    }

    private function _sendToClient( $user ){

        // CONFIG
        $from       = self::getGrav()['config']->get('plugins.maven.donation.from.email');
        $fromName   = self::getGrav()['config']->get('plugins.maven.donation.from.name');

        $vars = [
            "form" => $user,
            "data" => [
                "title" => self::getGrav()['language']->translate(["DONATION.MAILER.THANKS.TITLE", $user["first_name"]]),
                "body" => self::getGrav()['language']->translate(["DONATION.MAILER.THANKS.BODY"])
            ]
        ];

        $subject = self::getGrav()['language']->translate(["DONATION.MAILER.THANKS.SUBJECT"]);

        $content = "";
        self::getGrav()['twig']->twig_paths[] = __DIR__ . '/templates';
        $content = self::getGrav()['twig']->processString('{% include "forms/donation/thanks.html.twig" %}', $vars);

        $message = self::getGrav()['Email']->message($subject, $content, 'text/html')
            ->setFrom($from, $fromName)
            ->setTo($user["email"]);

        return self::getGrav()['Email']->send($message);
    }

    private function _sendToAdmin(){
        // CONFIG
        //$toBcc    = self::getGrav()['config']->get('plugins.maven.technical.email');
        $to       = self::getGrav()['config']->get('plugins.maven.administrator.email');

        $subject = self::getGrav()['config']->get('site.name') . " | " . self::getGrav()['language']->translate(["DONATION.MAILER.THANKS.SUBJECT"]);

        $comment = nl2br($this->form["comment"]);
        $comment = str_replace("\r","", $comment);
        $comment = str_replace("\n","", $comment);

        $content  = "<table>";
        $content .= "<tr><td>".$this->form["first_name"]." ".$this->form["last_name"]."</td></tr>";
        $content .= "<tr><td>".$this->form["address1"].", ".$this->form["zip"]." ".$this->form["city"]."</td></tr>";
        $content .= "<tr><td>Donation de CHF ".$this->form["amount"]."</td></tr>";
        $content .= "</table>";

        $message = self::getGrav()['Email']->message($subject, $content, 'text/html')
            ->setTo($to)
            //->setBcc($toBcc)
            ->setReplyTo($this->form["email"])
            ->setFrom($this->form["email"], $this->form["first_name"]." ".$this->form["last_name"]);

        return self::getGrav()['Email']->send($message);
    }


    public function getAllResults(){
        if( $this->DB() ){

            $results = $this->DB()->prepare('SELECT * from donations ORDER BY id DESC');
            $results->execute();

            return $results->fetchAll();
        }

        return null;
    }



    /**
     * Create unix timestamp for storing the data into the filesystem.
     *
     * @param string $format
     * @param int    $utimestamp
     *
     * @return string
     */
    private function udate($format = 'u', $utimestamp = null)
    {
        if (is_null($utimestamp)) {
            $utimestamp = microtime(true);
        }

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', \sprintf('%06d', $milliseconds), $format), $timestamp);
    }

}
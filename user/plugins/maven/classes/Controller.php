<?php
namespace Grav\Plugin\Maven;

use Grav\Common\Grav;
use RocketTheme\Toolbox\File\File;
use Grav\Common\Plugin;
use Grav\Common\User\User;
use Grav\Common\Page\Page;
use Grav\Common\Page\Pages;

use Grav\Plugin\Maven\DB;

use Grav\Common\GravTrait;

class Controller
{

    use GravTrait;

    protected $language;
    protected $theme;
    protected $uri;
    protected $db;

    function __construct(){

        $this->language     = self::getGrav()['language']->getActive()?self::getGrav()['language']->getActive():self::getGrav()['theme']->config()["default_lang"];
        $this->uri          = self::getGrav()['uri'];
        $this->api_route    = "/" . $this->language . self::getGrav()['config']->get('plugins.maven.api_route') ."/";
        $this->theme        = self::getGrav()['themes']->current();


        $path               = self::getGrav()['locator']->findResource('user://data', true);
        $database           = $path . DS . "weebox.sqlite";

        $this->db           = DB::getInstance('sqlite:' . $database);
    }

    function config( $var ){
        return self::getGrav()['config']->get($var);
    }

    function DB(){
        return $this->db;
    }

    function isPage( $foldername = null ){
        return ( $foldername == $this->getCurrentPageFolder() );
    }

    function getCurrentPageFolder(){
        $page = new Page;
        $page = $page->find($this->uri->path());
        //print_r($page->relativePagePath());
        //exit();
        if( $page ){
            return $page->relativePagePath();
        }
        return null;
    }

    function getPageFolderUrl( $foldername = null ){
        $pages = self::getGrav()["pages"];
        /*
        $page = new Page;
        $page = $page->find($foldername);
        $folder = $page?$page->relativePagePath():null;
        */

        foreach($pages->routes() as $key=>$val){
            $page = new Page;
            $page = $page->find($key);
            echo $foldername." == ".$page->relativePagePath();
            if( $foldername == $page->relativePagePath() ){
                return $page;
                break;
            }
        }

        return null;
    }

}
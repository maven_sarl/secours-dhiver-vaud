<?php
namespace Grav\Plugin\Maven;

use Grav\Common\Grav;
use Grav\Common\GravTrait;
use RocketTheme\Toolbox\File\File;
use Grav\Common\Page\Page;

use Grav\Plugin\Maven\Controller;
use PDO;

class Order extends Controller
{

    private $debug = false;
    private $form;

    function __construct(){
        parent::__construct();
        $this->_initTable();
    }


    private function _initTable(){

        $this->DB()->exec("CREATE TABLE IF NOT EXISTS orders (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    first_name TEXT,
                    last_name TEXT,
                    street TEXT,
                    zipcode TEXT,
                    city TEXT,
                    country TEXT,
                    email TEXT,
                    phone TEXT,
                    comment BLOB,
                    created_at DATETIME)");
    }

    public function routingPage(){


        if( $this->isPage( $this->config("plugins.maven.order.page") ) ){
            // Current page is donation
            self::getGrav()['twig']->twig_vars['order_success'] = isset($_GET["success"])?true:false;
            if( isset($_GET["token"]) ){
                self::getGrav()['twig']->twig_vars['order_form'] = $this->getUserByToken($_GET["token"]);
            }
        }
        // END

    }


    public function routingApi( $app ){

        if( $this->debug ){
            echo $this->language;
            echo "<br>";
            echo $this->api_route;
            echo "<br>";
            echo $this->uri->path();
            echo "<br>";
            echo $this->config("plugins.maven.donation.page");
            echo "<br>";
        }

        $self = $this;

        $app->post($this->api_route . 'order/create', function ($request, $response, $args) use ($self) {

            $self->form = $request->getParams();

            if( !$self->_validateReCAPTCHA() ){
                return $response->withJson(["success"=>"false", "error"=>"captcha error"], 500);
            }

            if( $self->_validateFields() ){
                // FIELDS OK
                $data = $self->addLeadToDatabase();
                return $response->withJson(["success"=>"true", "data"=>$data], 200);
            }else{
                // FIELDS ERROR
                return $response->withJson(["success"=>"false", "error"=>"fields error"], 500);
            }

        });

    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    private function _validateFields(){

        if (
            isset($this->form['email']) &&
            isset($this->form['comment']) &&
            isset($this->form['first_name']) &&
            isset($this->form['last_name']) &&
            isset($this->form['street']) &&
            isset($this->form['zipcode']) &&
            isset($this->form['city']) &&
            isset($this->form['country']) &&
            filter_var($this->form['email'], FILTER_VALIDATE_EMAIL)
        ){
            $this->form['first_name']   = $this->test_input($this->form['first_name']);
            $this->form['last_name']    = $this->test_input($this->form['last_name']);
            $this->form['email']        = $this->test_input($this->form['email']);
            $this->form['street']       = $this->test_input($this->form['street']);
            $this->form['zipcode']      = $this->test_input($this->form['zipcode']);
            $this->form['city']         = $this->test_input($this->form['city']);
            $this->form['country']      = $this->test_input($this->form['country']['name']);

            return true;
        }
        return false;
    }

    private function _validateReCAPTCHA(){

        if( isset($this->form['g-recaptcha-response']) && !empty($this->form['g-recaptcha-response']) ){

            $grecaptcha = $this->form['g-recaptcha-response'];
            $secretkey  = self::getGrav()['config']->get('plugins.maven.recaptcha.secret_key');

            $params = array(
                'secret' => $secretkey,
                'response' => $grecaptcha
            );

            $url = "https://www.google.com/recaptcha/api/siteverify?" . http_build_query($params);

            if (function_exists('curl_version')) {
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_TIMEOUT, 5);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $res = curl_exec($curl);
            } else {
                // Si curl n'est pas dispo, un bon vieux file_get_contents
                $res = file_get_contents($url);
            }


            $json = json_decode($res);

            //$response   = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$grecaptcha), true);
            return $json->success?true:false;

        }

        return false;

    }

    public function addLeadToDatabase()
    {

        $form = $this->form;

        if( $this->DB() ){

            //$this->db->beginTransaction();
            $query = $this->DB()->prepare('INSERT INTO orders (first_name, last_name, street, zipcode, city, country, email, phone, comment, created_at) VALUES (:first_name, :last_name, :street, :zipcode, :city, :country, :email, :phone, :comment, :created_at)');
            $query->execute(array(
                "first_name" => isset($form["first_name"])?$form["first_name"]:"",
                "last_name" => isset($form["last_name"])?$form["last_name"]:"",
                "street" => isset($form["street"])?$form["street"]:"",
                "zipcode" => isset($form["zipcode"])?$form["zipcode"]:"",
                "city" => isset($form["city"])?$form["city"]:"",
                "country" => isset($form["country"])?$form["country"]:"",
                "email" => isset($form["email"])?$form["email"]:"",
                "phone" => isset($form["phone"])?$form["phone"]:"",
                "comment" => isset($form["comment"])?$form["comment"]:"",
                "created_at" => date("d-m-Y H:i:s")
            ));

            //$this->db->commit();

            $results = $this->DB()->prepare('SELECT * from orders WHERE id = :id');
            $results->execute(array(
                "id" => $this->DB()->lastInsertId()
            ));

            $this->_sendToClient();
            $this->_sendToAdmin();

            return $results->fetchAll(PDO::FETCH_ASSOC)[0];
        }

        return null;
    }

    private function _sendToClient(){ 

        // CONFIG
        $from       = self::getGrav()['config']->get('plugins.maven.order.from.email');
        $fromName   = self::getGrav()['config']->get('plugins.maven.order.from.name');

        $vars = [
            "form" => $this->form,
            "data" => [
                "title" => self::getGrav()['language']->translate(["CONTACT.MAILER.THANKS.TITLE", $this->form["first_name"]]),
                "body" => self::getGrav()['language']->translate(["CONTACT.MAILER.THANKS.BODY"])
            ]
        ];

        $subject = self::getGrav()['config']->get('site.name') . " | " . self::getGrav()['language']->translate(["CONTACT.MAILER.THANKS.SUBJECT"]);

        $content = "";
        self::getGrav()['twig']->twig_paths[] = __DIR__ . '/templates';
        $content = self::getGrav()['twig']->processString('{% include "forms/order/thanks.html.twig" %}', $vars);

        $message = self::getGrav()['Email']->message($subject, $content, 'text/html')
            ->setFrom($from, $fromName)
            ->setTo($this->form["email"]);

        return self::getGrav()['Email']->send($message);
    }

    private function _sendToAdmin(){

        // CONFIG
        $toBcc       = self::getGrav()['config']->get('plugins.maven.administrator.email');
        $toBccName   = self::getGrav()['config']->get('plugins.maven.administrator.name');

        $to       = self::getGrav()['config']->get('plugins.maven.order.to_email');
        $toName   = self::getGrav()['config']->get('plugins.maven.order.to_name');

        $subject = self::getGrav()['config']->get('site.name') . " | " . self::getGrav()['language']->translate(["CONTACT.MAILER.THANKS.SUBJECT"]);
        $content = nl2br($this->form["comment"]);
        $content = str_replace("\r","", $content);
        $content = str_replace("\n","", $content);

        $content .= "<hr>";

        $content .= "<table>";
        foreach($this->form["articles"] as $article){
            $content .= "<tr>";
            $content .= "<td>".$article['name']."</td>";
            $content .= "<td>".$article['qty']."</td>";
            $content .= "<td>CHF ".$article['price']."</td>";
            $content .= "</tr>";
        }
        $content .= "</table>";

        $content .= "<hr>";

        $content .= "<table>";
        $content .= "<tr><td>".$this->form["first_name"]." ".$this->form["last_name"]."</td></tr>";
        $content .= "<tr><td>".$this->form["street"]."</td></tr>";
        $content .= "<tr><td>".$this->form["zipcode"]." ".$this->form["city"]."</td></tr>";
        $content .= "<tr><td>".$this->form["country"]."</td></tr>";
        $content .= "<tr><td></td></tr>";
        $content .= "<tr><td>Tel. ".$this->form["phone"]."</td></tr>";
        $content .= "</table>";

        $message = self::getGrav()['Email']->message($subject, $content, 'text/html')
            ->setTo($to, $toName)
            ->setBcc($toBcc, $toBccName)
            ->setReplyTo($this->form["email"])
            ->setFrom($this->form["email"], $this->form["first_name"]." ".$this->form["last_name"]);

        return self::getGrav()['Email']->send($message);
    }

    public function getAllResults(){
        if( $this->DB() ){

            $results = $this->DB()->prepare('SELECT * from orders ORDER BY id DESC');
            $results->execute();

            return $results->fetchAll();
        }

        return null;
    }




    /**
     * Create unix timestamp for storing the data into the filesystem.
     *
     * @param string $format
     * @param int    $utimestamp
     *
     * @return string
     */
    private function udate($format = 'u', $utimestamp = null)
    {
        if (is_null($utimestamp)) {
            $utimestamp = microtime(true);
        }

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', \sprintf('%06d', $milliseconds), $format), $timestamp);
    }

}
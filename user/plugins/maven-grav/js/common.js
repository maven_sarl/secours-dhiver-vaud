jQuery.fn.serializeJSON=function() {
    var json = {};
    jQuery.map(jQuery(this).serializeArray(), function(n, i) {
        var _ = n.name.indexOf('[');
        if (_ > -1) {
            var o = json;
            _name = n.name.replace(/\]/gi, '').split('[');
            for (var i=0, len=_name.length; i<len; i++) {
                if (i == len-1) {
                    if (o[_name[i]]) {
                        if (typeof o[_name[i]] == 'string') {
                            o[_name[i]] = [o[_name[i]]];
                        }
                        o[_name[i]].push(n.value);
                    }
                    else o[_name[i]] = n.value || '';
                }
                else o = o[_name[i]] = o[_name[i]] || {};
            }
        }
        else {
            if (json[n.name] !== undefined) {
                if (!json[n.name].push) {
                    json[n.name] = [json[n.name]];
                }
                json[n.name].push(n.value || '');
            }
            else json[n.name] = n.value || '';
        }
    });
    return json;
};

window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}

function setCookie(name,value,hours) {
    var expires = "";
    if (hours) {
        var date = new Date();
        date.setTime(date.getTime() + (hours*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}
$(document).ready(function(){
    var x = getCookie('weebox-popin');
    if (!x) {
        var popup = $(".weebox-popin");
        setTimeout(function(){
            setCookie('weebox-popin','show', 1);
            popup.removeClass("hide").fadeIn(500, function(){
                popup.addClass("show");
            });
        }, 2000);
        popup.find(".weebox-popin-wrapper").click(function(e){
            var target = e.target || e.srcElement;
            if( target.className == "weebox-popin-wrapper"){
                popup.removeClass("show").addClass("hide");
                popup.fadeOut(500);
            }
        });
        popup.find(".close").click(function(){
            popup.removeClass("show").addClass("hide");
            popup.fadeOut(500);
        });
    }

    /* RGPD FORM CONSENT BOX */
    let rgpdConsentBox = $(".rgpd-form-consent-checkbox");
    if( rgpdConsentBox.length > 0 )
    {
        rgpdConsentBox.find('input[name="rgpd_consent_form"]').change(function(){
            if( $(this).prop("checked") == true )
            {
                rgpdConsentBox.addClass('rgpd-form-consent-checkbox--checked');
            }else{
                rgpdConsentBox.removeClass('rgpd-form-consent-checkbox--checked');
            }
        });
    }
});
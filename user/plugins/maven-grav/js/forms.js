
preSubmitForm = function(e, form)
{
    e.preventDefault();

    var form = $(form);
    var recaptchaPublicKey = form.attr('recaptcha');
    var formName = form.attr('name');

    if( grecaptcha )
    {
        grecaptcha.ready(function() {
            grecaptcha.execute(recaptchaPublicKey, {action: formName}).then(function(token) {
                form.find('[name="token"],[name="action"]').remove();
                form.prepend('<input type="hidden" name="token" value="' + token + '">');
                form.prepend('<input type="hidden" name="action" value="' + formName + '">');
                submitForm(form);
            });
        });
    }else{
        submitForm(form);
    }

    return false;
}

submitForm = function(form)
{

    /* RGPD CONSENT */
    var consentRGPD = form.find('[name="rgpd_consent_form"]').is(':checked');

    if( consentRGPD <= 0 )
    {
        var ask = confirm(form.find('[name="rgpd_consent_form_confirm"]').val());
        if( ask )
        {
            form.find('[name="rgpd_consent_form"]').prop('checked', true);
        }else{
            return false;
        }
    }
    /* RGPD CONSENT */

    form.find('[name="result-success"]').fadeOut();
    form.find('[name="result-error"]').fadeOut();

    var formData = new FormData(form[0]);
    /*
    var file = $('input[name="files"]').get(0).files[0];
    formData.append('file', file);

    for (const pair of formData.entries()) {
        console.log(`${pair[0]}, ${pair[1]}`);
    }
     */

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        //dataType: 'json',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,//form.serialize(),
        success: function(result) {
            form.find('[name="result-success"]').fadeIn();
            form.find('[name="result-success"] .trigger').addClass('drawn');
            form.find('[name="result-fields"]').fadeOut();
            $('html,body').animate({
                scrollTop: form.offset().top - 60
            }, 200);

            /* SEND EVENT TO GTM */
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'form_success_' + form.attr('name'),
                'name': form.attr('action'),
                'form': form.serializeJSON()
            });
            /* END */
        },
        error: function(result) {
            form.find('[name="result-error"]').fadeIn();
            $('html,body').animate({
                scrollTop: form.offset().top - 60
            }, 200);
            form.find('.form-field').removeClass('form-error');
            $.each(result.responseJSON.error, function(index,value){
                form.find('[rel="'+value+'"]').addClass('form-error');
            })
        }
    });

}
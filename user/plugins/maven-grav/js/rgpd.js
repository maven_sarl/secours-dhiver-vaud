'use strict';
class Rgpd {

    constructor() {
        this.cookieName = "rgpd.consent";
        this.elmId = "rgpd-ask-popin";
        this.rules = [];
        this.vars = [];
    }

    add(rule, value) {
        this.rules.push({rule:rule, value:value});
    }

    init() {

        /*
         * Init Cookie Rules
         */
        for(var i=0;i<this.rules.length;i++){
            let variable = this.cookieName + "." + this.rules[i].rule;
            if( localStorage.getItem(variable) !== undefined && localStorage.getItem(variable) !== null )
            {
                // Cookie exists
              //  console.log('RGPD::rule '+variable+' exists', localStorage.getItem(variable));
            }else{
              //  console.log('RGPD::rule '+variable+' new', this.rules[i].value);
                localStorage.setItem(variable, this.rules[i].value);
            }
            this.vars.push(this.rules[i].rule);
        }

        var self = this;
        $('#rgpd-configuration input').each(function(){
            let variable = self.cookieName + "." + $(this).attr('name');
            if( localStorage.getItem(variable) == 1 && $(this).prop('checked') != true && $(this).prop('disabled') != true ){
                $(this).prop('checked', true);
            }
            if( localStorage.getItem(variable) == 0 && $(this).prop('disabled') != true ){
                $(this).prop('checked', false);
            }
        });
        $('#rgpd-configuration input').change(function() {
            var variable = self.cookieName + "." + $(this).attr('name');
            localStorage.setItem(variable, $(this).prop('checked') ? 1 : 0);
            self.launchEventGTM(variable, $(this).prop('checked'));
            if( $("#rgpd-ask-popin[rel='show']").length > 0 )
            {
                self.acceptConsent();
            }
        });
        setTimeout(function (){
            $('#rgpd-configuration .switch').animate({'opacity': 1});
        }, 200);

        var consent = this.getConsent();

        if( consent == 1 )
        {
            // consentAlreadyAccepted
           // console.log('RGPD::init.consentAlreadyAccepted');
            this.intitializeGTM();
        }else if( consent == -1 ){
            // consentAlreadyRefused
           // console.log('RGPD::init.consentAlreadyRefused');
        }else{
            // askForConsent
            //console.log('RGPD::init.askForConsent');
            var self = this;
            if( $("#rgpd-ask-popin[rel='show']").length > 0 )
            {
                // Cookie page, do not shop popin
            }else{
                $('body').toggleClass('rgpd-ask-popin--active');
                if( $("#rgpd-ask-popin[data-overlay='true']").length > 0 )
                {
                    $('body').toggleClass('rgpd-ask-popin--overlay');
                }

                document.getElementById("rgpd.button.accept").addEventListener("click", function() {
                    self.acceptConsent();
                });
                document.getElementById("rgpd.button.confirm").addEventListener("click", function() {
                    self.acceptConsent();
                });
                document.getElementById("rgpd.button.manage").addEventListener("click", function() {
                    self.manageConsent();
                });
                document.getElementById("rgpd.button.back").addEventListener("click", function() {
                    $("#rgpd-ask-popin .toggle-option input").prop("checked", true);
                    self.manageConsent();
                });
            }
        }
    }

    getConsent() {
        return localStorage.getItem(this.cookieName) || null;
    }
    manageConsent() {
        $('#rgpd-ask-popin').toggleClass('manage-cookies');
    }
    acceptConsent() {
        console.log('RGPD::consentAccepted');
        localStorage.setItem(this.cookieName, 1);
        document.getElementById(this.elmId).style.display = 'none';
        this.intitializeGTM();
        return true;
    }
    refuseConsent() {
        console.log('RGPD::consentRefused');
        localStorage.setItem(this.cookieName, -1);
        document.getElementById(this.elmId).style.display = 'none';
        return true;
    }
    launchEventGTM(variable, value) {
        window.dataLayer = window.dataLayer || [];
        variable += "." + (value==1?"allow":"disallow");
        console.log("RGPD::callEvent " + variable)
        window.dataLayer.push({
            event: variable,
            category: 'rgpd',
            action: (value==1?"allow":"disallow"),
            label: variable
        })
    }
    setGTMConsent() {
        let gtm = {};
        for(var i=0;i<this.vars.length;i++){
            let variable = this.cookieName + "." + this.vars[i];
            let status = localStorage.getItem(variable) == 1 ? "granted" : "denied";

            if( this.vars[i] == "necessary" ) {
                gtm.security_storage = status;
            }else if( this.vars[i] == "analytics" ){
                gtm.analytics_storage = status;
            }else if( this.vars[i] == "ad" ){
                gtm.ad_storage = status;
            }else if( this.vars[i] == "functionality" ){
                gtm.functionality_storage = status;
            }else if( this.vars[i] == "personalization" ){
                gtm.personalization_storage = status;
            }
        }
        gtag('consent', 'update', gtm);
        console.log("RGPD::setGTMConsent ", gtm)
    }
    intitializeGTM() {
        var self = this;
        for(var i=0;i<this.rules.length;i++){
            let variable = this.cookieName + "." + this.rules[i].rule;
            self.launchEventGTM(variable, localStorage.getItem(variable));
            console.log("RGPD::initEvent " + variable, localStorage.getItem(variable))
        }
        this.setGTMConsent();
    }
}

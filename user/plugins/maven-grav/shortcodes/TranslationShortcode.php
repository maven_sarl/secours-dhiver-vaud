<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class TranslationShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('translation', function(ShortcodeInterface $sc) {
            $term = $this->grav['maven']->getTranslation( $sc->getContent() );
            return '<span style="color:red;">'.$term.'</span>';
        });
    }
}
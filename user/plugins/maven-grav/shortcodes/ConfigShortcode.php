<?php

namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;
use Grav\Common\Page\Page;
use Grav\Common\Grav;

class ConfigShortcode extends Shortcode
{
    public function init()
    {

        $this->shortcode->getHandlers()->add('config', static function(ShortcodeInterface $sc) {

            $content = $sc->getContent();

            $name = $sc->getParameter('name');
            $grav = Grav::instance();
            $config = $grav['config'];

            $value = $config->get($name);

            if( $content )
            {
                // If Boolean => IF condition
                if( gettype($value) === "boolean" && $value == true )
                {
                    return $content;
                }else{
                    return "";
                }
            }

            return "<em rel='" . $name . "'>" . $value . "</em>";
        });
    }
}
<?php

namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;
use Grav\Common\Page\Page;
use Grav\Common\Grav;

class RgpdShortcode extends Shortcode
{
    public function init()
    {
        $twig = $this->grav['twig'];
        $plugin = $this->grav['config']->get('plugins.maven-grav');

        $this->shortcode->getHandlers()->add('rgpd', static function(ShortcodeInterface $sc) use ($twig, $plugin) {

            $id = $sc->getParameter('id');
            $class = $sc->getParameter('class');
            $path = $sc->getParameter('path');
            $type = $sc->getParameter('type');

            /* KEEP FOR INFO :-)
            $page = Grav::instance()['page'];
            $pageConfigHeaders = $page->find('/config')->header() ?? null;
            $rgpd_scripts = $pageConfigHeaders ?? $plugin ?? null;
            */

            return $twig->processTemplate('partials/rgpd-toggle.html.twig', [
                'config' => $plugin
            ]);
        });
    }
}
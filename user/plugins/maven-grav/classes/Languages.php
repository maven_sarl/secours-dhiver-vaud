<?php
namespace Grav\Plugin\MavenGrav;

use Grav\Common\Config\Config;
use Grav\Common\Grav;
use Grav\Common\Page\Page;
use Grav\Common\Page\Pages;
use RocketTheme\Toolbox\File\File;

class Languages
{

    protected $file = "";
    protected $grav;
    protected $translations = [];



    /*
     * __construct
     */
    public function __construct( Grav $grav, $file = "" )
    {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $this->grav = $grav;

        // Load Excel file if exists
        if( $this->grav['config']->get('site.excel_picker') )
        {
            // Take Config File
            $filename = $this->grav['config']->get('site.excel_picker');
            $file = $filename;
        }else{
            $filename = $this->grav['config']->get('plugins.maven-grav.excel_picker');
            $file = $filename;
        }

        //$this->cacheTranslations();

        return true;

    }

    public function setTranslationsToCurrentLang($lang)
    {
        $translations = [];
        $this->translations = (array) $this->translations;
        ksort($this->translations);

        foreach($this->translations as $key => $translation)
        {
            if( preg_match("/^(.*)." . $lang . "$/i", $key) )
            {

                $lastDotPos = strrpos($key, '.');

                if ($lastDotPos !== false) {
                    $key = substr($key, 0, $lastDotPos);
                }

                $translations[$key] = $translation;
            }
        }

        return $translations;
    }

    public function clearCache()
    {
        // Remove all translation files
        $path = $this->grav['locator']->findResource('user://data', true) . DS . 'maven-grav' . DS;

        if ($handle = opendir($path)) {

            while (false !== ($entry = readdir($handle))) {

                if ($entry != "." && $entry != "..") {
                    File::instance($path . $entry)->delete();
                }
            }

            closedir($handle);
        }

        //$this->cacheTranslations( true );
    }

    public function cacheTranslations($force = false)
    {

        // Cache results in json file
        $defaultLang = $this->grav['language']->getActive() ?? $this->getGravDefaultLanguage();
        $path = $this->grav['locator']->findResource('user://data', true) . DS . 'maven-grav' . DS;
        $cache = $path . 'translations';

        if( File::instance($cache . "." . $defaultLang . ".json")->exists() && $force == false )
        {
            $this->translations = json_decode(File::instance( $cache . "." . $defaultLang . ".json" )->raw());
            //$this->setTranslationsToCurrentLang();
            return true;
        }

        require_once 'Spout/Autoloader/autoload.php';

        $translations = [];

        // Get Excel translations file
        $file = $this->grav['config']->get('plugins.maven-grav.excel_picker');
        $pathExcel = $this->grav['locator']->findResource('user://data', true) . DS . 'maven-grav-translations' . DS . $file;

        if( File::instance($pathExcel)->exists() ) {

            $reader = \Box\Spout\Reader\Common\Creator\ReaderEntityFactory::createXLSXReader();
            $reader->open($pathExcel);

            foreach ($reader->getSheetIterator() as $sheet) {

                $headers = [];

                foreach ($sheet->getRowIterator() as $rowNumber => $row) {
                    // do stuff with the row

                    $data = [];
                    if ($rowNumber === 1) {
                        foreach ($row->getCells() as $cellNumber => $cell) {
                            if ($cellNumber > 0) {
                                $headers[] = $cell->getValue();
                            }
                        }
                    } else {
                        $slug = null;

                        foreach ($row->getCells() as $cellNumber => $cell) {
                            // do stuff with the row

                            $value = $cell->getValue();

                            if ($cellNumber === 0) {
                                $slug = $value;
                            }

                            $position = $cellNumber - 1;

                            if (isset($headers[$position])) {
                                $translations[$slug . '.' . $headers[$position]] = $value;

                            }

                        }
                    }

                }
            }
        }

        $this->translations = $translations;

        $this->generateTranslations();

        $languages = $this->getGravLanguages();
        foreach( $languages as $language )
        {
            $translations = $this->setTranslationsToCurrentLang( $language );
            File::instance($cache . "." . $language . ".json")->save( json_encode( $translations ) );
        }

        $this->translations = json_decode(File::instance( $cache . "." . $defaultLang . ".json" )->raw());

    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function getTranslation($key, $default)
    {
        $langDefault = $this->getGravDefaultLanguage();
        $lang = $this->grav['language']->getActive() ?? $langDefault;

        $showDefaultKey = $this->grav['config']->get('plugins.maven-grav.show_default_key');
        $showMissingKey = $this->grav['config']->get('plugins.maven-grav.show_missing_key');

        $key = strip_tags($key);

        $translations = (object) $this->translations;

        //return "<span style='font-family: Courier New;color: yellow;background:red;padding:0.25em;font-weight: bold;display:inline;font-size:12px;' title='".$key."'>" . $key . "." . $lang . "</span>";

        $value = null;
        if( isset($translations->{strtolower($key. "." . $lang)}) )
        {
            $value = $translations->{strtolower($key. "." . $lang)};
        }else if( isset($translations->{strtolower($key)}) )
        {
            $value = $translations->{strtolower($key)};
        }else if( $default )
        {
            $value = $default;
        }else{
            if( $showMissingKey )
            {
                $value = "<span style='font-family: Courier New;color: yellow;background:red;padding:0.25em;font-weight: bold;display:inline;' title='".$key."'>" . $key . "." . $lang . "</span>";
            }else{
                if( $showDefaultKey )
                {
                    $value = $key;
                }
            }
        }

        return $value;
        //return ($this->translations->{$slug.'.'.$lang} ?? null);
    }

    public function addTranslations($array, $lang = null)
    {

        foreach ($array as $key=>$value){
            $this->translations[strtolower($key).(($lang)?".".$lang:"")] = $value;
        }

    }

    private function getGravDefaultLanguage()
    {
        return $this->grav['language']->getDefault() ?? "EN";
    }

    private function getGravLanguages()
    {
        $defaultLang = $this->getGravDefaultLanguage();
        $languages = $this->grav['language']->getLanguages();
        $languages = count($languages) == 0 ? [$defaultLang] : $languages;

        return $languages;
    }

    public function generateTranslations()
    {

        $translations = [];

        $defaultLang = $this->getGravDefaultLanguage();
        $languages = $this->getGravLanguages();

        foreach($languages as $lang)
        {
            $translationsKeys = $this->convertArray( Grav::instance()['languages']->get($lang));
            foreach($translationsKeys as $key => $value)
            {
                if( !preg_match("/GRAV|PLUGIN(.*)/i", $key) )
                {
                    if( !isset($translations[$key]) )
                    {
                        $translations[$key] = [];
                    }
                    $translations[strtoupper($key)][strtoupper($lang)] = $value;

                }

            }
        }

        /*
        foreach($languages as $lang)
        {

            foreach($translations as $key => $trans)
            {
                if( !isset($translations[$key][strtoupper($lang)]) )
                {
                    // Translate via Deepl

                    $defaultValue = $translations[$key][strtoupper($defaultLang)] ?? null;
                    $translations[$key][strtoupper($lang)] = "";//"Missing: " . $defaultValue;

                }
            }

        }*/

        $this->addTranslations( $this->convertArray( $translations ) );

        /*
         * Parse Config Page
         */

        $pages = $this->grav['pages'];
        $language = $this->grav['language'];

        $active_lang = $language->getActive() ?? $defaultLang;

        $currentTransations = [];

        foreach($languages as $lang)
        {

            /*
             * Get translations from Config Page dans chaque langue
             */
            $language->init();
            $language->setActive($lang);
            $pages->reset();

            $configPage = $this->grav['pages']->find('/config');

            if( $configPage ){

                $translations = [];
                $translationsFromConfig = $configPage->header()->translations ?? [];
                $translationsFromConfigCustoms = [];
                if( isset($translationsFromConfig['customs']) )
                {
                    $translationsFromConfigCustoms = $translationsFromConfig['customs'];
                    unset($translationsFromConfig['customs']);
                }

                $array = array_merge($this->convertArray( $translationsFromConfig), $this->convertArray( $translationsFromConfigCustoms));

                foreach($array as $key => $value)
                {
                    if( !isset($currentTransations[$key]) )
                    {
                        $currentTransations[$key] = [];
                    }
                    foreach($this->getGravLanguages() as $l)
                    {
                        //$currentTransations[$key][$l] = "xx";
                    }
                    $currentTransations[$key][$lang] = $value;
                }

                $this->addTranslations( $this->convertArray( $currentTransations) );

            }
        }

        $language->init();
        $language->setActive($active_lang);
        $pages->reset();

        // EO
    }

    private function convertArray($arr, $narr = array(), $nkey = '') {

        if (gettype($arr) == "array" || gettype($arr) == "object" ){
            foreach ( $arr as $key => $value) {
                if (is_array($value)) {
                    $narr = array_merge($narr, $this->convertArray($value, $narr, $nkey . $key . '.'));
                } else {
                    $narr[$nkey . $key] = $value;
                }
            }
        }



        return $narr;
    }

}

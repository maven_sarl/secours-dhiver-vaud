<?php
namespace Grav\Plugin\MavenGrav;

use Grav\Common\Config\Config;
use Grav\Common\Grav;
use Symfony\Component\Mime\Address;
use Grav\Plugin\MavenGrav\Languages;

class Form
{

    protected $grav;
    protected $fromEmail = null;
    protected $fromName = null;
    protected $from = null;
    protected $replyEmail = null;
    protected $replyName = null;
    protected $reply = null;
    protected $to = null;
    protected $cc = null;
    protected $bcc = null;
    protected $subject = null;
    protected $message = null;
    protected $template = null;
    protected $values = [];

    /*
     * __construct
     */
    public function __construct()
    {
        $this->grav = Grav::instance();
    }

    public function setFrom($email, $name)
    {
        $this->fromEmail = $email;
        $this->fromName = $name;
        $this->from = new Address($email, $name);
        return $this;
    }

    public function setReplyTo($email, $name)
    {
        $this->replyEmail = $email;
        $this->replyName = $name;
        $this->reply = new Address($email, $name);
        return $this;
    }

    public function setTo($array)
    {
        $this->to = $array;
        return $this;
    }

    public function setCc($array)
    {
        $this->cc = $array;
        return $this;
    }

    public function setBcc($array)
    {
        $this->bcc = $array;
        return $this;
    }

    public function setSubject($string)
    {
        $this->subject = $string;
        return $this;
    }

    public function setValues($array)
    {
        $this->values = $array;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function send()
    {

        $translations = new Languages($this->grav);
        $translations->cacheTranslations();

        $this->values['t'] = (array) $translations->getTranslations();
        $this->values['config'] = Grav::instance()['config'];

        $twig = Grav::instance()['twig']->init();

        if( $this->template )
        {
            $body = $twig->processTemplate($this->template, $this->values);
        }else{
            $body = $twig->processString($this->message, $this->values);
        }

        $message = $this->grav['Email']->message($this->subject, $body, 'text/html')
            ->setFrom($this->from)
            ->setTo($this->to);

        if( $this->reply != null ){ $message->replyTo($this->reply); }
        if( $this->cc != null ){ $message->cc($this->cc); }
        if( $this->bcc != null ){ $message->bcc($this->bcc); }

        if( $this->grav['Email']->send($message) ){
            return true;
        }else{
            return false;
        }
    }

    public static function validateRecaptcha( $secretKey, $token, $action )
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $secretKey, 'response' => $token)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $arrResponse = json_decode($response, true);

        if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
            return true;
        }

        return false;
    }

}

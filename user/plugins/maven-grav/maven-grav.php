<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Grav;
use Grav\Common\Page\Types;
use Grav\Common\Plugin;
use Grav\Common\Session;
use Grav\Common\Page\Page;
use Grav\Common\Data;
use Grav\Common\Page\Pages;
use Grav\Common\Page\Collection;
use Grav\Common\Page\Header;
use Grav\Common\Uri;
use Grav\Common\Taxonomy;
use RocketTheme\Toolbox\Event\Event;
use DateTime;
use GuzzleHttp\Client;

use Grav\Plugin\MavenGrav\Languages;
use Grav\Plugin\MavenGrav\GeoLoc;
use RocketTheme\Toolbox\File\File;

/**
 * Class MavenGravPlugin
 * @package Grav\Plugin
 */
class MavenGravPlugin extends Plugin
{
    protected $tinypng;
    protected $translations;
    protected $dateFormat = "d-m-Y H:i";
    public $ip_address = [];

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'onGetPageBlueprints' => ['onGetPageBlueprints', 0],
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onPluginsInitialized' => [
                ['autoload', 100000], // TODO: Remove when plugin requires Grav >=1.7
                ['onPluginsInitialized', 0]
            ],
            'onThemeInitialized' => [
                ['onThemeInitialized', 0]
            ],
            'onBlueprintCreated' => ['onBlueprintCreated', 0],
            'onBeforeCacheClear' => ['onBeforeCacheClear', 0],
        ];
    }

    /**
     * Add page blueprints
     *
     * @param Event $event
     */
    public function onGetPageBlueprints(Event $event)
    {

        /** @var Types $types */
        $types = $event->types;
        $types->scanBlueprints('plugins://maven-grav/blueprints');

    }

    /**
     * Add current directory to twig lookup paths.
     */
    public function onTwigTemplatePaths()
    {
        if (!$this->isAdmin()) {
            $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';

            $this->grav['assets']
                ->addJs('plugin://maven-grav/js/common.js')
                ->addJs('plugin://maven-grav/js/rgpd.js')
                ->addCss('plugin://maven-grav/css/rgpd.css', 110);
        }


    }


    /**
     * Initialize the plugin
     */
    public function onAdminCreatePageFrontmatter($event)
    {

        $page   = $event;
        $header = $page['header'];

//        $header['headline'] = "cooucoucou";

        $event['header'] = $header;

        /*
        return;

        $page = $event['object'];

        if (!$page instanceof PageInterface) {
            return;
        }

        $header = $page->header();
        if (!$header instanceof Header) {
            $header['headline'] = "coucou";
            $header = new Header((array)$header);
        }
        */



        //$header = new Data((array)$page->header());
        //$page->header($header->toArray());
    }

    /**
     * Composer autoload.
     *
     * @return ClassLoader
     */
    public function autoload(): ClassLoader
    {
        return require __DIR__ . '/vendor/autoload.php';
    }

    public function onThemeInitialized(): void
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {

            $this->enable([
                'onAdminCreatePageFrontmatter' => ['onAdminCreatePageFrontmatter', 0],
            ]);
            return;
        }
    }

    /**
     * onPluginsInitialized.
     *
     * @return void
     */
    public function onPluginsInitialized(): void
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {

            require_once __DIR__ . '/classes/tinypng.php';
            $this->tinypng = new Tinypng($this->config->get('plugins.maven-grav.tinypng_api_key'));

            $this->enable([
                'onAdminSave' => ['onAdminSave', 0],
                //'onAdminTaskExecute' => ['onAdminTaskExecute', 0],
                'onAdminTwigTemplatePaths' => ['onAdminTwigTemplatePaths', 0],
                'onAdminAfterAddMedia' => ['onAdminAfterAddMedia', 0],
            ]);
            return;
        }

        $this->grav['maven'] = $this;


        $this->enable([
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
            'onTwigExtensions' => ['onTwigExtensions', 0],
            'onShortcodeHandlers' => ['onShortcodeHandlers', 0],
            'onPagesInitialized' => ['onPagesInitialized', 0],
        ]);

    }



    /**
     * onShortcodeHandlers.
     *
     * @return void
     */
    public function onShortcodeHandlers(): void
    {
        $this->grav['shortcode']->registerAllShortcodes(__DIR__.'/shortcodes');
    }

    /**
     * onTwigExtensions.
     *
     * @return void
     */
    public function onTwigExtensions(): void
    {
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('cleanFolderName', [$this, 'cleanFolderName'])
        );
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('__', [$this, 'getTranslation'])
        );
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('i18n', [$this, 'getTranslation'])
        );
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('getTranslations', [$this, 'getTranslations'])
        );
    }

    /**
     * getTranslation.
     *
     * @return String
     */
    public function getTranslation($value, $default = null): ?string
    {

        if ($this->translations && !empty($value))
        {
            $newValue = $this->translations->getTranslation($value, $default);
            return $newValue == null ? null : $newValue;
        }

        //return "#TRANS:" . $value;
        return null;

    }

    /**
     * cleanFolderName.
     *
     * @return String
     */
    public function cleanFolderName($value): ?string
    {

        $value = explode('.',$value);

        if(count($value) > 1 ) {
            $value = $value[1];
        } else {
            $value = $value[0];
        }

        return $value;
    }

    /**
     * getTranslation.
     *
     * @return String
     */
    public function getTranslations()
    {
        return $this->translations->getTranslations();
    }

    public function onBeforeCacheClear(Event $event)
    {
        $language = new Languages($this->grav);
        $language->clearCache();
    }



    public function treatWeeboxInfo()
    {
        $uri = $this->grav['uri'];

        if( preg_match("#^[/]?weebox-info/?(.*)[/]?$#i", $this->grav['uri']->path())  ){

            $url = "https://github.com/getgrav/grav";

            //  Initiate curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            //$gravData = json_decode($result, true);

            preg_match('/href=["\']?\/getgrav\/grav\/releases\/tag\/([0-9\.]+)["\']?/i', $result, $match);

            $isUpdated = false;
            $latestVersion = isset($match[1]) ? $match[1] : null;
            if( $latestVersion )
            {
                $isUpdated = GRAV_VERSION == $latestVersion ? 1 : 0;
            }

            header('Content-Type: application/json');
            echo json_encode([
                'SERVER_NAME' => $uri->scheme() . $uri->host() . str_replace('//', '/', $this->grav['uri']->rootUrl()),
                'PHP_VERSION' => PHP_VERSION,
                'GRAV_VERSION' => GRAV_VERSION,
                'GRAV_LATEST_VERSION' => $latestVersion,
                'GRAV_IS_UPDATED' => $isUpdated,
                'MAVEN_GRAV_VERSION' => $this->grav['config']->get("plugins.maven-grav.version"),
                'HAS_MAINTENANCE' => $this->grav['config']->get("plugins.maven-grav.has_maintenance")
            ]);
            exit();
        }
    }


    /**
     * Send user to a random page
     */
    public function onPagesInitialized(Event $e)
    {

        /*
         * Check if page is CRON
         */
        if( preg_match("#^/?clear-cache/?$#i", $this->grav['uri']->path()) ){
            $language = new Languages($this->grav);
            $language->clearCache();
            echo "<div style='background:#f3fae9;color:green;padding:10px;text-align:center;'>MavenGrav: Cache cleared</div>";
            exit();
        }
        /* ENDOF */


        if( $this->config->get('plugins.maven-grav.geoloc_active') == true )
        {
            $this->getVisitorGeoLocalization();
        }

        /*
         * Load languages
         */
        $this->translations = new Languages($this->grav);
        $this->translations->cacheTranslations();

        //echo "<pre>";
        //print_r($this->translations->getTranslations());
        //exit();


        $this->treatWeeboxInfo();

        $uri = $this->grav['uri'];
        $route = $uri->path() ?? null;


        if( isset($_GET['mode']) && isset($_GET['token']) && $_GET['mode'] === 'preview' )
        {

            $page = $this->grav['pages']->find($uri->path());
            if( $page )
            {
                if( $_GET['token'] !== $page->id() ){
                    // Invalid Token
                    //throw new \Exception("Token");
                }else{
                    // Page exists, Token valid !
                    unset($this->grav['page']);
                    $page->published(true);
                    $this->grav['page'] = $page;
                }

            }

        }

        //echo "<pre>";
        //print_r($this->translations->getTranslations());
        //exit();

    }


    public function onAdminTaskExecute(Event $event)
    {

        $message = $this->grav['Email']->message("Grav", "Task:". $event['method'], 'text/html')
            ->setFrom('info@maven.ch')
            ->setTo('info@maven.ch');

        // media.upload
        throw new \Exception($event['method']);
        print_r($event['method']);
        exit();
    }

    private function cleanFilename($chaine) {
        // Remplace les caractères accentués par leurs équivalents sans accent
        $chaine = strtr($chaine,
            "àáâãäåèéêëìíîïòóôõöùúûüñç",
            "aaaaaaeeeeiiiioooouuuunc"
        );

        // Remplace les espaces par des tirets
        $chaine = str_replace(' ', '-', $chaine);

        // Remplace les caractères spéciaux par des tirets de soulignement
        $chaine = preg_replace('/[^a-zA-Z0-9\-]/', '_', $chaine);

        return $chaine;
    }

    public function onAdminAfterAddMedia(Event $event)
    {
        $page = $event['object'];
        $filename = $_POST['name'] ?? null;
        $media = $page->media();

        $media = $media[$filename] ?? null;

        if( $media && $filename ){
            $page_path = $page->path();
            $path = "$page_path/$filename";
            $extension = strtolower($media->extension);

            $formatName = str_replace('.'.$media->extension, '', $filename);

            // Rename file
            $newName = $this->cleanFilename($formatName) . '.' .$media->extension;
            $newPath = "$page_path/$newName";

            if( in_array($extension, ['png', 'jpg', 'jpeg']) )
            {
                $result = $this->tinypng->tinify($path);
                if (!empty($result)) {
                    file_put_contents($path, $result);
                    rename($path, $newPath);
                }
            }else{
                rename($path, $newPath);
            }
        }

    }

    /**
     * Send user to a random page
     */
    public function onAdminTwigTemplatePaths($event): void
    {

        $paths = $event['paths'];
        array_unshift($paths, __DIR__ . '/admin/templates');
        $event['paths'] = $paths;

    }

    public function onAdminSave(Event $event)
    {

        // Get the page just saved
        //$page = $e->offsetGet('object');
        $page = $event['object'];

        if( $page && method_exists($page, "getType") && $page->getType() === "pages" )
        {
            if( !isset($page['header']['date']) )
            {
                // Set a default date
                $page['header']['date'] = date($this->dateFormat);
            }
            if( !isset($page['header']['publish_date']) )
            {
                // Set a default publish date
                $page['header']['publish_date'] = date($this->dateFormat);
            }
        }else{

            if( isset($page->has_maintenance) )
            {

                $language = new Languages($this->grav);
                $language->clearCache();

            }

        }


        //$content = $page->content();

        return true;

        /*
        print_r($page['header']);
        exit();

        if( $page->isPublished() )
        {
            // Page is published and must be live
        }


        // Add text to the content
        $content = $page->content();
        $page->content($content . "Add this text to end of file");

        $_SESSION["coucou"] = "Gab";


        // Current published page
        $filepath = dirname($page->filePath());
        $filename = basename($page->filePath());

        // Save draft with current date
        $filepath_draft = $filepath . self::PATH_DRAFT;
        $filename_draft = date('Ymd\_His\_') . $filename;

        if( !file_exists($filepath_draft) )
        {
            mkdir($filepath_draft);
        }

        $content = file_get_contents($page->filePath());
        file_put_contents($filepath_draft . '/' . $filename_draft, $content);
        */

    }


    /**
     * Set needed variables to display breadcrumbs.
     */
    public function onTwigSiteVariables()
    {



    }

    /**
     * Extend page blueprints with feed configuration options.
     *
     * @param Event $event
     */
    public function onBlueprintCreated(Event $event)
    {
        static $inEvent = false;

        /** @var Data\Blueprint $blueprint */
        $blueprint = $event['blueprint'];
        if (!$inEvent && $blueprint->get('form/fields/tabs', null, '/')) {
            if (in_array($blueprint->getFilename(), ['config'])) {

                $inEvent = true;
                $blueprints = new Data\Blueprints(__DIR__ . '/blueprints/');
                $extends = $blueprints->get('custom_translations');
                $blueprint->extend($extends, true);
                $inEvent = false;

                $inEvent = true;
                $blueprints = new Data\Blueprints(__DIR__ . '/blueprints/');
                $extends = $blueprints->get('popins');
                $blueprint->extend($extends, true);
                $inEvent = false;
            }
        }
    }



    public function getVisitorGeoLocalization()
    {

        $ip = null;

        /*
         * GEO LOCALISATION
         */
        try{

            $requestModel = new GeoLoc();
            $ip = $requestModel->getIpAddress();
            //$ip = '178.39.192.229';
            $cookieMd5 = md5("IP:" . $ip);
            if( $requestModel->isValidIpAddress($ip) )
            {
                if( isset($_COOKIE[$cookieMd5]) )
                {
                    $this->ip_address = json_decode($_COOKIE[$cookieMd5]);
                }else{
                    $this->ip_address = (object) $requestModel->getLocation($ip);
                    setcookie($cookieMd5, json_encode($this->ip_address), time() + (1 * 24 * 60 * 60) );
                }

            }else{
                throw new \Exception("Ip not valid");
            }
        }catch(\Exception $e){
            $this->ip_address = (object) [
                "status" => "success",
                "country" => "Undefined",
                "countryCode" => "XX"
            ];
        }

        $this->grav['twig']->twig_vars['ip_address'] = $this->ip_address;

    }

    /*
     * For the Admin BackOffice
     */
    public static function getSocialOptions()
    {
        return [
            'linkedin' => "LinkedIn",
            'facebook' => "Facebook",
            'instagram' => "Instagram",
            'youtube' => "Youtube",
            'tiktok' => "Tiktok",
            'twitch' => "Twitch",
            'pinterest' => "Pinterest"
        ];
    }

}
